#include "Augments2.h"

const std::string AugmentsForIntersectionSSE::name() const { return  "AugmentsForIntersectionSSE"; }
//=============================================================================================
void* AugmentsForIntersectionSSE::getOne() const { return new AugmentsForIntersectionSSE(); }
//=============================================================================================
void AugmentsForIntersectionSSE::entireBlockDidsAndScores(LowLevelListAccessorInterface* list, did_t* dids, void* scores, const did_t block) const { //decode given block and calc scores
	list->entireBlockDidsAndScores(dids,reinterpret_cast<float*>(scores),block,1); //1 for sse
}
//=======================buildPBS_M3======================================================================
void AugmentsForIntersectionSSE::batchUnpackingBlocksUptoDid(LowLevelListAccessorInterface* list, did_t& postings, did_t nextWindowsFirst, did_t* realDids, void* scores, did_t& block) const {
	list->batchUnpackingBlocksUptoDid(postings,nextWindowsFirst,realDids,reinterpret_cast<float*>(scores),block,1); //1 for sse
}
//=============================================================================================
void AugmentsForIntersectionSSE::batchUnpackingBlocks(LowLevelListAccessorInterface* list, did_t endBlock, did_t* dids, void* scores, did_t startBlock) const {
	list->batchUnpackingBlocks(endBlock,dids,reinterpret_cast<float*>(scores),startBlock,1); //1 for sse
}
//=============================================================================================
void AugmentsForIntersectionSSE::buildBM(LowLevelListAccessorInterface* list, const did_t* docIds, const void* scores, void* blockMaxes, const unsigned size, const did_t smallest){
	//stub
}
//=============================================================================================
void AugmentsForIntersectionSSE::buildPBS_M3(unsigned* dest, const unsigned* dids, const unsigned winStartDid, unsigned bits, const unsigned sz) {
#ifdef USEPOSTINGS
	memset(dest,0,CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK);
	COUTL << "here" << ENDL;
	buildPBS_M3_SSE_(dest,dids,winStartDid,bits,sz);
#endif
}
//=============================================================================================
void AugmentsForIntersectionSSE::buildLivenessBitvector(unchar* dest, void** blockScores, unchar** postingBitsets, const unchar threshold, const unsigned termsCount, const unsigned windowsize) {
}
