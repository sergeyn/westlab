#include <cstring>
#include "NewIterator.h"
#include "profiling.h"
#include "Pfor.h"
#include "SSE.h"
#include "Scalar.h"

using namespace enm;

const unsigned* NewIteratorBase::documentLengthArr;
unsigned  	NewIteratorBase::instancesCounter(0);


#ifdef	USEAUGMENTS
unsigned* 	NewIteratorAugmentedBase::fakePostingBitSet(nullptr);
score_t*   	NewIteratorAugmented::scoresBuffer;
unsigned  	NewIteratorAugmentedBase::didBlockBits(6);
unsigned  	NewIteratorAugmentedBase::currentWindow(0);
did_t*	  	NewIteratorAugmentedBase::scratchPadDids[MAX_TERMS_IN_QUERY];
score_t*  	NewIteratorAugmented::scratchPadScores[MAX_TERMS_IN_QUERY];
unsigned* 	NewIteratorAugmentedBase::scratchPadPBS[MAX_TERMS_IN_QUERY];
score_t*  	NewIteratorAugmented::scratchPadBMs[MAX_TERMS_IN_QUERY];
score_t*	NewIteratorAugmented::blockScores[MAX_TERMS_IN_QUERY];
unchar*		NewIteratorAugmented::blockScoresQuants[MAX_TERMS_IN_QUERY];
unchar*		NewIteratorAugmented::scratchPadBMQs[MAX_TERMS_IN_QUERY];
unchar*		NewIteratorAugmentedBase::bitSetsPBS[MAX_TERMS_IN_QUERY];
unchar* 	NewIteratorAugmentedBase::scratchPadQBMs[MAX_TERMS_IN_QUERY];
unsigned* 	NewIteratorAugmentedBase::liveDeadArea(nullptr);
unsigned  	NewIteratorAugmentedBase::firstDidOfWindow(0);
NewIteratorAugmented NewIteratorAugmented::scratchPads[MAX_TERMS_IN_QUERY];

#endif

//=============================================================================================
NewIteratorBase::NewIteratorBase() {
	if( ++NewIteratorBase::instancesCounter > 32) FATAL("Why do you need so many iterators?");
}
//=============================================================================================
void NewIteratorBase::setDocLenPtr(const unsigned* ptr) {
	NewIteratorBase::documentLengthArr = ptr;
}
//=============================================================================================
void NewIteratorBase::setUseRealScores(bool use) {
	useRealScores = use;
}
//=============================================================================================
void NewIteratorAugmented::initBM25Float(const unsigned unpaddedLen) {
	BM25Element = (float)3 * (float)(  log( 1 + (float)(CONSTS::NUMERIC::MAXD - unpaddedLen + 0.5)/(float)( unpaddedLen + 0.5))/log(2));
	realScoresFloat = nullptr;
}
//=============================================================================================
//open list sets the compressed data related pointers
void NewIteratorBase::injectCompressedState(const unsigned unpaddedLen, const unsigned *data, const unsigned *sizes, const unsigned* maxes, const unsigned* flgs) {

	paddedLength = (0 == unpaddedLen%CONSTS::NUMERIC::BS)  ? unpaddedLen : unpaddedLen + CONSTS::NUMERIC::BS - (unpaddedLen%CONSTS::NUMERIC::BS);	//number of postings in lists padded to be BS aligned

	assert(paddedLength && paddedLength<CONSTS::NUMERIC::MAXD && paddedLength%CONSTS::NUMERIC::BS == 0);
	assert(data && sizes && maxes && flgs);

	compressedBase = data;	// pfor compressed chunks
	maxDidPerBlock = maxes;	// arr of max values for each chunk
	sizePerBlock = sizes;	// arr of sizes for each chunk
	flags = flgs;			// arr of pfor flags for each chunk -- Shuai!!!
	resetPostingIterator();
	#ifdef DEBUG
	//sanity checks
	COUT4 << "P.Len: " <<paddedLength << " Blocks: "<< paddedLength/CONSTS::NUMERIC::BS << ENDL;
	for(unsigned i=0; i<paddedLength/CONSTS::NUMERIC::BS; ++i) {
		assert(maxDidPerBlock[i]<CONSTS::NUMERIC::MAXD);
	//	assert(sizePerBlock[i]<=CONSTS::NUMERIC::BS*3*sizeof(unsigned)); //just an upper bound, not tight
		assert(((flags[i]>>12) & 15)<=12);
	}
	#endif
	realDids = nullptr;
}
//=============================================================================================
void NewIteratorBase::resetPostingIterator() { //reset the list after traversal for new round
	offsetFromBase = compressedBlock = offsetInPostingSpace = fflag = 0;
	//decompress first block
	ptrToFreqs = packDecodeDids(didsBuffer, compressedBase, flags[0]&CONSTS::NUMERIC::maskFlagWithOnes, 0,unpacker); //decode new block
	did = didsBuffer[0];
}
//=============================================================================================

#ifdef	USEAUGMENTS
PreprocessingData::PreprocessingData() :
							quantBlockMaxes(nullptr),
							postingBitSet(nullptr), quantile(0),nonZeroBlockMaxes(nullptr),
							compressedZeros(nullptr),flagsForBMs(nullptr),sizesForBMs(nullptr)

{}

//=============================================================================================
NewIteratorAugmentedBase::NewIteratorAugmentedBase(){}
NewIteratorAugmented::NewIteratorAugmented()  {}
//=============================================================================================
void NewIteratorAugmented::initListFromCacheFloatPart(PreprocessingData& initData) {
	//blockMaxesBase = initData.blockMaxes;
	quantileForBM = initData.quantile;
	maxScoreOfList = initData.maxScoreOfListFloat;
        #ifdef USEUNCOMPRESSEDQUANTS
                rawQuants = initData.rawQuants;
        #endif
	#ifdef DEBUG
		res = initData;
	#endif
}
//=============================================================================================
void NewIteratorAugmentedBase::initListFromCache(PreprocessingData& initData) {
	if(policy == BMQC_FAKE || policy == BMQC_PBS || policy == BMQC_PBSC)
		bmQorC = initData.compressedZeros;
	else
		bmQorC = reinterpret_cast<unsigned*>(initData.quantBlockMaxes);
	flagsBM=initData.flagsForBMs;
	unsigned tmp[4] = {0,0,0,0};
	stateForBMQC = ktuple<unsigned,4>(tmp);
	nonZeroesBase = initData.nonZeroBlockMaxes;
	postingBitSetBase = initData.postingBitSet;
	sizesCompBMs = initData.sizesForBMs;
	//#ifdef DEBUG
	//if(policy == BMQC_FAKE || policy == BMQC_PBS || policy == BMQC_PBSC)
	//	for(unsigned i=1; i<64; ++i) if(sizesCompBMs[i]<=sizesCompBMs[i-1]) FATAL("!?");
	//#endif
	currentWindow = 0;
	//did=0;
}
//=============================================================================================
void NewIteratorAugmentedBase::setBits(const unsigned bits) {	 //set the bits of did space block
	didBlockBits = bits;
}
//=============================================================================================
void NewIteratorAugmented::handleAWindowOfOnTheFly(const did_t firstDidOfWindow, const did_t nextWindowsFirst, AugmentsHelperInterface* helper) {

	//no postings in this window (happens for instance in: light sabers physics)
	if(firstDidOfWindow>maxDidPerBlock[paddedLength/CONSTS::NUMERIC::BS]) {
		COUTV(5) << scratchPadId << " can't make it: " << firstDidOfWindow << ">" << paddedLength/CONSTS::NUMERIC::BS << ENDL;
		postingsInWindow = 0;

		blockMaxesQuant = blockScoresQuants[scratchPadId] = scratchPadQBMs[scratchPadId];
		memset(blockMaxesQuant,0,CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK);

		postingBitSet = scratchPadPBS[scratchPadId];
		bitSetsPBS[scratchPadId] = (unchar*)scratchPadPBS[scratchPadId];
		memset(postingBitSet,0,CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK);
		((unchar*)postingBitSet)[CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK-1]|=128; //always set the elephant in Cairo

		while (nextWindowsFirst > maxDidPerBlock[compressedBlock+1]) ++compressedBlock;
		realDids = scratchPadDids[scratchPadId];
		realDids[0] = did = nextWindowsFirst;
		realScoresFloat = scratchPadScores[scratchPadId];
		realScoresFloat[0] = 0.0f;
		//offsetInPostingSpace = 0;
		return;
	}

	//find starting block of a window
	const unsigned* blockI = std::lower_bound(maxDidPerBlock,maxDidPerBlock+1+(paddedLength/CONSTS::NUMERIC::BS),firstDidOfWindow);
	unsigned block = blockI - maxDidPerBlock;
	if(block) --block; //once I knew, why it is so...

	compressedBlock = block; //this is the first block of a did-s window in (compressed)postings-space
	assert(block<paddedLength/CONSTS::NUMERIC::BS);

	realDids = scratchPadDids[scratchPadId];
	realScoresFloat = scratchPadScores[scratchPadId];

	//------------------ bring dids and scores ------------------------------
	helper->entireBlockDidsAndScores(this,didsBuffer,scoresBuffer,block++); //special care for first block...

	unsigned i=0;
	while(didsBuffer[i]<firstDidOfWindow) ++i; //skip all those that are not in this window
	assert(i<=CONSTS::NUMERIC::BS); //if we skip more than a block it is flawed!

	//we will have written (CONSTS::NUMERIC::BS - i) postings and would like to be aligned to 16 bytes AFTER
	unsigned scoresGapInWin; //for compensation
	for(scoresGapInWin=0; ((CONSTS::NUMERIC::BS - i + scoresGapInWin)*sizeof(score_t))%16; ++scoresGapInWin) {}
	realScoresFloat += scoresGapInWin; //compensate

	unsigned postings = 0;
	for(; i<CONSTS::NUMERIC::BS; ++i) {
		realScoresFloat[postings]=scoresBuffer[i];
		realDids[postings++]=didsBuffer[i];
	}

	//and now all other blocks -- the most expensive part of this method
	helper->batchUnpackingBlocksUptoDid(this,postings, nextWindowsFirst, realDids, realScoresFloat, block);
	assert(compressedBlock<paddedLength/CONSTS::NUMERIC::BS);

	//refine the last block to exclude postings not in current window:
	while(postings>0 && realDids[postings-1]>=nextWindowsFirst) --postings;
	realDids[postings]=nextWindowsFirst+1; //just in case -- set an elephant. Was a bug in some cases for last window of OTF lists!
	postingsInWindow = postings; //getScores could use this threshold later

	//------------------ block maxes ------------------------------
	blockMaxesQuant = blockScoresQuants[scratchPadId] = scratchPadQBMs[scratchPadId];
	//genMaxesWithPtr(realDids, realScoresFloat, postingsInWindow); //when no quantization required
	//byteMaxesFromFloatScores(realDids,realScoresFloat,blockMaxesQuant, postingsInWindow,firstDidOfWindow, 80.0f); //quantize as you go
	helper->buildBM(this, realDids,(void*)realScoresFloat,(void*)blockMaxesQuant, postingsInWindow,firstDidOfWindow);

	//------------------ PBS --------------------------------------
	bitSetsPBS[scratchPadId] = (unchar*)scratchPadPBS[scratchPadId];
	postingBitSet = scratchPadPBS[scratchPadId];
	if(!(policy==BMQC_FAKE || policy==BMQ_FAKE || policy==BM_FAKE)) 	//don't do this for fakes
		helper->buildPBS_M3(postingBitSet,realDids, firstDidOfWindow, didBlockBits, postingsInWindow); //if PBS are off with #USEPOSTINGS it is O(1) call
	((unchar*)postingBitSet)[CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK-1]|=128; //always set the elephant in Cairo

	did = realDids[0];
	offsetInPostingSpace = 0;
}
//=============================================================================================
void NewIteratorAugmented::preprocessAugmentsForWindow(AugmentsHelperInterface* helper) {	//build the structures for current window
	firstDidOfWindow = currentWindow*(CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK << didBlockBits); //first did of current window
	const did_t nextWindowsFirst = std::min(CONSTS::NUMERIC::MAXD,firstDidOfWindow + (CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK << didBlockBits));
	const unsigned blocksCnt = (currentWindow*CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK);
	//COUTD << currentWindow << " " << policy << ENDL;
	if(policy == ONTHEFLY) {
		handleAWindowOfOnTheFly(firstDidOfWindow,nextWindowsFirst,helper);
		return;
	}

	if(firstDidOfWindow>maxDidPerBlock[(paddedLength/CONSTS::NUMERIC::BS)] ) {//can't make it
		//COUT4 << (" non-otf-can't make it: ") << ENDL;
		blockMaxes = blockScores[scratchPadId] = scratchPadBMs[scratchPadId];
		memset(blockMaxes,0,CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK << 2); //we need 4 times the space for block maxes
	}

	//stopped supporting non-quant block max structures:
	assert(!(policy ==  BM_FAKE || policy == BM_PBS || policy == BM_PBSC));

	//point the BM structure:
	if(policy ==  BMQ_FAKE || policy == BMQ_PBS || policy == BMQ_PBSC) {//we want to dequantize this one:
		NewIteratorAugmented::blockScoresQuants[scratchPadId] = (unchar*) (bmQorC) + blocksCnt;
	}
	else if(policy == BMQC_FAKE || policy == BMQC_PBS || policy == BMQC_PBSC) { //we want to decompress this first:
		unchar *buffer = NewIteratorAugmented::scratchPadQBMs[scratchPadId];
		const unsigned stopAtBlock = nextWindowsFirst >> didBlockBits;

		assert(res.compressedZeros==bmQorC); assert(res.sizesForBMs==sizesCompBMs); assert(res.flagsForBMs==flagsBM);

		decompressQuantized(buffer,stateForBMQC,nonZeroesBase,bmQorC,flagsBM,sizesCompBMs,stopAtBlock);

		NewIteratorAugmented::blockScoresQuants[scratchPadId] = (unchar*) NewIteratorAugmented::scratchPadQBMs[scratchPadId];
	}

	//point the PBS structure
	if(policy==BMQC_FAKE || policy==BMQ_FAKE || policy==BM_FAKE) {
		bitSetsPBS[scratchPadId] = (unchar*)fakePostingBitSet;
		postingBitSet = fakePostingBitSet;
	}
	else if(policy == BM_PBSC || policy == BMQ_PBSC || policy == BMQC_PBSC) { FATAL("no support"); }
	else {
		postingBitSet = (unsigned*)( (currentWindow*CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK) + ((unsigned char*)postingBitSetBase));
		bitSetsPBS[scratchPadId] = (unchar*) postingBitSet;
	}
}

//=============================================================================================
void NewIteratorAugmented::initScratchpads() {
	//assuming the bits were set already...
	for(unsigned i=0; i<MAX_TERMS_IN_QUERY; ++i) {
		NewIteratorAugmented::scoresBuffer = NEWM score_t[CONSTS::NUMERIC::BS];
		NewIteratorAugmented::scratchPadPBS[i] = NEWM unsigned[CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK/sizeof(unsigned)];
		NewIteratorAugmented::scratchPadBMs[i] = NEWM score_t[CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK*sizeof(score_t)];
		NewIteratorAugmented::scratchPadDids[i] = NEWM did_t[CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK<<didBlockBits];
		NewIteratorAugmented::scratchPadScores[i] = NEWM score_t[(1+CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK)<<didBlockBits];
		NewIteratorAugmented::scratchPadQBMs[i] = NEWM unchar[CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK];
		NewIteratorAugmented::liveDeadArea = NEWM unsigned[CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK]; //fits both version with/without postings
	}

	unsigned char* fakePostingBitSetp = NEWM unsigned char[CONSTS::NUMERIC::MAXD>>3]; //more than enough
	memset(fakePostingBitSetp,255,CONSTS::NUMERIC::MAXD>>3);
	fakePostingBitSet = reinterpret_cast<unsigned*>(fakePostingBitSetp);
}

//=============================================================================================
PreprocessingData NewIteratorAugmented::buildInitialAugnemtsData(AugmentsHelperInterface* helper) {
	PreprocessingData res;
	if(policy == NONE || policy == ONTHEFLY) return res; //nothing to do for those

	//calculate sizes:
	const unsigned windows = (CONSTS::NUMERIC::MAXD>>didBlockBits)/CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK;
	const did_t docsInWindow = CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK<<didBlockBits;
	const unsigned SIZE = (((CONSTS::NUMERIC::MAXD>>didBlockBits)>>CONSTS::NUMERIC::CACHE_FRIENDLY_BITS)+1)<<CONSTS::NUMERIC::CACHE_FRIENDLY_BITS;
	const unsigned chunkBytes = CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK;

	//allocate space:
	blockMaxes = blockMaxesBase = NEWM score_t[SIZE];
	res.quantBlockMaxes = NEWM unchar[SIZE];
	if(policy==BMQC_FAKE || policy==BMQ_FAKE || policy==BM_FAKE)
		res.postingBitSet = fakePostingBitSet; //make sure nobody deletes this one ;)
	else
		res.postingBitSet = NEWM unsigned[SIZE]; //TODO: check overallocation -- we need that much bytes, not unsigneds!

	//NOTE: those windows don't have to be (and in fact aren't) the same windows we are using in qp
	//build windows and copy augments over
	unsigned w=0;
	for(; w<windows+1; ++w) { //note the +1 !!
		handleAWindowOfOnTheFly(w*docsInWindow, std::min(CONSTS::NUMERIC::MAXD,(w+1)*docsInWindow), helper);
		memcpy(res.quantBlockMaxes+(w*chunkBytes), blockMaxesQuant,chunkBytes);
		if(!(policy==BMQC_FAKE || policy==BMQ_FAKE || policy==BM_FAKE)) {
			//COUTL << (w*chunkBytes) << " " << (w*chunkBytes)*8*(1<<(didBlockBits-3)) << " " << countSetBits(postingBitSet,chunkBytes)<<"\n";
			memcpy(((unchar*)res.postingBitSet)+(w*chunkBytes),postingBitSet,chunkBytes); //maybe we have wrong pbs for last window, but it is padding anyway
		}
	}

	if(policy == BMQC_FAKE || policy == BMQC_PBS || policy == BMQC_PBSC) { //we want to compress this one:
		compressQuantized(res.quantBlockMaxes,SIZE,res);
		DEL res.quantBlockMaxes;  //don't need it in this case
	}

	DEL blockMaxes; //don't need it in this case

	// we need to think how to get those...
	//res.maxScoreOfListFloat = *std::max_element(blockMaxesBase,blockMaxesBase+paddedLength/CONSTS::NUMERIC::BS);

	resetPostingIterator(); //just in case
    return res;
}
//=============================================================================================
inline void NewIteratorAugmented::buildBM(const did_t* docIds, const unchar* scores, unchar* blockMaxes, const unsigned size, const did_t smallest) const {
	memset(blockMaxes,0,CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK);
	typedGenMaxesWithPtr(docIds,scores,blockMaxes,size,smallest);
}
//=============================================================================================
//byteMaxesFromFloatScores
inline void NewIteratorAugmented::buildBM(const did_t* docIds, const float* scores, unchar* blockMaxes, const unsigned size, const did_t smallest) const{
	memset(blockMaxes,0,CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK);
	unchar dummy; //calculate block maxes
	for(size_t i=0; i<size; ++i) {
		const unchar scoreI = quantF_(scores[i]);
		const did_t did = docIds[i];
		unchar* buf[2] = {&dummy,blockMaxes+((did-smallest)>>didBlockBits)};
		*(buf[(scoreI > *(buf[1]))]) = scoreI;
	}
}
//=============================================================================================
template <typename T>
inline void NewIteratorAugmentedBase::typedGenMaxesWithPtr(const did_t* docIds, const T* scores, T* blockMaxes,  const unsigned size, const did_t smallest) const {
	//assume blockMaxes are initialized
	T dummy;
	for(size_t i=0; i<size; ++i) {
		const T scoreI = scores[i];
		//assert(scoreI<assrtBound);
		const did_t did = docIds[i]-smallest;
		T* buf[2] = {&dummy,blockMaxes+(did>>didBlockBits)}; 
		*(buf[(scoreI > *(buf[1]))]) = scoreI;
	}	
}
//=============================================================================================
void NewIteratorAugmented::genMaxesWithPtr(const did_t* docIds, const score_t* scores, const unsigned size, const did_t smallest) {
	typedGenMaxesWithPtr(docIds, scores, blockMaxes, size, smallest);
}
#endif //use augments...
//=============================================================================================
template <typename Q>
unsigned NewIteratorAugmentedBase::compressQuantized(const Q* quantized, const unsigned sz, PreprocessingData& prData) {
	//assuming sz is the padded and aligned version
	std::vector<unchar> nonZeroes;
	vecUInt skips;
	nonZeroes.reserve(sz);
	skips.reserve(sz);
	unsigned skipsCount=0;
	for(unsigned s=0; s<sz; ++s) {
		if(quantized[s]) {
			if(skipsCount)
				skips.push_back(skipsCount);
			skips.push_back(0);
			skipsCount = 0;
			nonZeroes.push_back(quantized[s]); //maybe conversion from short to unchar
		}
		else
			++skipsCount;
	}
	//pad
	while(skips.size()%CONSTS::NUMERIC::BS) { skips.push_back(1); }
	skips.push_back(1);	
	//pad more
	while(skips.size()%CONSTS::NUMERIC::BS) { skips.push_back(1); }
	
	#ifdef DEBUG
	unsigned offDst =0;
	unsigned nzOff = 0;
	std::vector<unchar> dstR(sz); 
	for(unsigned j=0; j<sz && offDst<sz; ++j)
                if(0 == skips[j]) {
			dstR[offDst++] = nonZeroes[nzOff++];
		}
                else
                        offDst+=skips[j];
	for(unsigned j=0; j<(CONSTS::NUMERIC::MAXD>>6); ++j)
		if(quantized[j]!=dstR[j]) { 
			COUTD << j << ": " << (int)quantized[j]<<"!="<<(int)dstR[j]<<ENDL;
			//if(j < 300123) FATAL("!"); 
		}
	COUTD << "Tested: " << sz << ENDL;
	#endif

	//now we have decomposed all the nonzeroes and the skips. we need to blockwise compress the skips:
	unsigned* compressedData = NEWM unsigned[sz]; //tmp destination vector
	unsigned  *dstPtr = compressedData;

	vecUInt flags;
	vecUInt offsets;

	flags.reserve(sz/CONSTS::NUMERIC::BS);
	offsets.reserve(sz/CONSTS::NUMERIC::BS);
	offsets.push_back(0);
	const unsigned skipsSz = (skips.size()/CONSTS::NUMERIC::BS +1)*CONSTS::NUMERIC::BS;
	OldPforDelta pfor;
	for(unsigned i=0; i<skipsSz; i+=CONSTS::NUMERIC::BS) {
		/*
		int flag = -1; //this is stupid ! -- we should use the trick of countBitsToPack
		for(int j=0; flag<0; ++j)
			flag = pack_encode(&dstPtr,&skips[i],j);
			*/

		std::pair<int,unsigned> packMeta = pfor.compressInts(&skips[i],dstPtr,CONSTS::NUMERIC::BS);
		dstPtr += packMeta.second;
		flags.push_back(packMeta.first);

		assert((dstPtr - compressedData)-offsets.back() <= 64);
		offsets.push_back(dstPtr-compressedData);
	}

	prData.compressedZeros = compressedData;
	prData.flagsForBMs = NEWM unsigned[flags.size()];
	std::copy(flags.data(),flags.data()+flags.size(),prData.flagsForBMs); //better to create new vector and swap to it
	prData.sizesForBMs = NEWM unsigned[offsets.size()];
	std::copy(offsets.data(),offsets.data()+offsets.size(),prData.sizesForBMs); //better to create new vector and swap to it
	prData.nonZeroBlockMaxes = NEWM unchar[nonZeroes.size()];
	std::copy(nonZeroes.data(),nonZeroes.data()+nonZeroes.size(),prData.nonZeroBlockMaxes); //better to create new vector and swap to it

#ifdef DEBUG
	/*
	for(unsigned i=1; i<offsets.size(); ++i)
		if(prData.sizesForBMs[i]<=prData.sizesForBMs[i-1]) FATAL("?");
	std::vector<unsigned> skipR;
	unsigned skipsBuf[CONSTS::NUMERIC::BS];
	for(unsigned chunk=0; chunk < flags.size(); ++chunk) {
		pack_decode_freqs(skipsBuf,compressedData+offsets[chunk],flags[chunk]);
		for(unsigned k=0; k<CONSTS::NUMERIC::BS; ++k)
			skipR.push_back(skipsBuf[k]);
	}
        for(unsigned j=0; j<skips.size(); ++j)
                if(skips[j]!=skipR[j]) FATAL("!");
        COUTD << "Tested2: " << sz << ENDL;

*/
#endif

	//sizes in bytes!
	const unsigned skipsCompressedSize = offsets.back();
	const unsigned nonZeroesSize = nonZeroes.size();
	const unsigned bitsSize = flags.size();
	unsigned compressedSize = skipsCompressedSize + bitsSize + nonZeroesSize;
	//COUTD << "flags.size() " << flags.size() << " skips size: " << skips.size() << " nonZeroesSize "  <<  nonZeroesSize << ENDL;
	//COUTD << compressedSize << " " << nonZeroesSize << "+" << skipsCompressedSize << " ->" << skipsSz << ENDL;
	//COUT << compressedSize << ENDL;
	return compressedSize;
}
//=============================================================================================
void NewIteratorAugmentedBase::decompressQuantized(unchar* dst, ktuple<unsigned,4>& offsets,
									const unchar* nonZeroBlockMaxes, unsigned* compressedZerosBase,
									const unsigned* flagsForBMs, unsigned* sizesForBMs, const unsigned lastBlockMax) {
	//note: that as the offsets (size) are absolute, here we must have the base ptr to compressedZeros, while the sizesForBMs is a relative to a window
	//offsets[0] => offset in nonZeroes where we stopped
	//offsets[1] => current compressed block for skips
	//offsets[2] => offset in last decompressed
	//offsets[3] => offset in dst buffer
	static unsigned skipsBuf[CONSTS::NUMERIC::BS];
	unsigned offDst = offsets[3];
	unsigned j=0;
	memset(dst,0,CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK);
	//special case: we skipped an entire window (or more)
	//we must skip to this place, but without writing to mem
	if(offDst>lastBlockMax) { //then the window (droids) you are looking for is all 0 anyway
		COUTD << "lastblock: "  << lastBlockMax << " offDst: " << offDst <<" <=" << (int)lastBlockMax-(int)offDst << ENDL; 
                return;

	}
	if(lastBlockMax-offDst>CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK){  //we skipped window!
		COUTL << lastBlockMax << "-" << offDst <<"=" << lastBlockMax-offDst << ENDL; 
		FATAL("^");
	}

	OldPforDelta unpacker;
	//now continue with the next blocks...
	while(offDst<lastBlockMax) {
		j = offsets[2]; //get the offset from either prev. iteration or last call
		packDecodeFreqs(skipsBuf,compressedZerosBase+sizesForBMs[offsets[1]],flagsForBMs[offsets[1]],&unpacker); //decompress to tmp buffer
		offsets[1]+=1; //inc compressed block
		for(;j<CONSTS::NUMERIC::BS && offDst<lastBlockMax;j++) {
			if(0 == skipsBuf[j]) { 
				//DEPRECATED!!! note: as dequant expects to get unsigned vals we inflate the byte to 4
				dst[offDst%CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK] = nonZeroBlockMaxes[offsets[0]++];
				++offDst;
			}
			else
				offDst+=skipsBuf[j];
			//COUTD << j << ": " << skipsBuf[j] << " " << offDst << ENDL;
		}
		offsets[2]=0; //reset j for next iteration
	}

	//COUTD << "j: " << j << " skipsBuf[j]: " << skipsBuf[j] << " skipsBuf[j+1]: " << skipsBuf[j+1] << " nz: " << (int)nonZeroBlockMaxes[offsets[0]] << ENDL;
	//special case: prev. window was a skip over window boundary
	offsets[3] = offDst;//-lastBlockMax;
	offsets[1]-=1;//get back one block
	offsets[2]=j;//register leftovers of it
}
//=============================================================================================
