/*
 * globals.cpp
 *
 *  Created on: Feb 17, 2012
 *      Author: sergeyn
 */



#include "globals.h"
#include <cmath>

unsigned CONSTS::NUMERIC::CACHE_FRIENDLY_BITS(12);
unsigned CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK(1<<CACHE_FRIENDLY_BITS); //in bytes
unsigned CONSTS::NUMERIC::MAXD(0);

unsigned globalVerbosity;

//this is terrible and silly!
bool isNumber(const STRING& str) {
	if (str.size() == 0) return false;
	unsigned i = str[0]=='-'? 1 : 0;
	bool dotFound=false, digFound=false;
	for (; i<str.size(); ++i)
	{
		if (str[i]=='.') { if (dotFound) return false; dotFound=true; }
		else if (str[i]>'9' || str[i]<'0') return false;
		else digFound=true;
	}
	return digFound;
}
