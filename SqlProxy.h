#ifndef _SQLPROXY_H_
#define _SQLPROXY_H_

#include <vector>
#include <iostream>
#include <algorithm>

#include "globals.h"
#include "profiling.h"
#include "sql/sqlite3.h"

										//"CREATE  TABLE  IF NOT EXISTS 'terms'('term' TEXT PRIMARY KEY  NOT NULL  UNIQUE, 'flagB' BLOB, 'maxB' BLOB, 'minB' BLOB, 'scoreB' BLOB, 'sizeB' BLOB)"
const static std::string termTableSchema("CREATE  TABLE  IF NOT EXISTS 'terms'('tid' INTEGER PRIMARY KEY  NOT NULL  UNIQUE, 'term' TEXT, 'compFlags' BLOB, 'maxDidOfBlocks' BLOB, 'compSizes' BLOB, 'maxScoreOfList' FLOAT, 'unpaddedLen' INTEGER)");

class SqlProxy {
	sqlite3 *db;
	sqlite3_stmt *ppStmt;
	std::string dbname;

public:
	sqlite3_stmt * prepare(const std::string& select) {
		const char *zSqlSelect = select.c_str();
		if( sqlite3_prepare_v2(db, zSqlSelect, -1, &ppStmt, NULL) != SQLITE_OK )  {
			CERR << "db error: " << sqlite3_errmsg(db) << ENDL;
			sqlite3_close(db);
			ppStmt = 0;
			return 0;
		}

		return ppStmt;
	}


	SqlProxy(std::string path) :db(0), ppStmt(0), dbname(path) {
		if(sqlite3_open(dbname.c_str(), &db) )
			FATAL("Can't open database: "+dbname);

		doInsertOrCreate(termTableSchema);
	}

	~SqlProxy() {
		sqlite3_exec(db, "END", NULL, NULL, NULL);
		sqlite3_close(db);
	}

	sqlite3 * getDB() { return db; }

	bool executeStoredStatement(bool doClose=false) {
		int rc = sqlite3_step(ppStmt);
		sqlite3_finalize(ppStmt);

		if(doClose) {
			sqlite3_exec(db, "END", NULL, NULL, NULL);
			sqlite3_close(db);
		}
		return rc == SQLITE_OK;
	}

	bool doInsertOrCreate(const std::string& query, bool doClose=false) {
		const char *zSql = query.c_str();
		if( sqlite3_prepare_v2(db, zSql, -1, &ppStmt, NULL) != SQLITE_OK )  {
			CERR << "db error: " << sqlite3_errmsg(db) << ENDL;
			sqlite3_close(db);
			ppStmt = 0;
			return false;
		}

		int rc = sqlite3_step(ppStmt);
		sqlite3_finalize(ppStmt);


		if(doClose) {
			sqlite3_exec(db, "END", NULL, NULL, NULL);
			sqlite3_close(db);
		}
		return rc == SQLITE_OK;
	}

};
//=============================================================================================
template <typename T>
static void getBlob(sqlite3_stmt* ppStmt, int cid, std::vector<T>& v) {
	if(sqlite3_column_type(ppStmt, cid) != SQLITE_BLOB)
		return;
	const int size = sqlite3_column_bytes(ppStmt, cid) / sizeof(T);
	const T* src = (const T*) sqlite3_column_blob(ppStmt, cid);
	v.clear();
	v.reserve(size);
	for(int i=0; i<size; ++i)
		v.push_back(src[i]);
}
//=============================================================================================
#endif
