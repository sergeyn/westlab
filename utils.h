#ifndef UTILS_H_
#define UTILS_H_

#include <limits>
#include "globals.h"
#include <algorithm>

#include "Interfaces.h"

// Knuth trick for comparing floating numbers
inline bool FloatEquality( float a, float b);
inline bool FloatComparisonPrivate( float a, float b);

// Arguments a,b and returns a) 0 in case of equality, b) 1 in case a > b, and c) -1 in case a < b
inline int Fcompare( float a, float b);

inline uint32_t intlog2(const uint32_t x);

inline unsigned int NearestPowerOf2( unsigned int x );

template <typename T> //stream vectors
std::ostream& operator<< (std::ostream& s, const std::vector<T>& t);

std::string getTimeString();

template <typename T>
inline T max3(const T& a, const T& b, const T& c);

template <typename T>
inline T min3(const T& a, const T& b, const T& c);

int readline(FILE* fd, char* line); //TODO: rewrite all this parsing nightmare //return -1 means finish, 1 means valid


extern unsigned allocatedSum;
inline void NEWMA(void** ptr,const unsigned bytecounts);

#define NEWM new
#define DEL delete[]
#define DELA delete[]

//instead of splitting in middle as std::lower_bound starts with begin and hops in powers of two
// -- better for cases where the value of interest is mostly located near begin rather than uniformly distrib.
template<typename _FIter, typename _Tp>
_FIter gallop_up_lower_bound(_FIter begin, _FIter end, const _Tp& val);

//instead of splitting in middle as std::lower_bound starts with begin and hops in powers of two
// -- better for cases where the value of interest is mostly located near begin rather than uniformly distrib.
//this is not the same convention as std:: version -- we need it for nextGEQing
template<typename _FIter, typename _Tp>
_FIter gallop_down_lower_bound(_FIter begin, _FIter curr, const _Tp& val);


template <typename Iterator>
long int appendToFile(const std::string& path, Iterator start, Iterator end);

unsigned countSetBits(const void* ptr, unsigned sizeInBytes);

inline void setBit(void* ptr, const unsigned bitNumber) {
	(reinterpret_cast<unsigned char*>(ptr))[bitNumber>>3] |= (1<<(bitNumber&7)); //turn on the one bit to set in the right byte
}

inline unsigned	getNextSetBit(const void* ptrBS, const unsigned index);

//branch optimization
#define unlikely(x)     __builtin_expect((x),0)
#define likely(x)     __builtin_expect((x),1)


//-------------------------------------------------------------------
template <typename S>
struct QpResult {
	unsigned int did;
	S score;

	inline QpResult<S>& operator=(const QpResult<S>& rhs) {
	        did=rhs.did;
	        score=rhs.score;
        	return *this;
	}

	inline void setInvalid() { did = CONSTS::NUMERIC::MAXD + 1; score = score-score; }
	inline bool isInValid() const { return did>CONSTS::NUMERIC::MAXD; }
	inline bool operator>(const QpResult<S>& rhs) const { return score>rhs.score; }
	inline bool operator<(const QpResult<S>& rhs) const  { return score<rhs.score; }

	inline QpResult() { setInvalid(); }
	inline QpResult(unsigned int d, S s) : did(d), score(s){}
	inline void setR(unsigned int d, S s){ did=d; score=s;}
};

//-------------------------------------------------------------------
template<class T>
class PriorityArray{
	unsigned size;
	std::vector<T> minData;

public:
	PriorityArray(const size_t sz=CONSTS::NUMERIC::TOPK);
	inline void setSizeLimit(unsigned sz);
	inline const std::vector<T>& getV() const;
	inline void putInArray(T* ptr);
	inline const T& fasthead() const;
	inline const T safehead() const;
	inline void sortData();
	inline bool push(const T& val);
	inline bool push(const void* val);
};
//-------------------------------------------------------------------
class Registry {
	hashMapStd<std::string,const RegistryEntry*> registry;
	void registerClass(const RegistryEntry* newClass);

public:
	Registry() {}
	~Registry();

	template<typename T>
	void registerClass();

	template<typename T>
	T* getInstanceOf(const std::string& className);
};
//-------------------------------------------------------------------
template <typename T, unsigned k>
struct ktuple {
	T tuple[k];
	ktuple(){}
	ktuple(T arr[k]) { for(unsigned i=0; i<k; ++i) tuple[i]=arr[i];}
	inline const T& operator[](unsigned i) const { return tuple[i];}
	inline T& operator[](unsigned i) { return tuple[i];}
};
//-------------------------------------------------------------------

#include "utilsIMP.h"

#endif /* UTILS_H_ */
