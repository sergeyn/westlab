#ifndef AUGMENTS2_H_
#define AUGMENTS2_H_

#include "Interfaces.h"


class AugmentsForIntersectionSSE : public AugmentsHelperInterface, public RegistryEntry {
public:
	AugmentsForIntersectionSSE(){}

	virtual const std::string name() const;
	virtual void* getOne() const;

	virtual void buildLivenessBitvector(unchar* dest, void** blockScores, unchar** postingBitsets, const unchar threshold, const unsigned termsCount, const unsigned windowsize);
	virtual void buildPBS_M3(unsigned* dest, const unsigned* dids, const unsigned winStartDid, unsigned bits, const unsigned sz);
	virtual void buildBM(LowLevelListAccessorInterface* list, const did_t* docIds, const void* scores, void* blockMaxes, const unsigned size, const did_t smallest);

	virtual void entireBlockDidsAndScores(LowLevelListAccessorInterface* list, did_t* dids, void* scores, const did_t block) const; //decode given block and calc scores
	virtual void batchUnpackingBlocks(LowLevelListAccessorInterface* list, did_t endBlock, did_t* dids, void* scores, did_t startBlock=0) const;
	virtual void batchUnpackingBlocksUptoDid(LowLevelListAccessorInterface* list, did_t& postings, did_t nextWindowsFirst, did_t* realDids, void* scores, did_t& block) const;
};


#endif /* AUGMENTS2_H_ */
