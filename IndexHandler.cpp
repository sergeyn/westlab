#include <fstream>
#include <stdio.h>

#include "IndexHandler.h"
#include "SqlProxy.h"
#include "utils.h"


#include "globals.h"
#include "NewIterator.h"


//returns the correct order of terms in an inverted file
std::vector<std::string> SqliteBasedLexicon::loadMeta(){
	SqlProxy ld(sqlFilename);
	sqlite3_stmt* ppStmt = ld.prepare("select * from terms order by tid");
	if(! ppStmt) FATAL("select failed");

	std::vector<std::string> centry;
	centry.reserve(1<<14);

	//tid,term,compFlags,maxDidOfBlocks,compSizes,maxScoreOfList
	while (sqlite3_step(ppStmt) == SQLITE_ROW) {  // For each row returned
		tid_t tid = sqlite3_column_int(ppStmt, 0);

		const std::string term(std::string((const char*)sqlite3_column_text(ppStmt, 1)));
		ListMetaData& c = termMetaInfo[term];
		c.setName(term);
		c.setTid(tid);
		//c.metaInfoLength = sqlite3_column_bytes(ppStmt, 2) / sizeof(unsigned int);

		vecUInt flags;
		getBlob(ppStmt,2,flags);//c.getFlagsReference()
		c.swapFlags(flags);

		vecUInt maxdids;
		getBlob(ppStmt,3,maxdids);
		c.swapMaxDidsPerBlock(maxdids);

		vecUInt sizes;
		getBlob(ppStmt,4,sizes); //c.getSizePerBlockReference()
		c.swapSizePerBlock(sizes);

		//c.setmaxScoreOfList = sqlite3_column_double(ppStmt,5); //not a generic info. sorry
		c.setUnpaddedLength(sqlite3_column_int(ppStmt,6));
		centry.push_back(c.getTerm());

	} //end while
	sqlite3_finalize(ppStmt);
	return centry;
}
//=============================================================================================
inline const void* SqliteBasedLexicon::getConstListHandleUnsafe(const std::string& term){
	if(termMetaInfo.count(term) && termMetaInfo[term].getUnpaddedLength()<3 ) COUT4 << term  << " needs support of lists shorter than 3 in index" << ENDL;

	 // need support of lists shorter than 3 in index for 1k query:
	 // xl1200; kerwan; 4000pt; colonostomy;

	return termMetaInfo.count(term) ?  &termMetaInfo[term] : nullptr;
}

void SqliteBasedLexicon::loadLexicon(const std::vector<std::string>* terms, bool calcAugments) {
	//NOTE:: the only way to maintain correct offsets is to calculate them in the order of tid -- as this is the order the terms are stored in file
	//hence we use the names from loadMeta -- to enforce the order
	std::vector<std::string> data;
	if(nullptr == terms) {
		data = loadMeta();
		terms = &data;
	}
	unsigned long offset = 0;

	for(const std::string& name : *terms) {
		ListMetaData& list = termMetaInfo[name];
		list.setOnDemandManager(onDemandManager);

		if(calcAugments) {
			//because we are prefix summing and inserting it is not an overhead to have a copy
			vecUInt sizePerBlock;
			sizePerBlock.push_back(0); //push 0 to make decompression sensible
			const vecUInt& srcSizes = list.getSizePerBlock();
			sizePerBlock.reserve(srcSizes.size()+1);
			for(unsigned spb : srcSizes)
				sizePerBlock.push_back(sizePerBlock.back()+spb+1); // +1? Shuai!!!
			list.setCompressedDataLengthBytes(sizePerBlock.back()*sizeof(unsigned));
			list.swapSizePerBlock(sizePerBlock); //return the data back to list

			vecUInt maxdids;
			list.swapMaxDidsPerBlock(maxdids);
			assert(!(maxdids.empty() || 0 == *maxdids.begin()));
			maxdids.insert(maxdids.begin(),0); //push 0 to make decompression sensible
			maxdids.push_back(CONSTS::NUMERIC::MAXD+256); //push MAXD to make decompression sensible
			list.swapMaxDidsPerBlock(maxdids);
		}
		else {
			long unsigned sums = 0;
			for(const unsigned spb : list.getSizePerBlock())
						sums += (spb+1); // +1? Shuai!!!
			list.setCompressedDataLengthBytes(sums*sizeof(unsigned));
		}

		list.setOffsetInFile(offset);
		offset+=list.getCompressedDataLengthBytes(); // be extremely careful with this part!


		//COUTD << list.term << " " << termMetaInfo[list.term].compressedDataLengthBytes << ENDL;
	}

	COUT4 << "Loaded from sql: " << termMetaInfo.size() << " meta-items" << ENDL;
}
//=============================================================================================
//returns the correct order of terms in an inverted file
std::vector<std::string> OldStyleSqliteBasedLexicon::loadMeta(const std::string& sqlPath, const std::string& where){
	SqlProxy ld(sqlPath);
	sqlite3_stmt* ppStmt = ld.prepare("select * from terms "+where);
	if(! ppStmt) FATAL("select failed");

//	0|term|TEXT|1||1  //	1|flagB|BLOB|0||0 //	2|maxB|BLOB|0||0 //	3|minB|BLOB|0||0  //	4|scoreB|BLOB|0||0 //	5|sizeB|BLOB|0||0
	std::vector<std::string> centry;
	centry.reserve(1<<14);

	while (sqlite3_step(ppStmt) == SQLITE_ROW) {  // For each row returned
		const std::string term(std::string((const char*)sqlite3_column_text(ppStmt, 0)));

		ListMetaData& c = termMetaInfo[term];
		c.setName(term);

		vecUInt flags;
		getBlob(ppStmt,1,flags);//c.getFlagsReference()
		c.swapFlags(flags);

		vecUInt maxdids;
		getBlob(ppStmt,2,maxdids);
		c.swapMaxDidsPerBlock(maxdids);

		vecUInt sizes;
		getBlob(ppStmt,5,sizes); //c.getSizePerBlockReference()
		c.swapSizePerBlock(sizes);
		centry.push_back(term);

	} //end while
	sqlite3_finalize(ppStmt);
	return centry;
}
//=============================================================================================
void OldStyleSqliteBasedLexicon::loadLexicon(const std::vector<std::string>* terms, bool) {
	//load basic table -- gives the order and the lengths of terms
	std::ifstream basicTable(termLengthFileName);
	std::string termB; did_t lengthB;

	termsToUintMap lengths;
	while(basicTable.good()) {
		basicTable >> termB >> lengthB;
		lengths[termB]=lengthB;
	}

	std::vector<std::string> data = loadMeta(sqlFilename);

	for(const std::string& name : data) {
		ListMetaData& list = termMetaInfo[name];
		list.setUnpaddedLength(lengths.at(name));

		list.setOnDemandManager(onDemandManager);

		//because we are prefix summing and inserting it is not an overhead to have a copy
		vecUInt sizePerBlock;
		sizePerBlock.push_back(0); //push 0 to make decompression sensible
		const vecUInt& srcSizes = list.getSizePerBlock();
		sizePerBlock.reserve(srcSizes.size()+1);
		for(unsigned spb : srcSizes)
			sizePerBlock.push_back(sizePerBlock.back()+spb+1); // +1? Shuai!!!
		list.setCompressedDataLengthBytes(sizePerBlock.back()*sizeof(unsigned));
		list.swapSizePerBlock(sizePerBlock); //return the data back to list

		vecUInt maxdids;
		list.swapMaxDidsPerBlock(maxdids);
		assert(!(maxdids.empty() || 0 == *maxdids.begin()));

		maxdids.insert(maxdids.begin(),0); //push 0 to make decompression sensible
		maxdids.push_back(CONSTS::NUMERIC::MAXD+256); //push MAXD to make decompression sensible
		list.swapMaxDidsPerBlock(maxdids);
	}

	COUT4 << "Loaded from old-style sql: " << termMetaInfo.size() << " meta-items" << ENDL;
}
//=============================================================================================
indexHandler::indexHandler(const std::string& doclens, const unsigned maxd, LexiconInterface* lex)
: lexicon(lex),doclensPath(doclens),maxD(maxd) {
	lexicon->loadLexicon();
	loadDocLengths();
	//const unsigned STORAGE_CAPACITY(3000);
}
//=============================================================================================
void indexHandler::loadDocLengths() {
	documentLength.resize(maxD);

	FILE *fdoclength = fopen64(doclensPath.c_str(),"r");
	if( fdoclength == NULL) FATAL(" doc length file is missing: " + doclensPath);

	if( fread(documentLength.data(),sizeof(unsigned), maxD, fdoclength) != maxD )
		FATAL("wrong doc length when processing " + doclensPath);

	fclose(fdoclength);

	for(int i =0;i<128 ; i++)
		documentLength[maxD + i] = maxD/2; //is the black magic to mess with BM25?

	COUT4 << "Loaded " << maxD << " doclens" << ENDL;
}
//=============================================================================================
std::vector<const ListMetaData*> indexHandler::operator[] (const std::vector<std::string>& terms){
	std::vector<const ListMetaData*> lps(terms.size());

	// note: this one might break when query has duplicate terms -- but this is wrong anyway :)
	for(size_t i=0; i<terms.size(); i++) {
		if(0 == lexicon->getListLength(terms[i])) FATAL("lacking term: " + terms[i]);
		//this is terrible...
		const ListMetaData* lpsi = reinterpret_cast<const ListMetaData*>(lexicon->getConstListHandleUnsafe(terms[i]));
		lps[i]=lpsi;
		assert(terms[i]==lpsi->getTerm() && ! lpsi->getMaxDidPerBlock().empty());
	}

#ifdef USEQUANTINDEX
	//		//load lookup table
	//		ifstream lookupF((CONSTS::trecRoot + "quant/" + word_l[i]).c_str());
	//		float val;
	//		unsigned q=0;
	//		while(lookupF.good()) {
	//			lookupF >> val;
	//			lpsi->lookup[q++]=val;
	//		}
#endif

	return lps;
}
//=============================================================================================
static void readChunkFromFile(const std::string& fname, long int offset, unsigned sizeBytes, char* buffer) {
	std::ifstream ifs(fname, std::ios::in | std::ios::binary);
	ifs.seekg (offset, std::ios::beg);
	ifs.read(buffer,sizeBytes);
	ifs.close();
}
//=============================================================================================
void BringRawFromFile::bring(const std::string& term, std::vector<unsigned>& dest) {
	const did_t compressedDataLengthBytes = lexicon->getRawSize(term);
	if(! compressedDataLengthBytes) FATAL("lacking term: " + term);

	const long int offsetInFile = lexicon->getLocation(term);
	assert(compressedDataLengthBytes && compressedDataLengthBytes<4*CONSTS::NUMERIC::MAXD);
	dest.resize(compressedDataLengthBytes/sizeof(unsigned));
	readChunkFromFile(filename,offsetInFile,compressedDataLengthBytes,(char*)dest.data());
}
//=============================================================================================
unsigned int readEntireFileToTmpBuffer(const char* fname, unsigned int* buffer){
        FILE* fin = fopen(fname,"r");
        if(fin == NULL) FATAL("couldn't open " +std::string(fname));

        unsigned int readInts = 0;
        while( fread(&(buffer[readInts]), sizeof(unsigned int), 1, fin) == 1 )
                ++readInts;

        fclose(fin);
        return readInts;
}
//=============================================================================================
void BringRawFromManyFiles::bring(const std::string& term, std::vector<unsigned>& dest) {

	const std::string in(pathToPool+term);

	static unsigned* temp_store = nullptr;
	if(nullptr == temp_store) temp_store = NEWM unsigned[CONSTS::NUMERIC::MAXD];

	size_t size = readEntireFileToTmpBuffer(in.c_str(),temp_store);
	dest.insert(dest.begin(),temp_store,temp_store+size);
}


