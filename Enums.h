#ifndef ENUMS_H_
#define ENUMS_H_

#include <string>
#include <ostream>
namespace enm {
	//NOTE don't use the word e-n-u-m in comments - the auto parser isn't terribly smart
	enum prprPolicy {ONTHEFLY, BM_FAKE, BM_PBS, BM_PBSC, BMQ_FAKE, BMQC_FAKE, BMQ_PBS, BMQC_PBS, BMQ_PBSC, BMQC_PBSC, NONE };
	enum counters { COMPLETEQP, SLOWNEXTGEQ, FASTNEXTGEQ, NEXTGEQ,NEXTGEQ_CHEAP, EVALUATION, HEAPIFY, CALC_BLOCK_SCORE,PROC_AUGMENTS_LST, PROC_AUGMENTS_ALL, LA_BUILD, TEST1, TEST2, NOP };

}

void convert(const std::string& str, enm::prprPolicy& p);
std::string convert(const enm::prprPolicy p);
void convert(const std::string& str, enm::counters& p);
std::string convert(const enm::counters p);

template <typename T>
inline std::ostream& enmPrn(std::ostream& s, const T& val) {s<<convert(val); return s; }

inline std::ostream& operator<< (std::ostream& s, const enm::prprPolicy& v) { return enmPrn(s,v); }
inline std::ostream& operator<< (std::ostream& s, const enm::counters& v) { return enmPrn(s,v); }

#endif /* ENUMS_H_ */
