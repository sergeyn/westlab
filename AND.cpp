#include "AND.h"

did_t intersection[25123123];
uint64_t intersectionBitSet[25123123];

did_t naiveIntersect(NewIteratorAugmented** lists, const unsigned termsCount) {
	did_t biggestCandidateDid = 0;
	did_t countHits = 0;

	if(termsCount == 0) return CONSTS::NUMERIC::MAXD;

	if(termsCount == 1) {

		//todo:: just decompress all blocks -- it will be faster!
		did_t did = lists[0]->nextGEQ<0>(0);
		while(did<CONSTS::NUMERIC::MAXD) {
			intersection[countHits++] = did;
			did = lists[0]->nextGEQ<0>(did+1);
		}
		return countHits;
	}
	//else: more than one list
	while (biggestCandidateDid<CONSTS::NUMERIC::MAXD) {
		bool aligned = true;

		//mv first list and compare
		biggestCandidateDid = std::max(biggestCandidateDid, lists[0]->nextGEQ<0>( biggestCandidateDid ));
		unsigned i=1;
		for (; i<termsCount && aligned && biggestCandidateDid<CONSTS::NUMERIC::MAXD; i++) {
			biggestCandidateDid = std::max(biggestCandidateDid, lists[i]->nextGEQ<0>( biggestCandidateDid )); //nextGEQ and compare
			aligned &= ( lists[i]->did == lists[0]->did );
		}

		if(i==termsCount && aligned) {
			intersection[countHits++] = biggestCandidateDid;
			++biggestCandidateDid;
			}
	} //end while window
	return countHits;
}

void embedDidsInPBS(const did_t* dids, const did_t sz, const unchar didBlockBits, void* dest, const unsigned szBytesDest) { //todo simd
	memset(dest,0,szBytesDest); //zero  all
	for(did_t i = 0; i<sz; ++i) setBit(dest,dids[i]>>didBlockBits);
	setBit(dest,szBytesDest*8-1); //dest[szBytesDest]|=128; //set an elephant
}
