/*
 * OR.cpp
 *
 *  Created on: Dec 14, 2012
 *      Author: constantinos
 */

#include "OR.h"
#include "profiling.h"

WindowManager::WindowManager(unsigned int maxSz, unsigned) : maxWindowSize(maxSz),current(0),didStartCurrent(0),didStartNext(0){

}


OR::OR(std::vector<NewIteratorAugmented*>& lps, Options opts, const score_t thresh) :
lists(lps), threshold(thresh), thresholdInt(0), options(opts){
}


const std::string  EvaluateDocs::name() const {
	std::stringstream str;
	str << "EvaluateDocs";
	return  str.str();
}

void EvaluateDocs::run(const unsigned) {
	COUT << lists.size() << "\n";
	//sort targets by did
	std::sort(targetMap.begin(), targetMap.end(), [](const QpResult<float>& a, const QpResult<float>& b){ return a.did < b.did; });
	for(const auto& p : targetMap)  {
		COUT << p.did << " " << p.score << " ";
		for(unsigned i=0; i<lists.size(); ++i)
			if(lists[i]->nextGEQ<false>(p.did) == p.did) { COUT << i << " ";}
		COUT << "\n";
	}
}

void* EvaluateDocs::getOne() const { return new EvaluateDocs(); }

	void EvaluateDocs::setTargets(const std::vector<QpResult<float> >& targets) { 	targetMap = targets;	}
	
	void EvaluateDocs::setup(std::vector<NewIteratorAugmented*>& lps, Options opts, AugmentsHelperInterface*, const float) {
		options = opts;
		lists = lps;
	}
	
void EvaluateDocs::putResults(void* res) {
	/*QpResult<float>* arr = reinterpret_cast<QpResult<float>*>(res);
	std::vector<QpResult<resultType> > src(resultsHeap.getV().size()); //get a copy of a heap
	resultsHeap.putInArray(src.data());
	aux__convertResultArrays(src.data(),arr,src.size());*/
}
/*
class EvaluateDocs : public QueryProcessorInterface {
private:
	std::vector<NewIteratorAugmented*> lists;
	Options options;

	PriorityArray<QpResult<resultType> > resultsHeap;

public:
	EvaluateDocs(){};
	~EvaluateDocs(){};
	void setup(std::vector<NewIteratorAugmented*>& lps, Options opts);

};
*/
/*
//================================ LA and Postings distribution ==================================
void OR::LAandPostingsDistributions(const unsigned topk) {
	const unsigned windows = (CONSTS::NUMERIC::MAXD>>options.DIDBLOCKBITS)/CONSTS::NUMERIC::;

	NewIteratorAugmented* listsA[10];
	const unsigned termsCnt = this->lists.size();
	for(unsigned i=0; i<termsCnt; ++i) listsA[i]=this->lists[i];

	unsigned long long liveMiniblock = 0;
	std::vector<float> thetas;
	for (unsigned i=70; i<=100; i+=5)
		thetas.push_back( (threshold*i)/100 );

	// foreach window
	for (unsigned winCnt=0; winCnt<=windows; ++winCnt) {
		for(unsigned i=0; i<termsCnt; ++i)
			listsA[i]->preprocessAugmentsForWindow();
		NewIteratorAugmented::buildLiveBlocks(thetas[0], termsCnt);

		// count live mini blocks
		unchar* ptr = reinterpret_cast<unchar*>(NewIteratorAugmentedBase::liveDeadArea);
		liveMiniblock += countSetBits((void*)ptr, (CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK>>3) );

		NewIteratorAugmented::incCurrentWindow();
	}
	COUT << liveMiniblock << ENDL;
}
//================================ LA distribution ==================================
void OR::LADistributions(const unsigned topk) {
	const unsigned windows = (CONSTS::NUMERIC::MAXD>>options.DIDBLOCKBITS)/CONSTS::NUMERIC::C;

	NewIteratorAugmented* listsA[10];
	const unsigned termsCnt = this->lists.size();
	for(unsigned i=0; i<termsCnt; ++i) listsA[i]=this->lists[i];

	did_t smallestCandidateDid = 0;
	score_t finalScore = 0;

	std::vector<unsigned> liveBlocks(7, 0);
	std::vector<float> thetas;
	unsigned totalBlocks = 0;
	//unsigned checkTotalBlocks = (CONSTS::NUMERIC::MAXD>>options.DIDBLOCKBITS);

	for (unsigned i=70; i<=100; i+=5)
		thetas.push_back( (threshold*i)/100 );

	// foreach window
	for (unsigned winCnt=0; winCnt<=windows; ++winCnt) {
		for(unsigned i=0; i<termsCnt; ++i)
			listsA[i]->preprocessAugmentsForWindow();
		NewIteratorAugmented::buildLiveBlocks(thetas[0], termsCnt);

		// compute next window bound
		const did_t nextWinStart = std::min(CONSTS::NUMERIC::MAXD,((1+winCnt)*CONSTS::NUMERIC::C) << options.DIDBLOCKBITS);
		smallestCandidateDid = listsA[0]->getFirstDidinWindow();

		// while candidate did in the range of the current window, try to evaluate it using ET methods
		while (smallestCandidateDid<nextWinStart) {
			finalScore = 0.0f;

			// sum the lists' block-max score
			for (unsigned i=0; i<termsCnt; i++)
				finalScore += listsA[i]->getMaxScoreOfBlock( smallestCandidateDid );

			for (unsigned i=0; i<thetas.size(); i++) {
				if ( Fcompare(finalScore, thetas[i]) >= 0 )
					++liveBlocks[i];
			}

			smallestCandidateDid = listsA[0]->getFirstDidOfNextBlock( smallestCandidateDid );
			++totalBlocks;
		}
		NewIteratorAugmented::incCurrentWindow();
	}

	for (unsigned i=0; i<thetas.size(); i++)
		COUT << liveBlocks[i] << "\t";
	//COUT << ENDL; //"totalblocks: " << totalBlocks << " and " << checkTotalBlocks << ENDL;
}

//=============================== Exhaustive ========================================
PriorityArray<QpResult<> > OR::exhaustiveWithWindows(const unsigned topK) {
	const unsigned windows = (CONSTS::NUMERIC::MAXD>>options.DIDBLOCKBITS)/CONSTS::NUMERIC::C;
	PriorityArray<QpResult<> > resultsHeap(topK);

	NewIteratorAugmented* listsA[10];
	const unsigned termsCnt = this->lists.size();
	for(unsigned i=0; i<termsCnt; ++i) listsA[i]=this->lists[i];

	sortByDid<ITERATOR_VEC_PTR,NewIteratorAugmented>( listsA+0, listsA+termsCnt);

	did_t smallestCandidateDid = 0;
	did_t nextSmallestDid = CONSTS::NUMERIC::MAXD;
	did_t nextLiveDid = 0;

	PartialScoreAccumulator scoreAcc;
	score_t curThreshold = threshold;

	// foreach window
	for (unsigned winCnt=0; winCnt<=windows; ++winCnt) {
		//profilerC::getInstance().start(enm::PROC_AUGMENTS_ALL);
#define FASTEX
//Boo-boo note: the fastex is broken if no USEREAL
#ifdef FASTEX		
		for(unsigned i=0; i<termsCnt; ++i)
			listsA[i]->preprocessAugmentsForWindow();
		NewIteratorAugmented::buildLiveBlocks(curThreshold,termsCnt);
#endif
		// compute next window bound
		const did_t nextWinStart = std::min(CONSTS::NUMERIC::MAXD,((1+winCnt)*CONSTS::NUMERIC::C) << options.DIDBLOCKBITS);

		// move lists, sort them and set the smallest did
#ifdef FASTEX
		nextLiveDid = (listsA[0]->getFirstDidinWindow() == nextWinStart) ? listsA[0]->getFirstDidinWindow() : NewIteratorAugmented::nextLiveArea( listsA[0]->getFirstDidinWindow() );
#else
		nextLiveDid = listsA[0]->getFirstDidinWindow();
#endif
		nextSmallestDid = CONSTS::NUMERIC::MAXD;
		for (unsigned i=0; i<termsCnt; i++) {
			listsA[i]->did = listsA[i]->nextGEQ( nextLiveDid );
			nextSmallestDid = std::min(nextSmallestDid, listsA[i]->did);
		}
		smallestCandidateDid = nextSmallestDid;
		//COUTL << "==============\n" << smallestCandidateDid << "\n";
		// while candidate did in the range of the current window, try to evaluate it using ET methods
		while (smallestCandidateDid<nextWinStart) {
			scoreAcc.init(0.0f);
			nextSmallestDid = CONSTS::NUMERIC::MAXD;

#ifdef FASTEX
			nextLiveDid = (smallestCandidateDid+1 == nextWinStart) ? smallestCandidateDid + 1 : NewIteratorAugmented::nextLiveArea( smallestCandidateDid + 1 );
#else
			nextLiveDid = (smallestCandidateDid+1);
#endif
			// COUTL << smallestCandidateDid << "\n";

			// evaluate aligned documents
			for (unsigned i=0; i<termsCnt; i++) {
				if ( listsA[i]->did == smallestCandidateDid ) {
					scoreAcc.add(listsA[i]->getScore(curThreshold));
					//profilerC::getInstance().stepCounter(enm::EVALUATION);
					listsA[i]->did = listsA[i]->nextGEQ( nextLiveDid );
				}
				nextSmallestDid = std::min(nextSmallestDid, listsA[i]->did);
			}

			// check if heapify is needed
			if (scoreAcc.compare(curThreshold)) {
				score_t finalScore;
				scoreAcc.get(finalScore);
				resultsHeap.push(QpResult<>(smallestCandidateDid,finalScore));
				curThreshold = std::max(curThreshold,resultsHeap.head().score);
			}

			// update nextSmallestCandidateDid
			smallestCandidateDid = nextSmallestDid;
		}
		NewIteratorAugmented::incCurrentWindow();
	}
	return resultsHeap;
}

//=============================== WAND ========================================
PriorityArray<QpResult<> > OR::WAND(const unsigned topk) {
	const unsigned windows = (CONSTS::NUMERIC::MAXD>>options.DIDBLOCKBITS)/CONSTS::NUMERIC::C;
	PriorityArray<QpResult<> > resultsHeap(topk);

	NewIteratorAugmented* listsA[10];
	const unsigned termsCnt = this->lists.size();
	for(unsigned i=0; i<termsCnt; ++i) listsA[i]=this->lists[i];

	did_t nextLiveDid = 0;
	score_t curThreshold = threshold;
	int pivot = -1;
	int thresholdKnowledge = FloatEquality(curThreshold, 0.0f) ? 1 : 0; // needed for having both oracle and no threshold algorithms in one algorithm
	sortByDid<ITERATOR_VEC_PTR, NewIteratorAugmented>( listsA+0, listsA+termsCnt);

	// foreach window
	for (unsigned winCnt=0; winCnt<=windows; ++winCnt) {
		for (NewIteratorAugmented* list : lists)
			list->preprocessAugmentsForWindow();
		NewIteratorAugmented::buildLiveBlocks(curThreshold, termsCnt);

		// compute next window bound
		const did_t nextWinStart = std::min(CONSTS::NUMERIC::MAXD,((1+winCnt)*CONSTS::NUMERIC::C) << options.DIDBLOCKBITS);

		// move lists, sort them and set the smallest did
		nextLiveDid = (listsA[0]->getFirstDidinWindow() == nextWinStart) ? listsA[0]->getFirstDidinWindow() : NewIteratorAugmented::nextLiveArea( lists[0]->getFirstDidinWindow() );
		for (unsigned i=0; i<termsCnt; i++)
			listsA[i]->did = listsA[i]->nextGEQ( nextLiveDid );
		sortByDid<ITERATOR_VEC_PTR, NewIteratorAugmented>( listsA+0, listsA+termsCnt);

		// while candidate did in the range of the current window, try to evaluate it using ET methods
		while (true) {
			if (! pivotSelectionWAND( listsA, curThreshold, pivot, termsCnt, nextWinStart, thresholdKnowledge) )
				break;
			else
				handlePivotWAND( listsA, pivot, termsCnt, resultsHeap, curThreshold, nextWinStart, thresholdKnowledge );
		}
		NewIteratorAugmented::incCurrentWindow();
	}
	return resultsHeap;
}

bool OR::pivotSelectionWAND( ITERATOR_VEC_PTR listsA, const score_t threshold, int& pivot, const unsigned termsCnt, const did_t nextWinStart, const int thresholdKnowledge) {
	// find pivot term until sum of max lists score >= threshold
	score_t sumOfListMaxscoresUpToPivot = 0.0f;
	pivot = -1;
	for (unsigned i=0; i<termsCnt; ++i) {
		sumOfListMaxscoresUpToPivot += listsA[i]->maxScoreOfList;
		if ( Fcompare(sumOfListMaxscoresUpToPivot, threshold) >= thresholdKnowledge ) {
			pivot = i;
			break;
		}
	}

	// check if pivot is valid or out of window bound
	if (pivot == -1 || listsA[pivot]->did >= CONSTS::NUMERIC::MAXD || listsA[pivot]->did >= nextWinStart )
		return false;
	else
		return true;
}

void OR::handlePivotWAND(ITERATOR_VEC_PTR listsA, const int pivot, const unsigned termsCnt, PriorityArray<QpResult<> >& resultsHeap, score_t& threshold, const did_t nextWinStart, const int thresholdKnowledge) {
	did_t pivotDid = listsA[pivot]->did;

	// check alignment + handle real
	if (pivotDid == listsA[0]->did) {
		const did_t nextLiveDid = (pivotDid + 1 == nextWinStart) ? pivotDid + 1 : NewIteratorAugmented::nextLiveArea( pivotDid + 1 );
		score_t finalScore = 0.0f;
		// we loop through all lists to ensure we pick lists with the same did that appear after the pivot list (in the current sorting)
		for (int i=0; i<termsCnt; ++i) {
			if ( listsA[i]->did == pivotDid) {
				finalScore += listsA[i]->getScore(threshold);
				//profilerC::getInstance().stepCounter(enm::EVALUATION);
				listsA[i]->did = listsA[i]->nextGEQ( nextLiveDid );
			}
		}

		// check if this did can make it to the topk and if so heapify
		if ( Fcompare(finalScore, threshold) >= thresholdKnowledge) {
			resultsHeap.push( QpResult<>( listsA[0]->did, finalScore ) );
			threshold = std::max(threshold, resultsHeap.head().score);
		}

		// sort lists
		sortByDid<ITERATOR_VEC_PTR, NewIteratorAugmented>( listsA+0, listsA+termsCnt);
	} else { // we have misalignment of dids so we find the term-list with the smallest list length (according to wand paper)
		int leastFrequentTerm = CONSTS::NUMERIC::MAXD;
		int leastFrequentTermIndex = -1;

		// find the shortest list
		for (unsigned i=0; i<pivot; ++i) {
			if( listsA[i]->getPaddedLength() < leastFrequentTerm && listsA[i]->did != pivotDid ) {
				leastFrequentTerm = listsA[i]->getPaddedLength();
				leastFrequentTermIndex = i;
			}
		}

		// move safely the selected list up to pivot's did
		const did_t nextLiveDid = (pivotDid == nextWinStart) ? pivotDid : NewIteratorAugmented::nextLiveArea( pivotDid );
		listsA[leastFrequentTermIndex]->did = listsA[leastFrequentTermIndex]->nextGEQ( nextLiveDid );

		// update the position of the list
		popdown<ITERATOR_VEC_PTR, NewIteratorAugmented> (listsA+leastFrequentTermIndex, listsA+termsCnt );
	}
}

//=============================== BMW ========================================
PriorityArray<QpResult<> > OR::BMW(const unsigned topk) {
	const unsigned windows = (CONSTS::NUMERIC::MAXD>>options.DIDBLOCKBITS)/CONSTS::NUMERIC::C;
	PriorityArray<QpResult<> > resultsHeap(topk);

	NewIteratorAugmented* listsA[10];
	const unsigned termsCnt = this->lists.size();
	for(unsigned i=0; i<termsCnt; ++i) listsA[i]=this->lists[i];

	score_t curThreshold = threshold;
	score_t sumOfBlockMaxesUpToPivot = 0.0f;
	did_t nextLiveDid = 0;
	int thresholdKnowledge = FloatEquality(curThreshold, 0.0f) ? 1 : 0; // needed for having both oracle and no threshold algorithms in one algorithm
	int pivot = -1;
	sortByDid<ITERATOR_VEC_PTR, NewIteratorAugmented>( listsA+0, listsA+termsCnt); // sort lists by did

	// foreach window
	for (unsigned winCnt=0; winCnt<=windows; ++winCnt) {
		for (NewIteratorAugmented* list : lists)
			list->preprocessAugmentsForWindow();
		NewIteratorAugmented::buildLiveBlocks(curThreshold, termsCnt);

		// compute next window bound
		const did_t nextWinStart = std::min(CONSTS::NUMERIC::MAXD,((1+winCnt)*CONSTS::NUMERIC::C) << options.DIDBLOCKBITS);

		// move lists and sort them
		nextLiveDid = (listsA[0]->getFirstDidinWindow() == nextWinStart) ? listsA[0]->getFirstDidinWindow() : NewIteratorAugmented::nextLiveArea( listsA[0]->getFirstDidinWindow() );
		for (unsigned i=0; i<termsCnt; i++)
			listsA[i]->did = listsA[i]->nextGEQ( nextLiveDid );
		sortByDid<ITERATOR_VEC_PTR, NewIteratorAugmented>( listsA+0, listsA+termsCnt);

		// while candidate did in the range of the current window, try to evaluate it using ET methods
		while (true) {
			// check if pickCandidate succeeded
			if (! pickCandidate(listsA, termsCnt, curThreshold, pivot, sumOfBlockMaxesUpToPivot, nextWinStart, thresholdKnowledge) )
				break;

			// check if blockMax check succeeded
			if ( Fcompare(sumOfBlockMaxesUpToPivot, curThreshold) >= thresholdKnowledge )
				handleReal(listsA, termsCnt, pivot, sumOfBlockMaxesUpToPivot, resultsHeap, curThreshold, topk, nextWinStart, thresholdKnowledge);
			else
				if (! handleFake(listsA, termsCnt, pivot, nextWinStart) )
					break;

		} //end while for the current window
		NewIteratorAugmented::incCurrentWindow();
	}
	return resultsHeap;
}

inline bool OR::pickCandidate(ITERATOR_VEC_PTR listsA, const unsigned termsCnt, const score_t threshold, int& pivot, score_t& sumOfBLockMaxesUptoPivot, const did_t nextWinStart, const int thresholdKnowledge) {
	// pivot selection step using the lists' maxscore
	score_t sumOfListMaxScores = 0.0f;
	pivot = -1;

	for (unsigned i=0; i<termsCnt; ++i) {
		sumOfListMaxScores += listsA[i]->getMaxScoreOfList();
		 if ( Fcompare(sumOfListMaxScores, threshold) >= thresholdKnowledge ) {
			// gather all lists with the same did as the pivot did (although after pivot term), because it's safe and saves computation
			while ( i+1<termsCnt && listsA[i+1]->did == listsA[i]->did)
				++i;

			pivot = i;
			break;
		}
	}

	// check whether we have a valid pivot or whether the pivot docID is out of the current window range
	if(pivot == -1 || listsA[pivot]->did >= CONSTS::NUMERIC::MAXD || listsA[pivot]->did >= nextWinStart)
		return false;

	// obtain the block-max score for all lists up to pivot term (according to the current docID ordering)
	sumOfBLockMaxesUptoPivot = listsA[pivot]->getMaxScoreOfBlock( listsA[pivot]->did );
	for (int i=0; i<pivot; ++i)
		//if ( listsA[pivot]->did <= listsA[i]->nextLivePosting( listsA[pivot]->did ) ) // OPTIMIZATION-1 (*) better block-max refinement
			sumOfBLockMaxesUptoPivot += listsA[i]->getMaxScoreOfBlock( listsA[pivot]->did );
	return true;
}

inline bool OR::handleFake(ITERATOR_VEC_PTR listsA, const unsigned termsCnt, const int pivot, const did_t nextWinStart) {
	int leastFrequentTerm = listsA[0]->getPaddedLength();
	int leastFrequentTermIndex = 0;

	// find the shortest list
	for (int i=1; i<pivot+1; ++i) {
		if ( listsA[i]->getPaddedLength() < leastFrequentTerm) {
			leastFrequentTerm = listsA[i]->getPaddedLength();
			leastFrequentTermIndex = i;
		}
	}

	did_t smallestMaxDid = listsA[pivot]->getFirstDidOfNextBlock( listsA[pivot]->did );
	smallestMaxDid = smallestMaxDid >= CONSTS::NUMERIC::MAXD ? CONSTS::NUMERIC::MAXD : smallestMaxDid;
	did_t did = (( pivot < termsCnt - 1) && smallestMaxDid > listsA[ pivot + 1 ]->did) ? listsA[ pivot + 1 ]->did : smallestMaxDid;

	if ( did <= listsA[pivot]->did)
		did = listsA[pivot]->did + 1;

	if (did >= CONSTS::NUMERIC::MAXD || did >= nextWinStart)
		return false;

	did_t nextLiveDid = (did == nextWinStart) ? did : NewIteratorAugmented::nextLiveArea( did );
	listsA[leastFrequentTermIndex]->did = listsA[leastFrequentTermIndex]->nextGEQ( nextLiveDid );

	// update the position of the list
	popdown<ITERATOR_VEC_PTR, NewIteratorAugmented> (listsA+leastFrequentTermIndex, listsA+termsCnt );
	return true;
}

inline void OR::handleReal(ITERATOR_VEC_PTR listsA, const unsigned termsCnt, const int pivot, score_t sumOfBLockMaxesUptoPivot, PriorityArray<QpResult<> >& resultsHeap, score_t& curThreshold, const unsigned topK, const did_t nextWinStart, const int thresholdKnowledge) {
	score_t impactScore = 0.0f;
	did_t pivotDid = listsA[pivot]->did;

	// if did alignment succeeded
	if ( pivotDid == listsA[0]->did ) {
		bool earlyTermination = false;
		for (int i=0 ; i<pivot+1; ++i)	{
			const score_t score = listsA[i]->getScore();
			//profilerC::getInstance().stepCounter(enm::EVALUATION);
			impactScore += score;
			sumOfBLockMaxesUptoPivot -= ( listsA[i]->getMaxScoreOfBlock( pivotDid ) - score );

			// early termination
			if ( Fcompare(sumOfBLockMaxesUptoPivot, curThreshold) == -1 ) {
				earlyTermination = true;
				break;
			}
		}

		// if heapify is needed
		if ( !earlyTermination && Fcompare(impactScore, curThreshold) >= thresholdKnowledge ) {
			resultsHeap.push(QpResult<>(pivotDid, impactScore));
			curThreshold = std::max(curThreshold, resultsHeap.head().score);
		}

		// move safely all lists up to pivot
		did_t nextLiveDid = ( pivotDid + 1 == nextWinStart) ? pivotDid + 1 : NewIteratorAugmented::nextLiveArea( pivotDid + 1 );
		for (int i=0; i<pivot+1; ++i)
			listsA[i]->did = listsA[i]->nextGEQ( nextLiveDid );

		// update the position of the list
		for(int i = pivot ; i > -1; i--)
			popdown<ITERATOR_VEC_PTR, NewIteratorAugmented> (listsA+i, listsA+termsCnt );
	} else { // if misalignment of dids - idf heuristic
		int leastFrequentTerm = listsA[0]->getPaddedLength();
		int leastFrequentTermIndex = 0;

		// find the shortest list
		for (unsigned i=1; i<pivot; ++i) { // 0
			if( listsA[i]->getPaddedLength() < leastFrequentTerm && listsA[i]->did != pivotDid ) {
				leastFrequentTerm = listsA[i]->getPaddedLength();
				leastFrequentTermIndex = i;
			}
		}

		// move safely the selected list up to pivot's did
		const did_t nextLiveDid = (pivotDid == nextWinStart) ? pivotDid : NewIteratorAugmented::nextLiveArea( pivotDid );
		listsA[leastFrequentTermIndex]->did = listsA[leastFrequentTermIndex]->nextGEQ( nextLiveDid );

		// update the position of the list
		popdown<ITERATOR_VEC_PTR, NewIteratorAugmented> (listsA+leastFrequentTermIndex, listsA+termsCnt );
	}
}
*/
/*
//=============================== Maxscore ========================================
PriorityArray<QpResult<> > OR::Maxscore(const unsigned topk) {
	const unsigned windows = (CONSTS::NUMERIC::MAXD>>options.DIDBLOCKBITS)/CONSTS::NUMERIC::C;
	PriorityArray<QpResult<> > resultsHeap(topk);

	NewIteratorAugmented* listsA[10];
	const unsigned termsCnt = this->lists.size();
	for(unsigned i=0; i<termsCnt; ++i) listsA[i]=this->lists[i];

	// Variables
	unsigned numEssentialLists = termsCnt;
	unsigned numNonEssentialLists = 0;
	score_t curNonEssentialMaxScoreSum = 0.0f;
	score_t curThreshold = threshold;
	bool essentialListAdded = false;
	bool thresholdKnowledge = FloatEquality(threshold, 0.0f) ? false : true;
	int thresholdKnowledgeComparison = FloatEquality(curThreshold, 0.0f) ? 1 : 0; // needed for having both oracle and no threshold algorithms in one algorithm

	// Vectors
	std::vector<float> listsMaxscore; listsMaxscore.reserve(termsCnt);
	std::vector<float> prefixMaxScoresSum; prefixMaxScoresSum.reserve(termsCnt - 1);
	did_t smallestCandidateDid = 0;
	did_t nextLiveDid = 0;

	// initial sorting of lists by max score
	sortByMaxscore<ITERATOR_VEC_PTR, NewIteratorAugmented>( listsA+0, listsA+termsCnt);

	// initialize lists' maxscore vector
	for (int i=0; i<termsCnt; i++)
		listsMaxscore.push_back( listsA[i]->getMaxScoreOfList() );

	// initialize prefix max score sum vector that is used in findNewNonEssentialLists
	prefixMaxScoresSum.push_back(listsA[0]->getMaxScoreOfList());
	for (int i=1; i<termsCnt-1; i++)
		prefixMaxScoresSum.push_back( prefixMaxScoresSum[i - 1] + listsA[i]->getMaxScoreOfList() );

	// if we were given some threshold prediction try to findNonEssentialLists
	if (thresholdKnowledge) {
		findNewNonEssentialLists(termsCnt, numEssentialLists, numNonEssentialLists, prefixMaxScoresSum, curThreshold, essentialListAdded, curNonEssentialMaxScoreSum);
		essentialListAdded = false;
	}

	// find first smallest candidate did in essential set
	if (! findSmallestDidInEssentialList(listsA, termsCnt, numNonEssentialLists, smallestCandidateDid, CONSTS::NUMERIC::MAXD) )
		return resultsHeap;

	// foreach window
	for (unsigned winCnt=0; winCnt<=windows; ++winCnt) {
		for (NewIteratorAugmented* list : lists)
			list->preprocessAugmentsForWindow();
		NewIteratorAugmented::buildLiveBlocks(curThreshold, termsCnt);

		// compute next window bound
		const did_t nextWinStart = std::min(CONSTS::NUMERIC::MAXD,((1+winCnt)*CONSTS::NUMERIC::C) << options.DIDBLOCKBITS);

		// move essential lists only so that we can find the new smallest did in essential set
		nextLiveDid = (listsA[0]->getFirstDidinWindow() == nextWinStart) ? listsA[0]->getFirstDidinWindow() : NewIteratorAugmented::nextLiveArea( listsA[0]->getFirstDidinWindow() );
		for (unsigned i=numNonEssentialLists; i<termsCnt; i++)
			listsA[i]->did = listsA[i]->nextGEQ( nextLiveDid );

		// find smallestCandidateDid in essential set
		if (! findSmallestDidInEssentialList(listsA, termsCnt, numNonEssentialLists, smallestCandidateDid, nextWinStart) ) {
			// in case out of window, set the new smallest candidate did to the start of the next window and move window number
			smallestCandidateDid = nextWinStart;
			NewIteratorAugmented::incCurrentWindow();
			continue;
		}

		// while candidate did in the range of the current window, try to evaluate it using ET methods
		while (smallestCandidateDid<nextWinStart) {
			bool earlyTerminationFlag = false;
			score_t impactScore = 0.0f;
			score_t earlyTermination = 0.0f;
			did_t nextCandidateDid = CONSTS::NUMERIC::MAXD + 1;
			nextLiveDid = (smallestCandidateDid + 1 == nextWinStart) ? smallestCandidateDid + 1 : NewIteratorAugmented::nextLiveArea( smallestCandidateDid + 1 );

			// evaluate essential set and pick next candidate did
			for (unsigned i=numNonEssentialLists; i<termsCnt; ++i) {
				if (listsA[i]->did == smallestCandidateDid) {
					impactScore += listsA[i]->getScore(threshold);
					//profilerC::getInstance().stepCounter(enm::EVALUATION);
					listsA[i]->did= listsA[i]->nextGEQ( nextLiveDid );
				}
				// pick next smallest candidate did
				nextCandidateDid = std::min(listsA[i]->did, nextCandidateDid);
			}

			// Early termination: if essential lists impact score plus the sum of non-essential lists maxscores > threshold
			if (Fcompare(impactScore + curNonEssentialMaxScoreSum, curThreshold) >= thresholdKnowledgeComparison) {
				earlyTermination = impactScore + curNonEssentialMaxScoreSum;
				nextLiveDid = (smallestCandidateDid == nextWinStart) ? smallestCandidateDid : NewIteratorAugmented::nextLiveArea( smallestCandidateDid );

				for (int i=numNonEssentialLists-1; i>=0; --i)	{
					// move pointers if needed
					if (listsA[i]->did < smallestCandidateDid)
						listsA[i]->did = listsA[i]->nextGEQ( nextLiveDid );

					// check if evaluation is needed
					if (listsA[i]->did == smallestCandidateDid) {
						const float score = listsA[i]->getScore(threshold);
						//profilerC::getInstance().stepCounter(enm::EVALUATION);
						impactScore += score;
						earlyTermination -= (listsMaxscore[i] - score);
					} else
						earlyTermination -= listsMaxscore[i];

					// early termination
					if (Fcompare(earlyTermination, curThreshold) == -1) {
						earlyTerminationFlag = true;
						break;
					}
				}

				// check if the candidate did can make it into the topk
				if (!earlyTerminationFlag && Fcompare(impactScore, curThreshold) >= thresholdKnowledgeComparison) {
					resultsHeap.push(QpResult<>(smallestCandidateDid, impactScore));
					curThreshold = std::max(resultsHeap.head().score, curThreshold);
					findNewNonEssentialLists(termsCnt, numEssentialLists, numNonEssentialLists, prefixMaxScoresSum, curThreshold, essentialListAdded, curNonEssentialMaxScoreSum);
					// if new non essential list added, we need to find the smallest did in essential set
					if (essentialListAdded) {
						essentialListAdded = false;
						nextCandidateDid = CONSTS::NUMERIC::MAXD + 1;
						for (unsigned i=numNonEssentialLists; i<termsCnt; ++i)
							nextCandidateDid = std::min(nextCandidateDid, listsA[i]->did);
					}
				}
			}
			// set the correct next smallest candidate did (two cases: a) either its already computed and no heapify occured b) already computed and heapify occured along with the addition of a new non essential list (cornercase)
			smallestCandidateDid = nextCandidateDid;
		} //end of current window while loop
		NewIteratorAugmented::incCurrentWindow();
	}
	return resultsHeap;
}

inline void OR::findNewNonEssentialLists(const unsigned termsCnt, unsigned& numEssentialLists, unsigned& numNonEssentialLists, const std::vector <score_t> prefixMaxScoresSum, const score_t threshold, bool& essentialListAdded, score_t& curNonEssentialMaxScoreSum) {
	while ( (numNonEssentialLists<termsCnt-1) && (Fcompare(prefixMaxScoresSum[numNonEssentialLists], threshold) == -1) ) {
		++numNonEssentialLists;     // increase number of non-essential lists
		--numEssentialLists; 		// decrease number of non-essential lists
		essentialListAdded = true;

		// set correct current prefix sum max score
		curNonEssentialMaxScoreSum = prefixMaxScoresSum[numNonEssentialLists - 1];
	}
}

inline bool OR::findSmallestDidInEssentialList(ITERATOR_VEC_PTR listsA, const unsigned termsCnt, const unsigned numNonEssentialLists, unsigned& smallestCandidateDid, const did_t bound) {
	// Note that since lists are sorted by maxscore, we can obtain the index of the
	// first essential list in our lists vector by using the numNonEssentialLists
	smallestCandidateDid = listsA[numNonEssentialLists]->did;
	for (int i=numNonEssentialLists+1; i<termsCnt; ++i)
		smallestCandidateDid = std::min(listsA[i]->did, smallestCandidateDid);

	// check if smallestCandidateDid out of current window bound
	if (smallestCandidateDid >= bound) return false;
	else return true;
}

//=============================== BMM ========================================
PriorityArray<QpResult<> > OR::BMM(const unsigned int topK) {
	const unsigned windows = (CONSTS::NUMERIC::MAXD>>options.DIDBLOCKBITS)/CONSTS::NUMERIC::C;
	PriorityArray<QpResult<> > resultsHeap(topK);

	NewIteratorAugmented* listsA[10];
	const unsigned termsCnt = this->lists.size();
	for(unsigned i=0; i<termsCnt; ++i) listsA[i]=this->lists[i];

	// Variables
	unsigned numEssentialLists = termsCnt;
	unsigned numNonEssentialLists = 0;
	score_t curNonEssentialMaxScoreSum = 0.0f;
	score_t curThreshold = threshold;
	bool essentialListAdded = false;
	bool checkNewEssentialList = false; // For version 2
	bool thresholdKnowledge = FloatEquality(curThreshold, 0.0f) ? false : true;
	did_t smallestCandidateDid = 0;
	did_t nextLiveDid = 0;

	// Vectors
	std::vector<score_t> listsMaxscore; listsMaxscore.reserve(termsCnt);
	std::vector<score_t> listsCurBlockMaxScore; listsCurBlockMaxScore.reserve(termsCnt);
	std::vector<score_t> prefixMaxScoresSum; prefixMaxScoresSum.reserve(termsCnt - 1);

	// initial sorting of lists by max score
	sortByMaxscore<ITERATOR_VEC_PTR, NewIteratorAugmented>( listsA+0, listsA+termsCnt);

	// initialize lists' maxscore vector and lists' current Block-Max score vector
	for (int i=0; i<termsCnt; i++) {
		listsMaxscore.push_back(listsA[0]->getMaxScoreOfList());
		listsCurBlockMaxScore.push_back(0.0f);
	}

	// initialize prefix max score sum vector that is used in findNewNonEssentialLists
	prefixMaxScoresSum.push_back(listsA[0]->getMaxScoreOfList());
	for (int i=1; i<termsCnt-1; i++)
		prefixMaxScoresSum.push_back( prefixMaxScoresSum[i - 1] + listsA[i]->getMaxScoreOfList() );

	// if we were given some threshold prediction try to findNonEssentialLists
	if (thresholdKnowledge) {
		findNewNonEssentialLists(termsCnt, numEssentialLists, numNonEssentialLists, prefixMaxScoresSum, curThreshold, essentialListAdded, curNonEssentialMaxScoreSum);
		essentialListAdded = false;
	}

	// find first smallest candidate did in essential set
	if (! findSmallestDidInEssentialList(listsA, termsCnt, numNonEssentialLists, smallestCandidateDid, CONSTS::NUMERIC::MAXD) )
		return resultsHeap;

	// foreach window
	for (unsigned winCnt=0; winCnt<=windows; ++winCnt) {
		for (NewIteratorAugmented* list : lists)
			list->preprocessAugmentsForWindow();
		NewIteratorAugmented::buildLiveBlocks(curThreshold, termsCnt);

		// compute next window bound
		const did_t nextWinStart = std::min(CONSTS::NUMERIC::MAXD,((1+winCnt)*CONSTS::NUMERIC::C) << options.DIDBLOCKBITS);

		// move lists, sort them and set the smallest did
		nextLiveDid = (listsA[0]->getFirstDidinWindow() == nextWinStart) ? listsA[0]->getFirstDidinWindow() : NewIteratorAugmented::nextLiveArea( listsA[0]->getFirstDidinWindow() );
		// move essential lists only so that we can find the new smallest did in essential set
		for (unsigned i=numNonEssentialLists; i<termsCnt; i++)
			listsA[i]->did = listsA[i]->nextGEQ( nextLiveDid );

		// find smallestCandidateDid in essential set
		if (! findSmallestDidInEssentialList(listsA, termsCnt, numNonEssentialLists, smallestCandidateDid, nextWinStart) ) {
			// in case out of window, set the new smallest candidate did to the start of the next window and move window number
			smallestCandidateDid = nextWinStart;
			NewIteratorAugmented::incCurrentWindow();
			continue;
		}

		// while smallestCandidateDid is a valid did for the current window
		while (smallestCandidateDid<nextWinStart) {
			// #######################################################################################################
			// 	Version 1 - essential eval first, non essential block refinement with early termination
			// #######################################################################################################
//			score_t impactScore = 0.0f;
//			score_t nonEssBlockScore = 0.0f;
//
//			// obtain essential set scores
//			for (unsigned i=numNonEssentialLists; i<termsCnt; i++) {
//				if (listsA[i]->did == smallestCandidateDid) {
//					impactScore += listsA[i]->getScore();
//					profilerC::getInstance().stepCounter(enm::EVALUATION);
//				}
//			}
//
//			// get non essential set block max scores
//			for (unsigned i=0; i<numNonEssentialLists; i++)
//				nonEssBlockScore += listsA[i]->getMaxScoreOfBlock( smallestCandidateDid );
//
//			// check for early termination
//			if ( Fcompare(impactScore + nonEssBlockScore, curThreshold) >=0 ) {
//				nextLiveDid = NewIteratorAugmented::nextLiveArea( smallestCandidateDid );
//				bool earlyTermination = false;
//				nonEssBlockScore += impactScore;
//
//				// non essential set refinement
//				for (int i=((int)numNonEssentialLists)-1; i>=0; i--) {
//					if (listsA[i]->did < smallestCandidateDid)
//						listsA[i]->did = listsA[i]->nextGEQ( nextLiveDid );
//
//					if (listsA[i]->did == smallestCandidateDid) {
//						score_t tmp = listsA[i]->getScore();
//						profilerC::getInstance().stepCounter(enm::EVALUATION);
//						impactScore +=  tmp;
//						nonEssBlockScore -= (listsA[i]->getMaxScoreOfBlock( smallestCandidateDid ) - tmp);
//					} else 	nonEssBlockScore -= listsA[i]->getMaxScoreOfBlock( smallestCandidateDid );
//
//					if (Fcompare(nonEssBlockScore, curThreshold) == -1) {
//						earlyTermination = true;
//						break;
//					}
//				}
//
//				// heapify if needed and update threshold and try to find new non essential lists
//				if (!earlyTermination && Fcompare(impactScore, curThreshold) >= 0) {
//					resultsHeap.push(QpResult(smallestCandidateDid, impactScore));
//					curThreshold = std::max(resultsHeap.head().score, threshold);
//					findNewNonEssentialLists(termsCnt, numEssentialLists, numNonEssentialLists, prefixMaxScoresSum, curThreshold, essentialListAdded, curNonEssentialMaxScoreSum);
//					essentialListAdded = false;
//				}
//			}
//
//			// skip safely in the essential set and update smallestCandidateDid
//			nextLiveDid = (smallestCandidateDid + 1 == nextWinStart) ? smallestCandidateDid + 1 : NewIteratorAugmented::nextLiveArea( smallestCandidateDid + 1 ); //smallestCandidateDid + 1;
//			did_t tempNextSmallestCandidateDid = CONSTS::NUMERIC::MAXD + 1;
//			for (unsigned i=numNonEssentialLists; i<termsCnt; i++) {
//				if (listsA[i]->did == smallestCandidateDid)
//					listsA[i]->did = listsA[i]->nextGEQ( nextLiveDid );
//
//				// pick the next smallest did
//				tempNextSmallestCandidateDid = std::min(tempNextSmallestCandidateDid, listsA[i]->did);
//			}
//			// next smallestCandidateDid has been found
//			smallestCandidateDid = tempNextSmallestCandidateDid;

			// #######################################################################################################
			// 			Version 2 - all filters enabled - a bit faster - uncomment checkNewEssentialList variable
			// #######################################################################################################

			evaluate(listsA, termsCnt, resultsHeap, curThreshold, topK, nextWinStart, numNonEssentialLists, smallestCandidateDid, listsCurBlockMaxScore, curNonEssentialMaxScoreSum, checkNewEssentialList, listsMaxscore);
			// if heapify, try to find more non essential lists
			if (checkNewEssentialList) {
				curThreshold = std::max(resultsHeap.head().score, curThreshold);
				findNewNonEssentialLists(termsCnt, numEssentialLists, numNonEssentialLists, prefixMaxScoresSum, curThreshold, essentialListAdded, curNonEssentialMaxScoreSum);
				essentialListAdded = false;
				checkNewEssentialList = false;
			}
		} //end current livearea window while loop
		NewIteratorAugmented::incCurrentWindow();
	}
	return resultsHeap;
}

inline void OR::evaluate(ITERATOR_VEC_PTR listsA, const unsigned termsCnt, PriorityArray<QpResult<> >& resultsHeap, const score_t threshold,
		const unsigned topK, const did_t nextWinStart, const int numNonEssentialLists, did_t& smallestCandidateDid, std::vector <score_t>& listsCurBlockMaxScore,
		const score_t curNonEssentialMaxScoreSum, bool& checkNewEssentialList, std::vector <score_t>& listsMaxscore) {

	score_t finalScore = 0.0f;
	score_t blockScoreSum = 0.0f;
	score_t essentialBlockMaxSum = 0.0f;
	score_t nonEssentialBlockMaxSum = 0.0f;
	score_t earlyTermination = 0.0f;

    // candidate population
    //PROFILER(CONSTS::CAND);

    // sum the block maxes of essential set
    for (int i=termsCnt-1; i>=numNonEssentialLists; --i) {
//    	if ( smallestCandidateDid <= listsA[i]->nextLivePosting( smallestCandidateDid ) ) // OPTIMIZATION-1 (*) better block-max refinement (performs worse ~ 0.5 ms)
    		essentialBlockMaxSum += listsA[i]->getMaxScoreOfBlock(smallestCandidateDid);
    }

    // Filter 1: sum(essential block maxes) + sum(non-essential maxscores)
    if ( Fcompare(essentialBlockMaxSum + curNonEssentialMaxScoreSum, threshold) >= 0 ) {
    	//PROFILER(CONSTS::EARLYTERMINATION0);

    	// used temporarily
    	earlyTermination = essentialBlockMaxSum + curNonEssentialMaxScoreSum;

    	// get block maxes of non-essential set
		for (int i=numNonEssentialLists-1; i>=0; --i) {
//			if ( smallestCandidateDid <= listsA[i]->nextLivePosting( smallestCandidateDid ) ) { // OPTIMIZATION-2 (**) better block-max refinement
				listsCurBlockMaxScore[i] = listsA[i]->getMaxScoreOfBlock(smallestCandidateDid);
				nonEssentialBlockMaxSum += listsCurBlockMaxScore[i];
				earlyTermination -= (listsMaxscore[i] - listsCurBlockMaxScore[i]);
///
//			} else { // (**)
//				listsCurBlockMaxScore[i] = 0.0f; // (**)
//				earlyTermination -= listsMaxscore[i]; // (**)
//			} // (**)
//
			// early termination
			if ( Fcompare(earlyTermination, threshold) == -1 )
				break;
		}

		// Filter 2: sum(essential set block maxes) + sum(non-essential set block maxes)
		if ( Fcompare(essentialBlockMaxSum + nonEssentialBlockMaxSum, threshold) >= 0 ) {
			//PROFILER(CONSTS::EARLYTERMINATION1);

			// early termination (*) is almost the same speed
//(*)		bool ET = false;
//(*)		earlyTermination = essentialBlockMaxSum + nonEssentialBlockMaxSum;

			// pick the nextLiveDid we can skip safely
			did_t nextLiveDid = (smallestCandidateDid + 1 == nextWinStart) ? smallestCandidateDid + 1 : NewIteratorAugmented::nextLiveArea( smallestCandidateDid + 1 ); //smallestCandidateDid + 1;

			// Evaluate essential set
			for (int i = termsCnt-1; i>=numNonEssentialLists; --i) {
				if (smallestCandidateDid == listsA[i]->did) {
					const score_t score = listsA[i]->getScore();
					//profilerC::getInstance().stepCounter(enm::EVALUATION);
					finalScore += score;
					listsA[i]->did = listsA[i]->nextGEQ( nextLiveDid );
				}
///   		// remove bracket in above live and uncomment the following lines (*)
//					earlyTermination -= (listsCurBlockMaxScore[i] - score);
//				} else
//					earlyTermination -= listsCurBlockMaxScore[i];
//
//				// early termination
//				if (Fcompare(earlyTermination, threshold) == -1) {
//					ET = true;
//					break;
//				}
//				// CAN HAVE EXTRA EARLY TERMINATION (no extra gain)
//
			}

			blockScoreSum = finalScore + nonEssentialBlockMaxSum;

			// Early termination 2: if actual score of essential set + sum of list maxscore of non-essential, less than threshold
			// Filter 3: essential score + sum of block maxes of non-essential set
//(*)		if ( !ET && Fcompare(blockScoreSum, threshold) >= 0 ) {
			if ( Fcompare(blockScoreSum, threshold) >= 0 ) {
				//PROFILER(CONSTS::EARLYTERMINATION3);
				// Evaluate non-essential set
				nextLiveDid = NewIteratorAugmented::nextLiveArea( smallestCandidateDid ); // maybe its overhead ? TOCHECK
				for (int i=numNonEssentialLists-1; i>=0; --i)	{
					// move pointers if needed
					if (listsA[i]->did < smallestCandidateDid)
						listsA[i]->did = listsA[i]->nextGEQ( nextLiveDid );

//					if ( !FloatEquality(listsCurBlockMaxScore[i], 0.0f) ) { // OPTIMIZATION-3 (***) better block-max refinement
//						if (listsA[i]->did < smallestCandidateDid) // (***)
//							listsA[i]->did = listsA[i]->nextGEQ( nextLiveDid ); // (***)

					// check if evaluation is needed
					if (smallestCandidateDid == listsA[i]->did) {
						const score_t score = listsA[i]->getScore();
						//profilerC::getInstance().stepCounter(enm::EVALUATION);
						finalScore += score;
						blockScoreSum -= (listsCurBlockMaxScore[i] - score);
					}
//				} // (***)

					// Early terminate during non-essential set max block scores refinement
					if ( Fcompare(blockScoreSum, threshold) == -1 ) {
						//PROFILER(CONSTS::EARLYTERMINATION5);
						break;
					}
				}

				// if smallest did passed all filters, heapify new result
				if ( Fcompare(finalScore, threshold) >= 0 ) {
					//PROFILER(CONSTS::HEAPIFY);
					resultsHeap.push(QpResult<>(smallestCandidateDid, finalScore));
					checkNewEssentialList = true;
				}

				// find next smallest did
				smallestCandidateDid = CONSTS::NUMERIC::MAXD + 1;
				for (int i = termsCnt-1; i>=numNonEssentialLists; --i)
					smallestCandidateDid = std::min(listsA[i]->did, smallestCandidateDid);
			} else { // Early termination: essential score + sum of block maxes of non-essential set < threshold
				//PROFILER(CONSTS::EARLYTERMINATION4);
				// temp value to help find the next smallest did
				did_t tempNextSmallestCandidateDid = CONSTS::NUMERIC::MAXD + 1;
				did_t nextLiveDid = (smallestCandidateDid + 1 == nextWinStart) ? smallestCandidateDid + 1 : NewIteratorAugmented::nextLiveArea( smallestCandidateDid + 1 );

				// skip safely in the essential set
				for (int i = termsCnt-1; i>=numNonEssentialLists; --i) {
					if (smallestCandidateDid == listsA[i]->did)
						listsA[i]->did = listsA[i]->nextGEQ( nextLiveDid ) ;

					// pick the next smallest did
					tempNextSmallestCandidateDid = std::min(listsA[i]->did, tempNextSmallestCandidateDid);
				}

				// next smallest did has already been found
				smallestCandidateDid = tempNextSmallestCandidateDid;
			}
		} else {
			//PROFILER(CONSTS::EARLYTERMINATION2);
			skipToNextLiveArea(listsA, termsCnt, numNonEssentialLists, nextWinStart, smallestCandidateDid);
		}
    } else {
    	//PROFILER(CONSTS::EARLYTERMINATION01);
    	skipToNextLiveArea(listsA, termsCnt, numNonEssentialLists, nextWinStart, smallestCandidateDid);
    }
}

inline void OR::skipToNextLiveArea(ITERATOR_VEC_PTR listsA, const unsigned termsCnt, const int numNonEssentialLists, const did_t nextWinStart, did_t& smallestCandidateDid) {
	did_t smallestMaxDid = listsA[numNonEssentialLists]->getFirstDidOfNextBlock(smallestCandidateDid);

 	// if smallestMaxDid in range, find next smallest did
   	if (smallestMaxDid < CONSTS::NUMERIC::MAXD) {
   		// initialize the next smallest did with a large value
   		smallestCandidateDid = CONSTS::NUMERIC::MAXD + 1;

   		// check whether the end of the block (that we can safely skip) is the same with the start of the next window and pick appropriate nextLiveDid
   		const did_t nextLiveDid = (smallestMaxDid == nextWinStart) ? smallestMaxDid : NewIteratorAugmented::nextLiveArea( smallestMaxDid );

   		// perform skipping only on essential set
   		for (int i=termsCnt-1; i>=numNonEssentialLists; --i) {
			listsA[i]->did = listsA[i]->nextGEQ( nextLiveDid );

			// pick the next smallestCandidateDid
			smallestCandidateDid = std::min(listsA[i]->did, smallestCandidateDid);
   		}
  	} else
  		smallestCandidateDid = CONSTS::NUMERIC::MAXD + 1;
}


//=============================== Exhaustive Simple ========================================
PriorityArray<QpResult<> > OR::exhaustiveSimple(const unsigned topk) {
	const int useReals = 0;
    PriorityArray<QpResult<> > resultsHeap(topk);

    NewIteratorAugmented* listsA[10];
    const unsigned termsCnt = this->lists.size();
    for(unsigned i=0; i<termsCnt; ++i) listsA[i]=this->lists[i];

    // sort lists by docID
    sortByDid<ITERATOR_VEC_PTR,NewIteratorAugmented>( listsA+0, listsA+termsCnt);

    did_t smallestCandidateDid = listsA[0]->did;
    score_t finalScore = 0;
    score_t curThreshold = threshold;
    int thresholdKnowledge = FloatEquality(curThreshold, 0.0f) ? 1 : 0; // needed for having both oracle and no threshold algorithms in one algorithm

    while (smallestCandidateDid < CONSTS::NUMERIC::MAXD) {
        // initialize final score
        finalScore = 0.0f;
        did_t nextSmallestDid = CONSTS::NUMERIC::MAXD;

        // evaluate all dids with did == smallestCandidateDid
        for (unsigned i=0; i<termsCnt; ++i){
            if (listsA[i]->did == smallestCandidateDid) {
                finalScore += listsA[i]->getScore<useReals>(threshold);
                //profilerC::getInstance().stepCounter(enm::EVALUATION);
                listsA[i]->did = listsA[i]->nextGEQ<useReals>( smallestCandidateDid + 1);
            }
            nextSmallestDid = std::min(nextSmallestDid, listsA[i]->did);
        }

        // if heapify is needed
        if ( Fcompare(finalScore, curThreshold) >= thresholdKnowledge ) {
            resultsHeap.push(QpResult<>(smallestCandidateDid,finalScore));
            curThreshold = std::max(curThreshold,resultsHeap.fasthead().score); //boo-boo
        }

        // update the nextSmallestCandidateDid
        smallestCandidateDid = nextSmallestDid;
    }
    return resultsHeap;
}
*/
