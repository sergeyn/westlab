#include <fstream>
#include <iostream>
#include "globals.h"
#include "utils.h"


//usage:
/*
//declare on top of Executor::run or runInt
 pairsOracle pairsOra("/data1/team/sergeyn/git/newTopK/QueryLog/goldenPairwise");

//use along with your threshold:
float maxOfPairs = pairsOra.maxScoreForQuery(queryTerms);

*/
template <int n>
class nSetOracle {
	strToFloatMap cache;
	std::string terms[n];

 	std::string joinStrings() {
        std::stringstream buffer;
        for(unsigned i=0; i<n-1; ++i)
			buffer << terms[i] << " ";
		buffer << terms[n-1];	
        return buffer.str();
}

	public:

	explicit nSetOracle(const std::string& fname) {	
	    if(fname == "") return;
        std::fstream is(fname.c_str());
        
        float score;
        while(is.good()) {
				for(unsigned i=0; i<n; ++i)
					is >> terms[i];
				is >> score;
				
				cache[joinStrings()] = score;
				//COUT << "|" << p1 + " " + p2 << "|" << ENDL;
        }	
	}	
	
	
	
	float maxScoreForQuery(const std::vector<std::string>& terms)  {
		std::vector<std::string> copyOfTerms(terms);
		std::sort(copyOfTerms.begin(),copyOfTerms.end());
		float max = 0.0f;
		if(n==1) {
			for(unsigned i=0; i<copyOfTerms.size(); ++i) {
					std::string key = copyOfTerms[i]; 
			//		COUT << " |" << key << "|" << cache.count(key)<< ENDL;
					if(cache.count(key) && max<cache[key])
						max = cache[key];				
			}
		}
		else if(n==2) {
			for(unsigned i=0; i<copyOfTerms.size()-1; ++i)
				for(unsigned j=i+1; j<copyOfTerms.size(); ++j) {
					std::string key = copyOfTerms[i]+" "+copyOfTerms[j]; 
			//		COUT << i << " " << j << " |" << key << "|" << cache.count(key)<< ENDL;
					if(cache.count(key) && max<cache[key])
						max = cache[key];
				}
		}
		return max;
	}
};

/*
class pairsOracle {
	strToFloatMap cache;
	
	public:
	
	explicit pairsOracle(const std::string& fname) {
        if(fname == "") return;
        std::fstream is(fname.c_str());
        std::string p1,p2;
        float score;
        while(is.good()) {
                is >> p1 >> p2 >> score;
				cache[p1 + " " + p2] = score;
				//COUT << "|" << p1 + " " + p2 << "|" << ENDL;
        }
	}
	
	float maxScoreForQuery(const std::vector<std::string>& terms)  {
		std::vector<std::string> copyOfTerms(terms);
		std::sort(copyOfTerms.begin(),copyOfTerms.end());
		float max = 0.0f;
		for(unsigned i=0; i<copyOfTerms.size()-1; ++i)
			for(unsigned j=i+1; j<copyOfTerms.size(); ++j) {
				std::string key = copyOfTerms[i]+" "+copyOfTerms[j]; 
				//COUT << i << " " << j << " |" << key << "|" << cache.count(key)<< ENDL;
				if(cache.count(key) && max<cache[key])
					max = cache[key];
			}
		return max;
	}
};
*/
