#ifndef INTERFACES_H_
#define INTERFACES_H_

#include <string>
#include <vector>

#include "types.h"

inline const std::string aux__nameForType(float) { return "float"; }
inline const std::string aux__nameForType(unchar) { return "unchar"; }
inline const std::string aux__nameForType(unsigned) { return "unsigned"; }

// a class can enter the registry only if it has a unique non-empty name and a "getOne function that returns ptr to newly constructed instance
class RegistryEntry {
public:
	virtual const std::string name() const = 0;
	virtual void* getOne() const = 0;
	virtual ~RegistryEntry(){}
};
//=============================================================================================
class LexiconInterface;

template <typename T>
class BringCompressedDataInterface {
public:
	//not const, as those could change the internal state of the bringing system (caching, etc.)
	virtual void bring(const std::string& term, T& dest) = 0;
	virtual void setPath(const std::string& path)=0;
	virtual void setLexicon(LexiconInterface* lex)=0;
	//virtual void bring(const unsigned tid, std::vector<unsigned>& dest) = 0;

	virtual ~BringCompressedDataInterface(){}
};
//=============================================================================================
class LexiconInterface {
public:
	virtual void loadLexicon(const std::vector<std::string>* terms=nullptr, bool calcAugments=true)=0;
	virtual void setPath(const std::string& path)=0;
	virtual did_t getListLength(const std::string& term)const= 0;
	virtual did_t getRawSize(const std::string& term) = 0;
	virtual long int getLocation(const std::string& term) = 0;
	virtual const void* getConstListHandleUnsafe(const std::string& term)=0; //ouch, the other choice is to make it a template...
	virtual tid_t getTid(const std::string& term) const = 0;

	virtual ~LexiconInterface() {}
};
//=============================================================================================
class PostingListReader {
public:
	virtual ~PostingListReader() {}
	virtual unsigned getList(const char* term, unsigned int wid, std::vector<did_t>& docids, std::vector<unsigned>& freqs) = 0;
};
//=============================================================================================
class IndexBuilderFilterInterface {
public:
	virtual bool good(const std::string& term)  = 0;
	virtual bool good(const unsigned long tid)  = 0;
	virtual ~IndexBuilderFilterInterface(){}
};
//=============================================================================================
class CompressionInterface {
public:
	//a general signature -- some compressions might not need all those
	//assume -- we decompress a single block and compress a single block

	//src -- compressed data
	//dest -- where to write the decompressed elements
	//compressionFlags -- all you need to know about the compressed block with respect to an algorithm
	//compressedSizeInBytes -- just in case. sometimes it can be understood from flags
	//return -- how many bytes where read -- again, it could be compressedSizeInBytes most of the time.
	virtual unsigned decompressToInts(const unsigned* src, unsigned* dest, int compressionFlags, unsigned compressedSizeInBytes) = 0;
	virtual unsigned decompressToShorts(const unsigned* src,unshort* dest, unsigned compressionFlags, unsigned compressedSizeInBytes) { return 0; }
	virtual unsigned decompressToBytes(const unsigned* src, unchar* dest, unsigned compressionFlags, unsigned compressedSizeInBytes) { return 0; }

	//src -- original values
	//dest -- where the compressed data will be (most often -- a temp. buffer)
	//count -- how many values (probably block-size)
	//returns the flag and a size in bytes of the compressed data (yes, flag could in theory have it already)
	virtual std::pair<unsigned,unsigned> compressInts(const unsigned* src, unsigned* dest, unsigned count) = 0;
	virtual std::pair<unsigned,unsigned> compressShorts(const unshort* src, unsigned* dest, unsigned count) { return  std::pair<unsigned,unsigned>(0,0); }
	virtual std::pair<unsigned,unsigned> compressBytes(const unchar* src, unsigned* dest, unsigned count) { return  std::pair<unsigned,unsigned>(0,0); }

	virtual const std::string getName() const = 0;
	virtual ~CompressionInterface(){}
};


class LowLevelListAccessorInterface {
public:
	virtual ~LowLevelListAccessorInterface(){}

	virtual void entireBlockDidsAndScores(did_t* dids, float* scores, const did_t block, const int instrSet=1) const = 0; //decode given block and calc scores
	virtual void entireBlockDidsAndScores(did_t* dids, unchar* scores, const did_t block, const int instrSet=1) const = 0; //decode given block and point to q. scores

	virtual void buildBM(const did_t* docIds, const float* scores, unchar* blockMaxes, const unsigned size, const did_t smallest) const = 0;
	virtual void buildBM(const did_t* docIds, const unchar* scores, unchar* blockMaxes, const unsigned size, const did_t smallest) const = 0;

	virtual void batchUnpackingBlocks(did_t endBlock, did_t* dids, float* scores, did_t startBlock=0, const int instrSet=1) const = 0;
	virtual void batchUnpackingBlocks(did_t endBlock, did_t* dids, unchar* scores, did_t startBlock=0, const int instrSet=1) const = 0;

	virtual void batchUnpackingBlocksUptoDid(did_t& postings, did_t nextWindowsFirst, did_t* realDids, float* scores, did_t& block, const int instrSet=1) const = 0;
	virtual void batchUnpackingBlocksUptoDid(did_t& postings, did_t nextWindowsFirst, did_t* realDids, unchar* scores, did_t& block, const int instrSet=1) const = 0;

};

//=============================================================================================
union ScoresArrayPtr {
	float* floats;
	unchar* bytes;
};
//=============================================================================================
class AugmentsHelperInterface  {
public:
	virtual ~AugmentsHelperInterface() {}

	virtual void buildLivenessBitvector(unchar* dest, void** blockScores, unchar** postingBitsets, const unchar threshold, const unsigned termsCount, const unsigned windowsize)=0;
	virtual void buildPBS_M3(unsigned* dest, const unsigned* dids, const unsigned winStartDid, unsigned bits, const unsigned sz)=0;
	virtual void buildBM(LowLevelListAccessorInterface* list, const did_t* docIds, const void* scores, void* blockMaxes, const unsigned size, const did_t smallest)=0;

	virtual void entireBlockDidsAndScores(LowLevelListAccessorInterface* list, did_t* dids, void* scores, const did_t block) const = 0; //decode given block and calc scores
	virtual void batchUnpackingBlocks(LowLevelListAccessorInterface* list, did_t endBlock, did_t* dids, void* scores, did_t startBlock=0) const = 0;
	virtual void batchUnpackingBlocksUptoDid(LowLevelListAccessorInterface* list, did_t& postings, did_t nextWindowsFirst, did_t* realDids, void* scores, did_t& block) const = 0;

};

#endif /* INTERFACES_H_ */
