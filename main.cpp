int mainBuilder(int argc, char * argv[] );
int mainRunner(int argc, char * argv[] );

#include "options.h"

Options parseCommandLineArgs(int argc, char * argv[]) {
	argc-=(argc>0); argv+=(argc>0); // skip program name argv[0]
	Options options;
	options.parseFromCmdLine(argc,argv);
	return options;
}

int main(int argc, char * argv[] ) {
#ifdef BUILDER
	return mainBuilder(argc,argv);
#else
	return mainRunner(argc,argv);
#endif
	return -1;
}
