#ifndef COMPRESSION_H_
#define COMPRESSION_H_

#include "types.h"
#include "Interfaces.h"

inline const unsigned int *packDecodeDidsOrFreqs(unsigned int *dstDecopmressedPtr, const unsigned int *srcCompressedPtr, int flag, int base, CompressionInterface* unpacker) {
	assert(base>=0);
	srcCompressedPtr+= unpacker->decompressToInts(srcCompressedPtr,dstDecopmressedPtr,flag,0);
	dstDecopmressedPtr[0] += base; //assume base = 0 for freqs
	return srcCompressedPtr;
}

inline const unsigned int *packDecodeFreqs(unsigned int *destDecomressedPtr, const unsigned int *srcCompressedPtr, int flag, CompressionInterface* unpacker) {
	return packDecodeDidsOrFreqs(destDecomressedPtr,srcCompressedPtr,flag,0,unpacker);
}

inline const unsigned int *packDecodeDids(unsigned int *destDecomressedPtr, const unsigned int *srcCompressedPtr, int flag,int base, CompressionInterface* unpacker) {
	return packDecodeDidsOrFreqs(destDecomressedPtr,srcCompressedPtr,flag,base,unpacker);
}



#endif /* COMPRESSION_H_ */
