#ifndef AUGMENTSLITTLEHELPERS_H_
#define AUGMENTSLITTLEHELPERS_H_

template <typename scoreType>
class AugmentsQuantBlockMaxSSE : public AugmentsHelperInterface, public RegistryEntry {
public:
	AugmentsQuantBlockMaxSSE(){}

	virtual const std::string name() const;
	virtual void* getOne() const;

	virtual void buildLivenessBitvector(unchar* dest, void** blockScores, unchar** postingBitsets, const unchar threshold, const unsigned termsCount, const unsigned windowsize);
	virtual void buildPBS_M3(unsigned* dest, const unsigned* dids, const unsigned winStartDid, unsigned bits, const unsigned sz);
	virtual void buildBM(LowLevelListAccessorInterface* list, const did_t* docIds, const void* scores, void* blockMaxes, const unsigned size, const did_t smallest);

	virtual void entireBlockDidsAndScores(LowLevelListAccessorInterface* list, did_t* dids, void* scores, const did_t block) const; //decode given block and calc scores
	virtual void batchUnpackingBlocks(LowLevelListAccessorInterface* list, did_t endBlock, did_t* dids, void* scores, did_t startBlock=0) const;
	virtual void batchUnpackingBlocksUptoDid(LowLevelListAccessorInterface* list, did_t& postings, did_t nextWindowsFirst, did_t* realDids, void* scores, did_t& block) const;
};

template <typename scoreType>
class AugmentsQuantBlockMaxScalar : public AugmentsHelperInterface, public RegistryEntry {
public:
	AugmentsQuantBlockMaxScalar(){}

	virtual const std::string name() const;
	virtual void* getOne() const;

	virtual void buildLivenessBitvector(unchar* dest, void** blockScores, unchar** postingBitsets, const unchar threshold, const unsigned termsCount, const unsigned windowsize);
	virtual void buildPBS_M3(unsigned* dest, const unsigned* dids, const unsigned winStartDid, unsigned bits, const unsigned sz);
	virtual void buildBM(LowLevelListAccessorInterface* list, const did_t* docIds, const void* scores, void* blockMaxes, const unsigned size, const did_t smallest);

	virtual void entireBlockDidsAndScores(LowLevelListAccessorInterface* list, did_t* dids, void* scores, const did_t block) const; //decode given block and calc scores
	virtual void batchUnpackingBlocks(LowLevelListAccessorInterface* list, did_t endBlock, did_t* dids, void* scores, did_t startBlock=0) const;
	virtual void batchUnpackingBlocksUptoDid(LowLevelListAccessorInterface* list, did_t& postings, did_t nextWindowsFirst, did_t* realDids, void* scores, did_t& block) const;
};
//=============================================================================================

template <typename scoreType>
const std::string AugmentsQuantBlockMaxSSE<scoreType>::name() const { return  "AugmentsQuantBlockMaxSSE<"+aux__nameForType(scoreType())+">"; }
//=============================================================================================
template <typename scoreType>
void* AugmentsQuantBlockMaxSSE<scoreType>::getOne() const { return new AugmentsQuantBlockMaxSSE<scoreType>(); }
//=============================================================================================
template <typename scoreType>
void AugmentsQuantBlockMaxSSE<scoreType>::entireBlockDidsAndScores(LowLevelListAccessorInterface* list, did_t* dids, void* scores, const did_t block) const { //decode given block and calc scores
	list->entireBlockDidsAndScores(dids,reinterpret_cast<scoreType*>(scores),block,1); //1 for sse
}
//=============================================================================================
template <typename scoreType>
void AugmentsQuantBlockMaxSSE<scoreType>::batchUnpackingBlocksUptoDid(LowLevelListAccessorInterface* list, did_t& postings, did_t nextWindowsFirst, did_t* realDids, void* scores, did_t& block) const {
	list->batchUnpackingBlocksUptoDid(postings,nextWindowsFirst,realDids,reinterpret_cast<scoreType*>(scores),block,1); //1 for sse
}
//=============================================================================================
template <typename scoreType>
void AugmentsQuantBlockMaxSSE<scoreType>::batchUnpackingBlocks(LowLevelListAccessorInterface* list, did_t endBlock, did_t* dids, void* scores, did_t startBlock) const {
	list->batchUnpackingBlocks(endBlock,dids,reinterpret_cast<scoreType*>(scores),startBlock,1); //1 for sse
}
//=============================================================================================
template <typename scoreType>
void AugmentsQuantBlockMaxSSE<scoreType>::buildBM(LowLevelListAccessorInterface* list, const did_t* docIds, const void* scores, void* blockMaxes, const unsigned size, const did_t smallest){
	list->buildBM(docIds,reinterpret_cast<const scoreType*>(scores), reinterpret_cast<unchar*>(blockMaxes), size, smallest); //quantize as you go
}
//=============================================================================================
template <typename scoreType>
void AugmentsQuantBlockMaxSSE<scoreType>::buildPBS_M3(unsigned* dest, const unsigned* dids, const unsigned winStartDid, unsigned bits, const unsigned sz) {
#ifdef USEPOSTINGS
	memset(dest,0,CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK);
	buildPBS_M3_SSE_(dest,dids,winStartDid,bits,sz);
#endif
}
//=============================================================================================
template <typename scoreType>
void AugmentsQuantBlockMaxSSE<scoreType>::buildLivenessBitvector(unchar* dest, void** blockScores, unchar** postingBitsets, const unchar threshold, const unsigned termsCount, const unsigned windowsize) {
	buildLiveAreaQuantSSE_(dest,reinterpret_cast<unchar**>(blockScores),postingBitsets,threshold,termsCount,windowsize);
}
//=============================================================================================
template <typename scoreType>
const std::string AugmentsQuantBlockMaxScalar<scoreType>::name() const { return  "AugmentsQuantBlockMaxScalar<"+aux__nameForType(scoreType())+">"; }
//=============================================================================================
template <typename scoreType>
void* AugmentsQuantBlockMaxScalar<scoreType>::getOne() const { return new AugmentsQuantBlockMaxScalar<scoreType>(); }
//=============================================================================================
template <typename scoreType>
void AugmentsQuantBlockMaxScalar<scoreType>::entireBlockDidsAndScores(LowLevelListAccessorInterface* list, did_t* dids, void* scores, const did_t block) const { //decode given block and calc scores
	list->entireBlockDidsAndScores(dids,reinterpret_cast<scoreType*>(scores),block,0);//0 for scalar
}
//=============================================================================================
template <typename scoreType>
void AugmentsQuantBlockMaxScalar<scoreType>::batchUnpackingBlocksUptoDid(LowLevelListAccessorInterface* list, did_t& postings, did_t nextWindowsFirst, did_t* realDids, void* scores, did_t& block) const {
	list->batchUnpackingBlocksUptoDid(postings,nextWindowsFirst,realDids,reinterpret_cast<scoreType*>(scores),block,0);//0 for scalar
}
//=============================================================================================
template <typename scoreType>
void AugmentsQuantBlockMaxScalar<scoreType>::batchUnpackingBlocks(LowLevelListAccessorInterface* list, did_t endBlock, did_t* dids, void* scores, did_t startBlock) const {
	list->batchUnpackingBlocks(endBlock,dids,reinterpret_cast<scoreType*>(scores),startBlock,0); //0 for scalar
}
//=============================================================================================
template <typename scoreType>
void AugmentsQuantBlockMaxScalar<scoreType>::buildBM(LowLevelListAccessorInterface* list, const did_t* docIds, const void* scores, void* blockMaxes, const unsigned size, const did_t smallest) {
	list->buildBM(docIds,reinterpret_cast<const scoreType*>(scores), reinterpret_cast<unchar*>(blockMaxes), size, smallest);
}
//=============================================================================================
template <typename scoreType>
void AugmentsQuantBlockMaxScalar<scoreType>::buildPBS_M3(unsigned* dest, const unsigned* dids, const unsigned winStartDid, unsigned bits, const unsigned sz) {
#ifdef USEPOSTINGS
	memset(dest,0,CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK);
	buildPBS_M3_Scalar_(dest,dids,winStartDid,bits,sz);
#endif
}
//=============================================================================================
template <typename scoreType>
void AugmentsQuantBlockMaxScalar<scoreType>::buildLivenessBitvector(unchar* dest, void** blockScores, unchar** postingBitsets, const unchar threshold, const unsigned termsCount, const unsigned windowsize) {
	//buildLiveAreaQuantSSE_(dest,reinterpret_cast<unchar**>(blockScores),postingBitsets,threshold,termsCount,windowsize);
	buildLiveAreaQuantScalar_(dest,reinterpret_cast<unchar**>(blockScores),postingBitsets,threshold,termsCount,windowsize);
}
//=============================================================================================

#endif /* AUGMENTSLITTLEHELPERS_H_ */
