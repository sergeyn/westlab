#ifndef LISTMETADATA_H_
#define LISTMETADATA_H_

#include "Interfaces.h"
#include "globals.h"
#include "OnDemandResourceManager.h"
//=============================================================================================
class ListMetaData {
private:
	did_t unpaddedLength;
	tid_t tid;
	std::string term;
	long int offsetInFile;
	did_t compressedDataLengthBytes;

	OnDemandManager<std::string,vecUInt>* dataBringer;

	vecUInt flags;							// the pfor flag for each chunk -- Shuai!!!
	vecUInt sizePerBlock;					// the compressed size for each chunk
	vecUInt maxDidPerBlock;					// the max value for each chunk

public:


	ListMetaData() : unpaddedLength(0), tid(0), offsetInFile(0), compressedDataLengthBytes(0),dataBringer(nullptr) {}

	void setOnDemandManager(OnDemandManager<std::string,vecUInt>* dataBringer) { this->dataBringer = dataBringer; }

	const unsigned* getMaterializedData() const { assert(nullptr != dataBringer); return dataBringer->getMaterializedData(term).data(); }

	did_t getCompressedDataLengthBytes() const { return compressedDataLengthBytes; 	}
	void setCompressedDataLengthBytes(did_t compressedDataLengthBytes) { this->compressedDataLengthBytes = compressedDataLengthBytes;	}

	long int getOffsetInFile() const { return offsetInFile; }
	void setOffsetInFile(long int offsetInFile) { this->offsetInFile = offsetInFile; }

	const std::string& getTerm() const { return term; }
	void setName(const std::string& name) { term=name; /*compressedData.setName(name);*/ }

	tid_t getTid() const { return tid; }
	void setTid(tid_t id) { tid = id; }

	did_t getUnpaddedLength() const { return unpaddedLength; }
	void setUnpaddedLength(did_t unpaddedLength) { 	this->unpaddedLength = unpaddedLength;	}

	const unsigned* getFlagsPtr() const { return flags.data(); }
	const unsigned* getSizePerBlockPtr() const { return sizePerBlock.data(); }
	const did_t*    getMaxDidPerBlockPtr() const { return maxDidPerBlock.data(); }

	const vecUInt& getFlagsPerBlock() const { return flags; }
	const vecUInt& getSizePerBlock() const { return sizePerBlock; }
	const vecUInt& getMaxDidPerBlock() const { return maxDidPerBlock; }

	void swapFlags(vecUInt& rhs) { flags.swap(rhs); }
	void swapSizePerBlock(vecUInt& rhs) { sizePerBlock.swap(rhs); }
	void swapMaxDidsPerBlock(vecUInt& rhs) { maxDidPerBlock.swap(rhs);}

};

#endif /* LISTMETADATA_H_ */
