#ifndef UTILS_H_IM
#define UTILS_H_IM

template<typename T>
T* Registry::getInstanceOf(const std::string& className) {
	if(!registry.count(className)) FATAL("["+className+"] was not registered");
	T* ptr = reinterpret_cast<T*>(registry[className]->getOne());
	return ptr;
}

template<typename T>
void  Registry::registerClass() {
	registerClass(new T);
}

// Knuth trick for comparing floating numbers
inline bool FloatEquality( float a, float b) { // check if a and b are equal with respect to the defined tolerance epsilon
	//const float epsilon = 0.0001; // or some other small number (for our case it is sufficient)
	if ((std::fabs(a-b) <= CONSTS::NUMERIC::EPSILON*std::fabs(a)) && (std::fabs(a-b) <= CONSTS::NUMERIC::EPSILON*std::fabs(b))) return true;
	else return false; }

inline bool FloatComparisonPrivate( float a, float b) { // compare two floats ONLY for greater or smaller and NOT for equality
	//if (a > b) return true; 	else return false;
	return (a>b);
}

// Arguments a,b and returns a) 0 in case of equality, b) 1 in case a > b, and c) -1 in case a < b
inline int Fcompare( float a, float b) {
	if (FloatEquality(a, b)) return 0; // equal
	else { if (FloatComparisonPrivate(a, b)) return 1;  // a > b
	else return -1; }      // a < b
}

/*
	inline void Get_Percentiles(lptrArray& lps, std::vector<float>& percentiles, const float& threshold) {
		const float frequency = lps[i]->getFreq();
		const float score = lps[i]->calcScore(frequency,pages[smallest_did]);
	}
 */

inline uint32_t intlog2(const uint32_t x) {
	uint32_t y;
	asm ( "\tbsr %1, %0\n"
			: "=r"(y)
			  : "r" (x)
	);
	return y;
}

inline unsigned int NearestPowerOf2( unsigned int x ){ //test this first...
	return 1 << (1+intlog2(x));
}

template <typename T> //stream vectors
std::ostream& operator<< (std::ostream& s, const std::vector<T>& t) {
	for(auto& it : t)
		s << it << ' ';
	return s;
}

template<typename T>
PriorityArray<T>::PriorityArray(const size_t sz){ setSizeLimit(sz); }
	//std::fill(minData.begin(),minData.end(),std::numeric_limits<T>::min());

template<typename T>
inline void PriorityArray<T>::setSizeLimit(unsigned sz) {size=sz; minData.clear(); minData.resize(size+1);}

template<typename T>
inline const std::vector<T>& PriorityArray<T>::getV() const { return minData;}

template<typename T>
inline void PriorityArray<T>::putInArray(T* ptr) {sortData(); std::copy(minData.data(),minData.data()+minData.size(), ptr); }

template<typename T>
inline const T PriorityArray<T>::safehead() const {
	if(!minData[0].isInValid()) return minData[0];
	T minV = minData[0];
	for(const T& r : minData) if(!r.isInValid() && minV>r) minV=r;
	return minV;
}

template<typename T>
inline const T& PriorityArray<T>::fasthead() const { return minData[0]; }

template<typename T>
inline void PriorityArray<T>::sortData() { std::sort(minData.begin(), minData.end()); std::reverse(minData.begin(), minData.end()); }

template<typename T>
inline bool PriorityArray<T>::push(const void* val) {
	return push(*(reinterpret_cast<T*>(val)));
}

template<typename T>
inline bool PriorityArray<T>::push(const T& val){
	//are we better than the smallest top-k member?
	if(minData[0]>val)
		return false;
	minData[0] = val;
	for (unsigned int i = 0; (2*i+1) < size; ){
		unsigned int next = 2*i + 1;
		if (minData[next] > minData[next+1])
			++next;
		if (minData[i] > minData[next])
			std::swap(minData[i], minData[next]);
		else
			break;
		i = next;
	}
	return true;
}


template <typename T>
inline T max3(const T& a, const T& b, const T& c) {
	if(a<b) return (b<c) ? c : b;
	else return (a<c) ? c : a;
}

template <typename T>
inline T min3(const T& a, const T& b, const T& c) {
	if(a>b) return (b>c) ? c : b;
	else return (a>c) ? c : a;
}

inline void NEWMA(void** ptr,const unsigned bytecounts) {
	char *ptrT = new char[bytecounts];
	ptr[0] = (void*) ptrT;
	//if(posix_memalign(ptr,128,bytecounts)) FATAL("NEWMA failed");
	//allocatedSum += bytecounts;
	//COUTL << " allocated " << bytecounts << " total: " << allocatedSum << ENDL;
}

//instead of splitting in middle as std::lower_bound starts with begin and hops in powers of two
// -- better for cases where the value of interest is mostly located near begin rather than uniformly distrib.
template<typename _FIter, typename _Tp>
_FIter gallop_up_lower_bound(_FIter begin, _FIter end, const _Tp& val) {
	assert(*(end-1) >= val); //the search must end -- rowinski is a problem again
	unsigned skip = 1;
	while(begin<end) {
		if(val == *begin)	return begin;
		else if(*begin < val) {
			begin = begin+skip;
			skip <<=1;
		}
		else //the last skip was too much
			return std::lower_bound(begin-(skip>>1),begin,val);
	}
	//the last skip was way too much
	return std::lower_bound(begin-(skip>>1),end,val);
}

//instead of splitting in middle as std::lower_bound starts with begin and hops in powers of two
// -- better for cases where the value of interest is mostly located near begin rather than uniformly distrib.
//this is not the same convention as std:: version -- we need it for nextGEQing
template<typename _FIter, typename _Tp>
_FIter gallop_down_lower_bound(_FIter begin, _FIter curr, const _Tp& val) {
	assert(*(curr) > val); //the search must end
	unsigned skip = 1;
	while(begin<curr) {
		if(val == *curr)	return curr;
		else if(*curr > val) {
			curr = curr-skip;
			skip <<=1;
		}
		else //the last skip was too much
			return std::lower_bound(curr,curr+(skip>>1),val);
	}
	//the last skip was way too much
	return std::lower_bound(begin,curr+(skip>>1),val);
}

template <typename Iterator> //make ofstream version, really
long int appendToFile(const std::string& path, Iterator start, Iterator end) {
	FILE *fdo = fopen(path.c_str(),"a");
	if(! fdo) FATAL("Failed writing to " + path );
	std::vector<typename Iterator::value_type> tmpcpy(start,end);
	const int res = fwrite(tmpcpy.data(),sizeof(typename Iterator::value_type),tmpcpy.size(),fdo);
	const long int offset = ftell(fdo);
	fclose(fdo);

	if((unsigned)res!=tmpcpy.size()) FATAL("Failed writing to " + path );
	//for (; start !=end; ++start) if (fwrite(&(*start),sizeof(typename Iterator::value_type) , 1, fdo) != 1) FATAL("Failed writing to " + path );
	return offset;
}


//=============================================================================================
//also check - http://stackoverflow.com/questions/2741065/what-is-the-return-value-of-sched-find-first-bit-if-it-doesnt-find-anything
//note! if lands in live-bit with _close_ neigbor (within one word) that is live will return THAT guy!
inline unsigned	getNextSetBit(const void* ptrBS, const unsigned index) { //assume last is always set!
	//note! if usetype is long (64 bits) you must use ctzl, if smaller, use ctz!
	typedef uint64_t usetype;
	const unsigned logBitsInType = 6;
	const unsigned bitsInType =  8*sizeof(usetype);
	static_assert(1<<logBitsInType == 8*sizeof(usetype),"1<<logBitsInType != 8*sizeof(usetype)");

	const unsigned packIndex = index>>logBitsInType;
	const usetype *ptr =  reinterpret_cast<const usetype *>(ptrBS);

	unsigned offsetInPacked = index%bitsInType;
	if(ptr[packIndex]>>offsetInPacked)
		return (packIndex*bitsInType)+offsetInPacked+__builtin_ctzl(ptr[packIndex]>>offsetInPacked);

	unsigned int i=packIndex+1;
	for(; ptr[i]==0; ++i); //skip to next live pack

	unsigned packOffset = __builtin_ctzl(ptr[i]);
	return (i<<logBitsInType) + packOffset;
}

inline bool isBitSet(const unchar* bs, unsigned bit) { return (1<<(bit%8))&(bs[bit>>3]);}
#endif /* UTILS_H_ */
