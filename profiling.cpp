/*
 * profiling.cpp
 *
 *  Created on: Jan 12, 2012
 *      Author: sergeyn
 */
#include "profiling.h"
#include "globals.h"
#include "Enums.h"

profilerC profilerC::GlobalProfiler;
dummyProfiler dummyProfiler::dummy;

profilerC::profilerC() :cycles(0) {
	//setCosts();
	for(unsigned i=0; i<enm::NOP; ++i) {
		initNewCounter();  //initialize named counters
		add(convert(enm::counters(i)),i); //init named timers
	}
}

void profilerC::printReport() const {
	std::map<int,sTimer>::const_iterator it;
	for ( it=timers.begin() ; it != timers.end(); it++ ) {
		int id = (*it).first;
		const sTimer& t = (*it).second;
		const std::string cm((*comments.find(id)).second);
		if(t.getCount()<1) {
			COUTV(5) << "skipping timer " << cm << ENDL;
			continue;
		}

		unsigned long count = t.getCount();
		double total = 	t.getTotal()/1000.0; //in ms
		
		COUT1 << id << ": "<< cm <<
		" total(ms): " << total /*-(cycleCost*(t.getCount()/100000000))*/
	 	<< /*"(" << total << */" count: " << count  <<
		" avg: " <<   total/count << ENDL;

	}
	unsigned i = 0;
	for(unsigned long c : counters) {
		if(c>0) {COUT1 << i << ": " << (convert(enm::counters(i))) << " count: " << c << ENDL;}
		++i;
	}
}

void thrashSomeCache(const unsigned size) {
             char *c = (char *)malloc(size);
             for (int i = 0; i < 0xffff; i++)
               for (int j = 0; j < size; j++)
                 c[j] = i^j;
             free(c);
}
