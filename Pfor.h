#ifndef _PFOR_H_
#define _PFOR_H_


/*
Its gonna take time
A whole lot of precious time
Its gonna take patience and time, ummm
To do it, to do it, to do it, to do it, to do it,
To do it right, child!
George Harrison
*/

#include "globals.h"
#include "Interfaces.h"

class OldPforDeltaWithOldPacker : public CompressionInterface, public RegistryEntry {
public:
	OldPforDeltaWithOldPacker(){}
	virtual const std::string name() const { return "OldPforDelta"; }
	virtual const std::string getName() const { return name(); }
	virtual void* getOne() const { return new OldPforDeltaWithOldPacker(); }
	//src -- compressed data
	//dest -- where to write the decompressed elements
	//compressionFlags -- all you need to know about the compressed block with respect to an algorithm
	//compressedSizeInBytes -- just in case. sometimes it can be understood from flags
	//return -- how many bytes where read -- again, it could be compressedSizeInBytes most of the time.
	virtual unsigned decompressToInts(const unsigned* src, unsigned* dest, int compressionFlags, unsigned compressedSizeInBytes);

	//src -- original values
	//dest -- where the compressed data will be (most often -- a temp. buffer)
	//count -- how many values (probably block-size)
	//returns the flag and a size in bytes of the compressed data (yes, flag could in theory have it already)
	virtual std::pair<unsigned,unsigned> compressInts(const unsigned* src, unsigned* dest, unsigned count);
};

typedef OldPforDeltaWithOldPacker OldPforDelta ;

class OldPforDeltaWithNewPacker : public CompressionInterface, public RegistryEntry {
public:
	OldPforDeltaWithNewPacker(){}
	virtual const std::string name() const { return "OldPforDeltaWithNewPacker"; }
	virtual const std::string getName() const { return name(); }
	virtual void* getOne() const { return new OldPforDeltaWithNewPacker(); }

	virtual unsigned decompressToInts(const unsigned* src, unsigned* dest, int compressionFlags, unsigned compressedSizeInBytes);
	virtual std::pair<unsigned,unsigned> compressInts(const unsigned* src, unsigned* dest, unsigned count);
};

#endif
