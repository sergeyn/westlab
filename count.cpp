#include <iostream>
#include <fstream>
#include <unordered_set>
#include "globals.h"
#include "utils.h"
#include "Enums.h"

//#define USEPBSINCOUNT
#define USEBMQCINCOUNT

const std::string termsList("/data1/team/sergeyn/git/newTopK/QueryLog/uniqTerms_from1k");
const std::string termLenFreq("/data1/team/sergeyn/git/newTopK/termLenFreq");

class listUnderPolicy {
public:
	static unsigned didbits;
	float score;
	unsigned length;
	float size; //space budget in bytes
	float time; //preproc. speedup in ms.
	enm::prprPolicy policy;
	std::string term;

	static inline float getGoldOTFtime(const unsigned len) {
		return ((1.37471482f/100000.0f)*len+0.4f);
	}

	static inline float getGoldBMQCspace(const unsigned len) {
        return 0.6647780492f*len+25520.1741f;
	}

	static inline float getGoldBMQCtime(const unsigned len) {
		return  getGoldOTFtime(len)-((4.906487456f/1000000.0f)*len+0.4170874391f);
	}


	listUnderPolicy(const std::string& trm, enm::prprPolicy plcy, const unsigned len, const unsigned freq) : length(len), policy(plcy), term(trm) {
		if(plcy == enm::ONTHEFLY) {
			size = 0.0000001;
			time = 0;
		}
		else if(plcy == enm::BMQC_PBS) { //note: there is no BMQC_FAKE as BMQC<2^18
			size = getGoldBMQCspace(len);
#ifdef USEPBSINCOUNT
			size += CONSTS::NUMERIC::MAXD>>didbits;
#endif
			time = getGoldBMQCtime(len);
		}
		else if(plcy == enm::BMQ_FAKE || plcy == enm::BMQ_PBS) {
			size = CONSTS::NUMERIC::MAXD>>didbits;
#ifdef USEPBSINCOUNT
			if(plcy == enm::BMQ_PBS)
				size += CONSTS::NUMERIC::MAXD>>didbits;
#endif
			time = getGoldOTFtime(len)-0.1387279378f;
		}
		score = (time*freq)/size;
	}

	inline bool operator<(const listUnderPolicy& rhs) const  { return score>rhs.score; } //reverse sorting order
};
unsigned listUnderPolicy::didbits(6);

class CostBased {
	std::vector<listUnderPolicy> allTerms;
	std::vector<unsigned> lengths;
	std::unordered_set<std::string> queryTerms;
public:
	explicit CostBased(const std::string& trainFile) {
		allTerms.reserve(CONSTS::NUMERIC::MAXTERMS);
		lengths.reserve(CONSTS::NUMERIC::MAXTERMS);
		std::ifstream train(trainFile);
		std::string term;
		unsigned len,freq;
		while(train.good()) {
			train >>term>>len>>freq;
			freq += 1; //"smooth" even more
			lengths.push_back(len);
			allTerms.push_back(listUnderPolicy(term,enm::ONTHEFLY,len,freq));
			if(len>(1<<12)) { //BMQ area
				#ifdef USEBMQCINCOUNTING
				 if(len>(1<<15) && len<(1<<18)) {  //BMQC
					allTerms.push_back(listUnderPolicy(term,enm::BMQC_PBS,len,freq));
				}else 
				#endif
				{ //BMQ
					if(len<(1<<22)) //BMQ_PBS
						allTerms.push_back(listUnderPolicy(term,enm::BMQ_PBS,len,freq));
					else
						allTerms.push_back(listUnderPolicy(term,enm::BMQ_FAKE,len,freq));

				}
			}//end if bmq
		} //end while
		CERR << "Loaded" << ENDL;
		std::sort(allTerms.begin(),allTerms.end());
		CERR << "Sorted" << ENDL;
	} 

	void loadQueryTerms(const std::string& termsUniqFile) {
		std::ifstream terms(termsUniqFile);
		std::string term;
		while(terms.good()) {
			terms >>term;
			queryTerms.insert(term);
		}
	}

	void makePolicyFile(const std::string& fname, float sizeInBytes) {
		std::ofstream out(fname, std::ios_base::out);
		float budget = 0;
		for(unsigned i=0; i<allTerms.size() && budget<sizeInBytes; ++i) {
			budget+=allTerms[i].size;
			if(queryTerms.count(allTerms[i].term)) //we have it in query log:
				out << allTerms[i].term << " " << allTerms[i].policy << ENDL;
		}
	}

	float getSizeOfStatic(unsigned stop1, unsigned stop2) {
		const std::vector<unsigned>& lens = lengths;
		float spaceUnderBits = 0;
		float blocksUnderBits =  0.0001 + (CONSTS::NUMERIC::MAXD>>listUnderPolicy::didbits);

		for(unsigned i=0; i<lens.size(); ++i) {
			unsigned len = lens[i];
			if(len<stop1 || len<4096) { //OTF
			}
			else if (len < stop2 && len>(1<<15) ) {// #bmqc -- note the barrier
				spaceUnderBits += listUnderPolicy::getGoldBMQCspace(len);
#ifdef USEPBSINCOUNT
				spaceUnderBits += blocksUnderBits;
#endif
			}
			else if( len < (1<<22) ){//#bmq_pbs
				spaceUnderBits += blocksUnderBits ;
#ifdef USEPBSINCOUNT
				spaceUnderBits += blocksUnderBits;
#endif
			}
			else	{ //bmq_fake
				spaceUnderBits += blocksUnderBits ;
			}
		}
		return spaceUnderBits;
	}

	unsigned getDiagonal(const float budget, unsigned left, unsigned right){
		if(right-left<2)
			return left;
		unsigned middle = (right+left)>>1;
		float midspace = getSizeOfStatic(middle,middle);
		if(midspace>budget) //we have extra space, let's have more preprocessed:
			return getDiagonal(budget,middle,right);
		return getDiagonal(budget,left,middle);
	}

	unsigned getHor(const float budget, unsigned left, unsigned right, const unsigned bmqc=(1<<18)){
		if(right-left<2)
			return left;
		unsigned middle = (right+left)>>1;
		float midspace = getSizeOfStatic(middle,bmqc);
		if(midspace>budget) //we have extra space, let's have more preprocessed:
			return getHor(budget,middle,right,bmqc);
		return getHor(budget,left,middle,bmqc);
	}

	void findTwoStaticPoints(float spaceBudget) {
		unsigned place = getDiagonal(spaceBudget,0,CONSTS::NUMERIC::MAXD);
		unsigned place2 = getHor(spaceBudget,0,CONSTS::NUMERIC::MAXD);
		std::cout << place << " " << place << " "  << getSizeOfStatic(place,place) << " "  << place2 <<" "<<(1<<18)<< " " << getSizeOfStatic(place2,1<<18)<< std::endl;
	}

};


void doAllCounting() {
	CostBased cb(termLenFreq);
/*
	float gb = 1<<30;
	cb.findTwoStaticPoints(0.5f*gb);
	cb.findTwoStaticPoints(1.f*gb);
	cb.findTwoStaticPoints(1.53f*gb);
	cb.findTwoStaticPoints(2.f*gb);
	cb.findTwoStaticPoints(3.f*gb);
	cb.findTwoStaticPoints(4.f*gb);
	cb.findTwoStaticPoints(5.f*gb); 
	cb.findTwoStaticPoints(6.f*gb); 
*/

	cb.loadQueryTerms(termsList);
	cb.makePolicyFile("/tmp/05g_policy",0.5f * ((float) (1<<30) ) );
	cb.makePolicyFile("/tmp/1g_policy",1.0f * ((float) (1<<30) ) );
	cb.makePolicyFile("/tmp/2g_policy",2.0f * ((float) (1<<30) ) );
	cb.makePolicyFile("/tmp/3g_policy",3.0f * ((float) (1<<30) ) );
	cb.makePolicyFile("/tmp/4g_policy",4.0f * ((float) (1<<30) ) );
	cb.makePolicyFile("/tmp/5g_policy",5.0f * ((float) (1<<30) ) );
	cb.makePolicyFile("/tmp/6g_policy",6.0f * ((float) (1<<30) ) );

}

/*
unsigned getDiagonal(const std::vector<unsigned>& lens, const float budget, unsigned left, unsigned right){
	if(right-left<2)
		return left;
	unsigned middle = (right+left)>>1;
	float midspace = run(lens,middle,middle);
	if(midspace>budget) //we have extra space, let's have more preprocessed:
		return getDiagonal(lens,budget,middle,right);
	return getDiagonal(lens,budget,left,middle);
}

unsigned getHor(const std::vector<unsigned>& lens, const float budget, unsigned left, unsigned right, const unsigned bmqc=(1<<18)){
	if(right-left<2)
		return left;
	unsigned middle = (right+left)>>1;
	float midspace = run(lens,middle,bmqc);
	if(midspace>budget) //we have extra space, let's have more preprocessed:
		return getHor(lens,budget,middle,right,bmqc);
	return getHor(lens,budget,left,middle,bmqc);
}


int runmain(int argc, char * argv[] ) {


	 std::vector<unsigned> lens;
	const float spaceBudget = atof(argv[1]);	
	const float theta = 0.01f;
	const unsigned maxd = MAXD;

	//find the starting point on diagonal

	unsigned place = getDiagonal(lens,spaceBudget,0,maxd);
	unsigned place2 = getHor(lens,spaceBudget,0,maxd);
	std::cout << place << " " << run(lens,place,place) << " "  << place2 <<","<<(1<<18)<< " " << run(lens,place2,1<<18)<< std::endl;
	return 0;
	//deploy a grid

	const unsigned sqr = 4000;


	for(unsigned s1 = 1; s1<maxd; s1+=maxd/sqr) {
		for(unsigned s2 = s1; s2<(1<<18) || (s1==s2 && s2<maxd); s2 +=maxd/sqr) {
			float space = run(lens,s1,s2) ;
			if(space > spaceBudget-theta && space < spaceBudget+theta)
				 std::cout << s1 << " " << s2 << " " << space << std::endl;
		}
	}
	return 0;
}
 */
