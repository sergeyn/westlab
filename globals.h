#ifndef GLOBALS_H_
#define GLOBALS_H_

#include <stdlib.h>
#include <string>
#include <iostream>
#include <sstream>
#include <ostream>
#include <vector>
#include <assert.h>
#include <stdint.h>

#include "types.h"

// ! moved all "uses" to NewIterator.h

//#define PROFILING
#ifdef GPROFILING
#define MYNOINLINE __attribute__((noinline))
#define STEPCOUNTER(a) //
#else
#define MYNOINLINE
#define STEPCOUNTER(a) profilerC::getInstance().stepCounter(a);
#endif

//#define SHOWLINESALWAYS
#ifdef SHOWLINESALWAYS
	#define COUT std::cout << __FILE__ << ":" << __LINE__ << " "
	#define CERR std::cerr << __FILE__ << ":" << __LINE__ << " "
#else
	#define COUT std::cout
	#define CERR std::cerr
#endif

	#define COUTL std::cout << __FILE__ << ":" << __LINE__ << " "

	#define COUT1 if(globalVerbosity>=1)COUT << " <1> "
	#define COUT2 if(globalVerbosity>=2)COUT << " <2> "
	#define COUT3 if(globalVerbosity>=3)COUT << " <3> "
	#define COUT4 if(globalVerbosity>=4)COUT << " <4> "
	#define COUTV(v) if(globalVerbosity>=v)COUT << " <" << v << "> "

#ifdef DEBUG
	#define COUTD COUTL
#else
	#define COUTD if(0) COUT
#endif
	#define LOG logFile
	#define ENDL std::endl
	#define HERE COUTL << ENDL;
	#define FATAL(msg) { CERR << __FILE__ << ":" << __LINE__ << " " << msg << ENDL; throw; }
	#define STRINGSTREAM std::stringstream
	#define SLASHN "\n"
	template <class B> STRING toString(const B& d) { STRINGSTREAM s; s << d; return s.str(); }

	//this is terrible and silly!
	bool isNumber(const STRING& str);

	extern unsigned globalVerbosity;
	extern std::ofstream logFile;



namespace CONSTS {
//deprecated!!! use enm::
enum counters { CNT_EVICTIONS,
	WAND,GETBLOCKSCORE,GETBLOCKSCORE1, BLOCKFAIL,ALIGN,MISALIGNED, GETFREQ, EVAL,HEAPIFY,HEAPIFYOK,NEXTGEQ,NEXTGEQ1,ALLQS,ONTHEFLYQUERY,ONTHEFLYLIST,DEQUANT,DEQUANTBM,
	SKIPS, SKIPSB, DECODE, CAND, MISTRUST, ESSENTIAL, SHALLOWPOINTER, SHALLOWPOINTER1, SORT, DOCIDFILTER1, DOCIDFILTER2,
	EARLYTERMINATION0, EARLYTERMINATION01, EARLYTERMINATION1, EARLYTERMINATION2, EARLYTERMINATION3, EARLYTERMINATION4, EARLYTERMINATION5,
	NONESSENTIAL,STEP1, STEP2, STEP3, NOP };

	const std::string QLogPath("../QueryLog/");
	const std::string fResultLog("../result_log");
	const std::string fQueryScore("../query_score");
	const std::string Golden_Threshold( CONSTS::QLogPath + "Golden_Threshold_trec06"); // Golden_Threshold_trec06 // Golden_Threshold_trec05
	const std::string Golden_AND_Threshold( CONSTS::QLogPath + "Golden_Threshold_AND_trec06");

	const std::string entireIndexListLengths( CONSTS::QLogPath + "Entire_index_list_lengths");
	const std::string Unpadded_10k_postings( CONSTS::QLogPath + "10k_pair_term_unpadded_postings");
	const std::string Entire_Index_Maxscore_Unpadded_List_length("../QueryLog/Entire_Index_Maxscore_Unpadded_List_length"); //../QueryLog/
	const std::string ikQueryScores(CONSTS::QLogPath + "1k_scores");
	const std::string IndexMetada( CONSTS::QLogPath + "indexMetadata.info");

	// thresholds predictions stored
	const std::string maxTopKThresholds( CONSTS::QLogPath + "maxTopKThresholds");
	const std::string MaxPairsThresholds( CONSTS::QLogPath + "PairsThresholds");
	const std::string topKScoresANDDidsThresholds( CONSTS::QLogPath + "topKScoresANDDidsThresholds");

	const std::string Starting_Threshold( CONSTS::QLogPath + "10th_Starting_Threshold");
	const std::string Top1_Threshold( CONSTS::QLogPath + "Top-1_Threshold");

	const std::string GOOD_TERM_SUFFIX("_good");
	const std::string BAD_TERM_SUFFIX("_bad");

	#define MONOLITHICINDEX
	namespace RAW_NDEX {
		const std::string zeroMapping( CONSTS::QLogPath + "10000q_terms.mapping"); //10000q_terms.mapping CHANGED !!
		const std::string termsMapping( CONSTS::QLogPath + "10000q_terms.mapping");
		// 10000q_terms.mapping // normal and reordered
		// 10000q_terms.mapping_layered // layered
		// 10000q_terms.mapping_layered_sorted // reordered + layered
		// version2: 10000q_terms_mapping_layered_version2 // layered (version2)
		// TREC05
		// trec05_mapping
		//const std::string termsMapping_Layer("../QueryLog/10000q_terms.mapping_layered");

		const std::string INFO_INDEX("8_1.inf");
		const std::string INDEX("8_1.dat");
		const std::string SORTED("_sorted");
		const std::string DOCUMENT_LENGTH("doclen_file");
		const std::string TERMMAPPING("word_file");
		const std::string MERGED_BOOL_PATH("merged_bool/");

		const std::string trecRawRoot("/data/sding/TwoLevelTrec/index_result/");
	}

	namespace NUMERIC {
		const unsigned QUANTIZATION(255);

		const unsigned ONDEMANDTERMS(30000); // 2194 (2187) -- the lowest value to have 0 evictions on our 1K queries sample
		extern unsigned CACHE_FRIENDLY_BITS;//(12);
		extern unsigned CACHE_FRIENDLY_CHUNK; //(1<<CACHE_FRIENDLY_BITS); //in bytes
		extern unsigned MAXD;
		const int MAXTERMS(32818983);
		const float MAX_SCORE(70.903f);
		const int MAXDBITS(25);
		const int TOPK(10);

		const unsigned maskFlagWithOnes(65535);
		const unsigned int BS(64);
		const double FRAC(0.10); //for pfor compression means 90% agreement
		const unsigned BLOCKAGREE(57);
		const float EPSILON(0.0001);
		const int SAMPLEERROR(10);
	
		const float GLOBAL_LIN_QUANTILE(MAX_SCORE/255.0f);
		const float GLOBAL_LIN_QUANTILE_R(255.0f/MAX_SCORE);

		const unchar FAKE_PBS_LIMIT(22);
	}

	// Layering
	const int LAYER_SPLIT_POSTINGS_THRESHOLD(4096); // 65536 : Version1 (BMW): 50000 // Version2: 4096
	const float LAYER_SCORE_PARAMETER(0.25); // Version1 (BMW): 0.02  // Version2: 0.25  //0.5f // 0.25f

};

inline bool FloatZero(float n) { return 0 == (int*)(((void*)(&n))); }
inline unchar quantF_(const float val, const float quantile=CONSTS::NUMERIC::GLOBAL_LIN_QUANTILE_R) { return FloatZero(val) ? 0: 1 + (val*quantile);}
inline float  dequantF_(const unchar val, const float quantile=CONSTS::NUMERIC::GLOBAL_LIN_QUANTILE) { return ((float)val)*quantile; }

#endif /* GLOBALS_H_ */
