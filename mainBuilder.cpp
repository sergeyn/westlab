#include <fstream>
#include "options.h"
#include "globals.h"
#include "utils.h"
#include "indexBuilder/IndexBuilder.h"
#include "Pfor.h"

Options parseCommandLineArgs(int argc, char * argv[]);

int mainBuilder(int argc, char * argv[] ){
	COUT << "build index" << ENDL;

	Options options1 =  parseCommandLineArgs(argc, argv);
	globalVerbosity = options1.VERBOSITY;

	Registry* registry = new Registry();
	registry->registerClass<OldPforDelta>();
	registry->registerClass<OldPforDeltaWithNewPacker>();

	CompressionInterface* cmprsr = registry->getInstanceOf<CompressionInterface>(options1.BLD_COMPRESSOR);

	indexFlatFile(options1.BLD_FLATFILE.c_str(),options1.BLD_MAXD,options1.BLD_DESTFOLDER.c_str(), options1.BLD_ISQUANT, cmprsr);
	return 0;


	//build index
	std::mutex mtx;
	//const std::string sourceBase = "/data/sding/TwoLevelTrec/index_result/";
	const std::string sourceBase = "/home/sergeyn/BMW/index_result/";
	lengthAwareFilter filter(sourceBase+"merged_bool/8_1.inf",0);
	bool calcScores = true;
	bool quantize = true;
	IndexBuilder::buildIndex(mtx,filter,"/home/sergeyn/BMW/GlobalLinearQuant1/",calcScores,quantize,sourceBase, "/tmp/1.f"/*sourceBase+"word_file"*/);
	//IndexBuilder::mergeTwoNewStyleIndexDirs("/data3/team/smallIndexEven/","/data3/team/smallIndexOdd/","/data3/team/testMerge/");

	return 0;
}
