#ifndef SSE_H_
#define SSE_H_

#include "globals.h"
#include "types.h"
#include <tmmintrin.h>

//lookups for converting nibbles to byte-masks for shuffle
const unsigned long long m__x(0xffffffffffffffff); //64 ones
#define AND_SHIFT_MASK(n) ((unsigned long long)(0xffffffffffffffff) & (unsigned long long)(n))<<32
const unsigned long long byteMaskFromNibbleForDuplicating_00[16*2] = {
		m__x&0xffffffff, m__x&0xffffff00,m__x&0xffff00ff,m__x&0xffff0000,
		m__x&0xff00ffff, m__x&0xff00ff00,m__x&0xff0000ff,m__x&0xff000000,
		m__x&0xffffff, m__x&0xffff00, m__x&0xff00ff, m__x&0xff0000,
		m__x&0xffff, m__x&0xff00, m__x&0xff, m__x&0,

		AND_SHIFT_MASK(0xffffffff), AND_SHIFT_MASK(0xffffff00), AND_SHIFT_MASK(0xffff00ff), AND_SHIFT_MASK(0xffff0000),
		AND_SHIFT_MASK(0xff00ffff), AND_SHIFT_MASK(0xff00ff00), AND_SHIFT_MASK(0xff0000ff), AND_SHIFT_MASK(0xff000000),
		AND_SHIFT_MASK(0xffffff), AND_SHIFT_MASK(0xffff00), AND_SHIFT_MASK(0xff00ff), AND_SHIFT_MASK(0xff0000),
		AND_SHIFT_MASK(0xffff), AND_SHIFT_MASK(0xff00), AND_SHIFT_MASK(0xff), AND_SHIFT_MASK(0)
}	;

const unsigned long long byteMaskFromNibbleForDuplicating_01[16*2] = {
		m__x&0xffffffff, m__x&0xffffff01, m__x&0xffff01ff, m__x&0xffff0101,
		m__x&0xff01ffff, m__x&0xff01ff01, m__x&0xff0101ff, m__x&0xff010101,
		m__x&0x01ffffff, m__x&0x01ffff01, m__x&0x01ff01ff, m__x&0x01ff0101,
		m__x&0x0101ffff, m__x&0x0101ff01, m__x&0x010101ff, m__x&0x01010101,

		AND_SHIFT_MASK(0xffffffff), AND_SHIFT_MASK(0xffffff01), AND_SHIFT_MASK(0xffff01ff), AND_SHIFT_MASK(0xffff0101),
		AND_SHIFT_MASK(0xff01ffff), AND_SHIFT_MASK(0xff01ff01), AND_SHIFT_MASK(0xff0101ff), AND_SHIFT_MASK(0xff010101),
		AND_SHIFT_MASK(0x01ffffff), AND_SHIFT_MASK(0x01ffff01), AND_SHIFT_MASK(0x01ff01ff), AND_SHIFT_MASK(0x01ff0101),
		AND_SHIFT_MASK(0x0101ffff), AND_SHIFT_MASK(0x0101ff01), AND_SHIFT_MASK(0x010101ff), AND_SHIFT_MASK(0x01010101)
}	;

inline unsigned offsetInAlignedForSSE(void* ptr) { //bytes
	unsigned long long tmp = (unsigned long long)ptr;
	return (tmp%16);
}

inline bool isAlignedForSSE(void* ptr) {
	return offsetInAlignedForSSE(ptr)==0;
}

//helper function that constructs from 16 bits the 16-byte mask for shuffle
inline __m128i aux__initByteMasksFromNibbles(unsigned short bits16) {
	//we construct two 64 bit masks from 2 lower and 2 upper nibbles and unite them to 128 bit
	return _mm_set_epi64x(
			byteMaskFromNibbleForDuplicating_01[(bits16>>8)&15] //10
			                                    |byteMaskFromNibbleForDuplicating_01[16+((bits16>>12)&15)], //11

			                                    byteMaskFromNibbleForDuplicating_00[bits16&15] //00
			                                                                        |byteMaskFromNibbleForDuplicating_00[16+((bits16>>4)&15)] //01

	);
}

//byte_result[i]=val if bit_maskBits[i]=1
inline __m128i duplicateTwoValsAndMaskWith16bits(unsigned char valLow, unsigned char valHigh, unsigned short maskBits) {
	return _mm_shuffle_epi8(
			_mm_set_epi8(0,0,0,0,0,0,0,0,0,0,0,0,0,0,valHigh,valLow), //set the value
			aux__initByteMasksFromNibbles(maskBits) //get masks as bytes
	);

}
//=============================================================================================
inline unchar cvtEpi8ToEpi16Mask(int smask) {
	unchar mask = (smask>>13)&3;
	mask <<= 2;
	mask |= (smask>>9)&3;
	mask <<= 2;
	mask |= (smask>>5)&3;
	mask <<= 2;
	mask |= (smask>>1)&3;
	return mask;
}

void buildLiveAreaQuantSSE_(unchar* ptr, unchar** blockScores, unchar** bitSetsPBS, const unchar threshold, const unsigned termsCount, const unsigned windowsize);
void buildPBS_M3_SSE_(unsigned* dest, const unsigned* dids, const unsigned winStartDid, unsigned bits, const unsigned sz);


inline __m128 calcFourScoreAddOne(__m128 freq, __m128 len, float BM25Element) {
	__m128 v1 = {BM25Element,BM25Element,BM25Element,BM25Element};
	__m128 v2 = {2,2,2,2};
	__m128 v3 = {0.25,0.25,0.25,0.25};
	__m128 v4 = {0.75,0.75,0.75,0.75};
	__m128 v6 = {1.0/130,1.0/130,1.0/130,1.0/130};
	__m128 ones = {1.0,1.0,1.0,1.0};
	freq = _mm_add_ps(freq,ones);
	__m128 result =
	_mm_div_ps(_mm_mul_ps(v1,freq),
			   _mm_add_ps(_mm_mul_ps(_mm_add_ps(_mm_mul_ps(_mm_mul_ps(len,v6),v4),v3),v2),freq));
	return result;
}

inline __m128 calcScore4(__m128 freq, __m128 len, float bM25) {
	__m128 v1 = {bM25,bM25,bM25,bM25};
	__m128 v2 = {2,2,2,2};
	__m128 v3 = {0.25,0.25,0.25,0.25};
	__m128 v4 = {0.75,0.75,0.75,0.75};
	__m128 v6 = {1.0/130,1.0/130,1.0/130,1.0/130};
	// __m128 ones = {1.0,1.0,1.0,1.0}; // freq = _mm_add_ps(freq,ones);
	__m128 result =
			_mm_div_ps(_mm_mul_ps(v1,freq),
					_mm_add_ps(_mm_mul_ps(_mm_add_ps(_mm_mul_ps(_mm_mul_ps(len,v6),v4),v3),v2),freq));
	return result;
}

/*
inline void calcScoresSSE_(const did_t* dids, const unsigned *freqs, score_t* scores, const unsigned size, float bM25, const did_t* documentLengthArr) {
	unsigned lengths[CONSTS::NUMERIC::BS];
	for(unsigned i=0; i<CONSTS::NUMERIC::BS; ++i) {
		lengths[i]=documentLengthArr[dids[i]]; //build lengths array
	}
	for(unsigned i=0; i<CONSTS::NUMERIC::BS; i+=4)  {
		__m128 freq = _mm_cvtepi32_ps(_mm_load_si128((__m128i*)(freqs+i)));
		__m128 len = _mm_cvtepi32_ps(_mm_load_si128((__m128i*)(lengths+i)));
		__m128 res = calcScore4(freq,len,bM25); //sse version -- knows NOT to add +1 to freqs
		_mm_store_ps(scores+i,res);
	}
}
*/

inline void calcScoresForBlockSSE(const unsigned* freqs, const unsigned* lengths, float BM25Element, float* scores ) {
	for(unsigned i=0; i<CONSTS::NUMERIC::BS; i+=4) 	{
			__m128 freq = _mm_cvtepi32_ps(_mm_load_si128((__m128i*)(freqs+i)));
			__m128 len = _mm_cvtepi32_ps(_mm_load_si128((__m128i*)(lengths+i)));
			__m128 res = calcFourScoreAddOne(freq,len,BM25Element); //sse version -- knows to add +1 to freqs
			_mm_store_ps(scores+i,res);
	}
}

void unitTestBytesAndBits();
void unitTestDuplicateTwoValsAndMask();

#endif /* SSE_H_ */
