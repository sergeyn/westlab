#include "Pfor.h"
#include <cstring>
#include "PackNew.h"
#include "PackOld.h"

template<int selector>
int pack_encode(unsigned int **w, const unsigned int *p, unsigned bitsSelectorFromArr);
void pack(const unsigned*src, const unsigned bits, const unsigned size, unsigned int *dest);

//-------------------------------------------------------------------
template<int select>
inline void packSelect(const unsigned*src, const unsigned bits, const unsigned size, unsigned int *dest) {
	//for(unsigned i=0; i<size; ++i)COUT << src[i] << " " ; COUTL<<ENDL;
	if(select == 1 ) pack(src,bits,size,dest);
	else if(select == 2) newPackHub(src,bits,size,dest);
	//for(unsigned i=0; i<32; ++i)COUT << (unsigned int) ( ((unsigned char*)dest)[i]) << " " ; COUTL<<ENDL;
}
//-------------------------------------------------------------------
template<int select>
inline void unpackSelect(unsigned int *dstDecomressedPtr, const unsigned int *srcCompressedPtr, int flag) {
	if(select == 1 ) unpackHub(dstDecomressedPtr,srcCompressedPtr,flag);
	else if(select == 2) newUnpackHub(dstDecomressedPtr,srcCompressedPtr,flag);
}
//-------------------------------------------------------------------
//              				0,1,2,3,4,5,6,7, 8, 9,10,11,12
int bitNumsForPforEncode[13] = {2,3,4,5,6,7,8,9,10,12,16,20,32};
//-------------------------------------------------------------------
namespace pfor {
const unsigned flagFor8Bits(0);
const unsigned flagFor16Bits(1);
const unsigned flagFor32Bits(2);
}
//-------------------------------------------------------------------
template<int selector>
inline unsigned oldPforDecopmress(const unsigned* srcCompressedPtr, unsigned* dstDecopmressedPtr, int flag, unsigned compressedSizeInBytes) {
	unsigned i, offsetInDst;
	unsigned skipToNextExc;
	unsigned bits = bitNumsForPforEncode[(flag>>12) & 15];
	unsigned mode = (flag>>10) & 3;
	unsigned excOffset = flag & 1023;
	const unsigned* const startingPosition = srcCompressedPtr;

	unpackSelect<selector>(dstDecopmressedPtr, srcCompressedPtr,flag); //unpack the non-exceptions array 64*bits (the flag hides the bits)
	srcCompressedPtr += ((bits * CONSTS::NUMERIC::BS)/(8*sizeof(unsigned int))); //how many ints this chunk is. we shall add the exceptions later

	//now walk the list of exceptions and perform replacements
	switch (mode) 	{
		case(pfor::flagFor8Bits): //---------------------------------------------------------------------
			for (offsetInDst = excOffset, i = 0; offsetInDst < CONSTS::NUMERIC::BS; i++)      { //unpack exceptions when they are 8 bits
			  skipToNextExc = dstDecopmressedPtr[offsetInDst]+1;
			  dstDecopmressedPtr[offsetInDst] = (srcCompressedPtr[i>>2]>>(24-((i&3)<<3)))&255;
			  offsetInDst += skipToNextExc;
			}
		srcCompressedPtr += (i>>2);
		if ((i&3) > 0)  ++srcCompressedPtr;
	break;
	case(pfor::flagFor16Bits): //---------------------------------------------------------------------
		for (offsetInDst = excOffset, i = 0; offsetInDst < CONSTS::NUMERIC::BS; i++)      { //unpack exceptions when they are 16 bits
		  skipToNextExc = dstDecopmressedPtr[offsetInDst]+1;
		  dstDecopmressedPtr[offsetInDst] = (srcCompressedPtr[i>>1]>>(16-((i&1)<<4)))&65535;
		  offsetInDst += skipToNextExc;
		}
		srcCompressedPtr += (i>>1);
		if ((i&1) > 0)  ++srcCompressedPtr;
	break;
		case(pfor::flagFor32Bits): //---------------------------------------------------------------------
			for (offsetInDst = excOffset, i=0; offsetInDst < CONSTS::NUMERIC::BS; i++)      { //unpack exceptions when they are 32 bits
			  skipToNextExc = dstDecopmressedPtr[offsetInDst]+1;
			  dstDecopmressedPtr[offsetInDst] = srcCompressedPtr[i];
			  offsetInDst += skipToNextExc;
			}
		srcCompressedPtr += i;
		break;
	default: throw WrongFlag(); break;
	}
	return srcCompressedPtr-startingPosition;
}
//-------------------------------------------------------------------
unsigned OldPforDeltaWithOldPacker::decompressToInts(const unsigned* srcCompressedPtr, unsigned* dstDecopmressedPtr, int flag, unsigned compressedSizeInBytes){
	return oldPforDecopmress<1>(srcCompressedPtr,dstDecopmressedPtr,flag,compressedSizeInBytes);
}
//-------------------------------------------------------------------
std::pair<unsigned,unsigned> OldPforDeltaWithOldPacker::compressInts(const unsigned* src, unsigned* dest, unsigned count){
	assert(count == CONSTS::NUMERIC::BS);
	int flag = -1;
	const unsigned *destStart = dest;
	for (unsigned j = 0; flag < 0; j++)	{
			flag = pack_encode<1>(&dest,src, j);
	}
	return std::pair<unsigned,unsigned>(flag,dest-destStart);
}
//-------------------------------------------------------------------
unsigned OldPforDeltaWithNewPacker::decompressToInts(const unsigned* srcCompressedPtr, unsigned* dstDecopmressedPtr, int flag, unsigned compressedSizeInBytes){
	return oldPforDecopmress<2>(srcCompressedPtr,dstDecopmressedPtr,flag,compressedSizeInBytes);
}
//-------------------------------------------------------------------
std::pair<unsigned,unsigned> OldPforDeltaWithNewPacker::compressInts(const unsigned* src, unsigned* dest, unsigned count){
	assert(count == CONSTS::NUMERIC::BS);
	int flag = -1;
	const unsigned *destStart = dest;
	for (unsigned j = 0; flag < 0; j++)	{
			flag = pack_encode<2>(&dest,src, j);
	}
	return std::pair<unsigned,unsigned>(flag,dest-destStart);
}
//-------------------------------------------------------------------
inline int getFlagFromParams(unsigned bitsSelector, unsigned excBitsFlag, unsigned start) { return ((bitsSelector<<12) + (excBitsFlag<<10) + start); }
//-------------------------------------------------------------------
template<int selector>
int pack_encode(unsigned int **dest, const unsigned int *src, unsigned bitsSelectorFromArr){
	int l, exCnt, excBits, excBitsFlag;
	unsigned s;
	unsigned i;
	unsigned int m;
	int bits = bitNumsForPforEncode[bitsSelectorFromArr];
	int start;
	unsigned int numsToPack[CONSTS::NUMERIC::BS]; //temp buffer for numbers
	unsigned int  exceptions[CONSTS::NUMERIC::BS]; //temp buffer for exceptions

	if (bits == 32)	{ //the stupid case of no compression at all
		for (i = 0; i < CONSTS::NUMERIC::BS; i++)  (*dest)[i] = src[i];
		*dest += CONSTS::NUMERIC::BS;
		return getFlagFromParams(bitsSelectorFromArr,2,CONSTS::NUMERIC::BS);
	}

	//find largest number in block
	for (m = 0, i = 0; i < CONSTS::NUMERIC::BS; i++)  if (src[i] > m)  m = src[i];

	//see how many bits we need for it
	if (m < 256)   			{ excBits = 8;  excBitsFlag = pfor::flagFor8Bits; }
	else if (m < 65536)     { excBits = 16; excBitsFlag = pfor::flagFor16Bits; }
	else     				{ excBits = 32; excBitsFlag = pfor::flagFor32Bits; 	}

	for (start = 0, exCnt = 0, l = -1, i = 0; i < CONSTS::NUMERIC::BS; i++) {
		if ((src[i] >= (1<<bits)) || ((l >= 0) && (i-l == (1<<bits))))    { //if exception
			if (l < 0)
				start = i;
			else
				numsToPack[l] = i - l - 1;

			exceptions[exCnt++] = src[i];
			l = i;
		}
		else //not exception
			numsToPack[i] = src[i];
	}

	if (l >= 0)
		numsToPack[l] = (1<<bits)-1;
	else
		start = CONSTS::NUMERIC::BS;

	//if there are no more than FRAC(10%) exceptions:
	if ((double)(exCnt) <= CONSTS::NUMERIC::FRAC * (double)(CONSTS::NUMERIC::BS)) 	{
		s = ((bits * CONSTS::NUMERIC::BS)>>5);
		for (i = 0; i < s; i++)  (*dest)[i] = 0;
		//pack output
		//COUT << " outputB: " << bits << "\n ";
		packSelect<selector>(numsToPack, bits, CONSTS::NUMERIC::BS, *dest);
		*dest += s;

		s = ((excBits * exCnt)>>5) + ((((excBits * exCnt) & 31) > 0)? 1:0);
		for (i = 0; i < s; i++)  (*dest)[i] = 0;
		packSelect<1>(exceptions, excBits, exCnt, *dest); //pack exceptions -- we are using old packer for exceptions!
		//COUT << " Ex: " << excBits << ENDL;
		*dest += s;

		return getFlagFromParams(bitsSelectorFromArr,excBitsFlag,start); //return the correct flag
	}
	else //too much exceptions:
		return(-1); //fail
}

/*
template <typename T>
unsigned char sizesInBitsHistogram(const T* chunk, unsigned char* histogramDest, const unsigned size) {
	//assume histogram is zeroed...
	unsigned i;

	//note! if T is long (64 bits) you must use clzl, if smaller, use clz!
	for(i=0; i<size; ++i)
			histogramDest[32-__builtin_clz(chunk[i])] += 1;
	//now prefix sum the histogram:
	unsigned sum = 0;
	for(i=0; i<32 && sum<CONSTS::NUMERIC::BLOCKAGREE; ++i) //BLOCKAGREE = BS*0.9
		sum += histogramDest[i];
	return i-1; // i-1 bits are enough to represent 90% of the values in this block
}
*/
