#ifndef INDEXHANDLER_H_
#define INDEXHANDLER_H_

#include "globals.h"
#include "Interfaces.h"
#include "OnDemandResourceManager.h"
#include "ListMetaData.h"
//=============================================================================================
class BringRawFromFile : public BringCompressedDataInterface<vecUInt> {
	std::string filename;
	LexiconInterface* lexicon;

	public:
	 //implements bringing interface -- will read raw data from disk to memory
	explicit BringRawFromFile(const std::string& path="") : filename(path), lexicon(nullptr) {}
	virtual void setPath(const std::string& path) {filename=path; }
	virtual void bring(const std::string& term, std::vector<unsigned>& dest);
	virtual void setLexicon(LexiconInterface* lex) {lexicon=lex;}
};
//=============================================================================================
class BringRawFromManyFiles : public BringCompressedDataInterface<vecUInt> {
	std::string pathToPool;

	public:
	 //implements bringing interface -- will read raw data from disk to memory
	explicit BringRawFromManyFiles(const std::string& path="") : pathToPool(path) {}
	virtual void setPath(const std::string& path) {pathToPool=path; }
	virtual void bring(const std::string& term, std::vector<unsigned>& dest);
	virtual void setLexicon(LexiconInterface*) {} //don't-care
};
//=============================================================================================
class SqliteBasedLexicon : public LexiconInterface, public RegistryEntry {
	hashMapStd<std::string,ListMetaData> termMetaInfo;
	std::string sqlFilename;
	OnDemandManager<std::string,vecUInt> *onDemandManager;
	BringRawFromFile bringer;

public:
	explicit SqliteBasedLexicon() : sqlFilename(""), onDemandManager(nullptr){}

	std::vector<std::string>  loadMeta();
	virtual void setPath(const std::string& path) {
		bringer.setPath(path);
		bringer.setLexicon(this);
		onDemandManager = new OnDemandManager<std::string,vecUInt>(&bringer,CONSTS::NUMERIC::ONDEMANDTERMS);
		sqlFilename = path+"/indx.sqlite";
		bringer.setPath(path+"/allCompressedLists");
		onDemandManager = new OnDemandManager<std::string,vecUInt>(&bringer,CONSTS::NUMERIC::ONDEMANDTERMS);
	}

	virtual const std::string name() const { return "SqliteBasedLexicon"; }
	virtual void* getOne() const { return new SqliteBasedLexicon; }

	~SqliteBasedLexicon(){ delete onDemandManager;}

	virtual void loadLexicon(const std::vector<std::string>* terms=nullptr, bool calcAugments=true);

	virtual tid_t getTid(const std::string& term) const { return termMetaInfo.count(term) ?  termMetaInfo.at(term).getTid() : 0; }
	virtual did_t getListLength(const std::string& term) const { return termMetaInfo.count(term) ?  termMetaInfo.at(term).getUnpaddedLength() : 0; }
	virtual long int getLocation(const std::string& term) { return termMetaInfo.count(term) ?  termMetaInfo[term].getOffsetInFile() : 0; }
	virtual did_t getRawSize(const std::string& term) { return termMetaInfo.count(term) ? termMetaInfo[term].getCompressedDataLengthBytes() : 0; }

	//virtual const listIndexMetaData* getListHandle(const std::string& term){return termMetaInfo.count(term) ?  &termMetaInfo[term] : nullptr; }
	virtual const void* getConstListHandleUnsafe(const std::string& term);
};
//=============================================================================================
class OldStyleSqliteBasedLexicon : public LexiconInterface, public RegistryEntry {
	hashMapStd<std::string,ListMetaData> termMetaInfo;
	std::string sqlFilename;
	std::string termLengthFileName;
	OnDemandManager<std::string,vecUInt> *onDemandManager;
	BringRawFromManyFiles bringer;

public:
	OldStyleSqliteBasedLexicon()
			: sqlFilename(""), termLengthFileName(""), onDemandManager(nullptr){}
	virtual void setPath(const std::string& path) {
		sqlFilename = path+"/indx.sqlite";
		termLengthFileName = path+"/basic_table";
		bringer.setPath(path+"/pool/");
		onDemandManager = new OnDemandManager<std::string,vecUInt>(&bringer,CONSTS::NUMERIC::ONDEMANDTERMS);
	}
	std::vector<std::string>  loadMeta(const std::string& sqlpath, const std::string& where="");

	virtual const std::string name() const { return "OldStyleSqliteBasedLexicon"; }
	virtual void* getOne() const { return new OldStyleSqliteBasedLexicon; }

	~OldStyleSqliteBasedLexicon(){delete onDemandManager;}
	virtual void loadLexicon(const std::vector<std::string>* terms=nullptr, bool calcAugments=true);
	virtual tid_t getTid(const std::string& term)  const { return termMetaInfo.count(term) ?  termMetaInfo.at(term).getTid() : 0; }
	virtual did_t getListLength(const std::string& term) const {return termMetaInfo.count(term) ?  termMetaInfo.at(term).getUnpaddedLength() : 0; }
	virtual long int getLocation(const std::string& term) { return termMetaInfo.count(term) ?  termMetaInfo[term].getOffsetInFile() : 0; }
	virtual did_t getRawSize(const std::string& term) { return termMetaInfo.count(term) ? termMetaInfo[term].getCompressedDataLengthBytes() : 0; }

	virtual const void* getConstListHandleUnsafe(const std::string& term){return termMetaInfo.count(term) ?  &termMetaInfo[term] : nullptr; }
};
//=============================================================================================
class indexHandler{
public:
	LexiconInterface* lexicon;
	vecUInt documentLength;

	//std::string dataPath;
	std::string doclensPath;
	//std::string sqlitePath;

	did_t maxD;
	void loadDocLengths();

public:
	indexHandler(const std::string& doclens, const unsigned maxd, LexiconInterface* lex);

	std::vector<const ListMetaData*> operator[] (const std::vector<std::string>& terms); //will materialize the terms
	unsigned* getDocLensPtr() { return documentLength.data(); } //don't like exposing this one here...
	bool isLoaded(const std::string& term) { return lexicon->getListLength(term); }
};

#endif /* INDEXHANDLER_H_ */
