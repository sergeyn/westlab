#ifndef SHIFTLIST_H_
#define SHIFTLIST_H_

/*
 * Define:
 * Deciding to work with 2^B posting chunks
 * Assuming ideally uniform distributed 2^P postings in docid space of size 2^25
 * We want to create 2^(P-B) did ranges and populate them with postings
 * The *expected* amount of postings in a bucket is 2^B
 * The range of every bucket is 2^(25-(P-B))
 *
 * Examples:
 * For B=3 we want to have 8 postings in a did bucket
 *
 * Given a List with 256k postings => P=18
 * We are going to have 2^15=32k buckets with 8 postings each
 * The range of every bucket is going to be 2^(25-15)=1024
 * Uncompressed needed 256k*4bytes = 1024kbytes
 * Compressed (given no overflows happened): 32k*8*2bytes = 512kbytes
 *
 * Given a List with 64k postings => P=16
 * We are going to have 2^13=8192 buckets with 8 postings each
 * The range of every bucket is going to be 2^(25-13)=4096
 * Uncompressed needed 64k*4bytes = 256kbytes
 * Compressed (given no overflows happened): 8192*8*2bytes = 128kbytes
 *
 * Given a List with 4k postings => P=12
 * We are going to have 2^9=512 buckets with 8 postings each
 * The range of every bucket is going to be 2^(25-9)=64k
 * Uncompressed needed 4k*4bytes = 16kbytes
 * Compressed (given no overflows happened): 512*8*2bytes = 8kbytes
 *
 * Note: we have natural bounds here:
 * 1) for more than 2m postings we should just use bitvectors
 * 2) for less than 4k postings we can't use the scheme -- the range of a block will not allow us to operate with uint_16
 *
 *
 ** DEPRECATED SECTION
 * The trouble is that we are as always bound to powers of 2.
 * If we have a list of 2^k+C postings (and also they are far from being distributed ideally in did space) we are going to have many overflows for large Cs
 * So the nice compression ratio of 2X is not achievable in practice.
 *
 * If instead of shifts we use ordinary integer division we don't need the next part:

 * What we could do for an arbitrary list of  4k<=size<=2m postings is to realize it
 * as a forest of at most O(logn) hashes that cover different parts of did space, sort of like binomial heaps work.
 * We take the largest power of two P : P<|list|
 * and consider the first 2^P postings in the list -- they are covering a did range (0, list[P]) -- so we build the hash for this
 * Then we apply the same idea to the remaining postings recursively.
 * Upon lookup you decide in which hash you need to look and perform the shifting and masking according the config of that one. From practical POV you are still O(1)
 */

#include "globals.h"

class ShiftList {
public:
	typedef uint16_t data_t;
	typedef std::vector<data_t> data_vec;
	static unsigned MAXDID;
	static const unchar LOGBUCKETSIZE = 4;
	static const unchar BUCKETSIZE = 1<<LOGBUCKETSIZE;

	explicit ShiftList(unsigned length=0);
	explicit ShiftList(const std::vector<unsigned>& src);

	inline unsigned getBucketsCount() const { return postings.size()>>LOGBUCKETSIZE; } //not counting the overflows
	inline unsigned getBucketOffset(unsigned did) const { return (did/getRange())<<LOGBUCKETSIZE; }
	inline unsigned getRange() const { return MAXDID/getBucketsCount(); }

	//template<int bits>
	inline void getBucketForDid(unsigned did, ShiftList::data_t* arr) const {
		unsigned i=getBucketOffset(did);
		unsigned const start = i*getRange(); //can and will AVX later
		arr[0] = start+postings[i++];
		arr[1] = start+postings[i++];
		arr[2] = start+postings[i++];
		arr[3] = start+postings[i++];
		arr[4] = start+postings[i++];
		arr[5] = start+postings[i++];
		arr[6] = start+postings[i++];
		arr[7] = start+postings[i];
		//static_assert(LOGBUCKETSIZE==3,"wrong num of elements returned from getB");
	}

	inline bool has(unsigned did) const {
		unsigned i=getBucketOffset(did);
		unsigned const cmp = did-(i>>ShiftList::LOGBUCKETSIZE)*getRange();
		bool ret = false; //can and will AVX later
		//COUTL << (i>>ShiftList::LOGBUCKETSIZE)*getRange() << " start for bucketOffset: " <<(i>>ShiftList::LOGBUCKETSIZE) << " " ; for(unsigned j=0; j<8; ++j) COUT << postings[i+j] << " "; COUT <<ENDL;
		ret |= (postings[i++] == cmp); ret |= (postings[i++] == cmp);
		ret |= (postings[i++] == cmp); ret |= (postings[i++] == cmp);
		ret |= (postings[i++] == cmp); ret |= (postings[i++] == cmp);
		ret |= (postings[i++] == cmp); ret |= (postings[i++] == cmp);
		return ret;
	}

private:
	data_vec postings;
};

void _unit_test_ShiftList(unsigned length, bool isBenchmark=false);
#endif /* SHIFTLIST_H_ */
