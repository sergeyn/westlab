#ifndef _PACKNEW_H_
#define _PACKNEW_H_

#include "globals.h"

template <int Bits>
struct PackedBits {
	unsigned d00 : Bits; unsigned d01 : Bits; unsigned d02 : Bits; unsigned d03 : Bits;
	unsigned d04 : Bits; unsigned d05 : Bits; unsigned d06 : Bits; unsigned d07 : Bits;
	unsigned d08 : Bits; unsigned d09 : Bits; unsigned d10 : Bits; unsigned d11 : Bits;
	unsigned d12 : Bits; unsigned d13 : Bits; unsigned d14 : Bits; unsigned d15 : Bits;
	unsigned d16 : Bits; unsigned d17 : Bits; unsigned d18 : Bits; unsigned d19 : Bits;
	unsigned d20 : Bits; unsigned d21 : Bits; unsigned d22 : Bits; unsigned d23 : Bits;
	unsigned d24 : Bits; unsigned d25 : Bits; unsigned d26 : Bits; unsigned d27 : Bits;
	unsigned d28 : Bits; unsigned d29 : Bits; unsigned d30 : Bits; unsigned d31 : Bits;

	static inline void unpack(const unsigned* source, unsigned* dest) {
		assert(4*Bits == sizeof(PackedBits<Bits>));
		const PackedBits<Bits>* d = reinterpret_cast<const PackedBits<Bits>*>(source);
		dest[0]=d->d00; dest[1]=d->d01; dest[2]=d->d02; dest[3]=d->d03; dest[4]=d->d04; dest[5]=d->d05; dest[6]=d->d06; dest[7]=d->d07;
		dest[8]=d->d08; dest[9]=d->d09; dest[10]=d->d10; dest[11]=d->d11; dest[12]=d->d12; dest[13]=d->d13; dest[14]=d->d14; dest[15]=d->d15;
		dest[16]=d->d16; dest[17]=d->d17; dest[18]=d->d18; dest[19]=d->d19; dest[20]=d->d20; dest[21]=d->d21; dest[22]=d->d22; dest[23]=d->d23;
		dest[24]=d->d24; dest[25]=d->d25; dest[26]=d->d26; dest[27]=d->d27; dest[28]=d->d28; dest[29]=d->d29; dest[30]=d->d30; dest[31]=d->d31;
	}

	static inline void pack(const unsigned* source, unsigned* dest) {
		assert(4*Bits == sizeof(PackedBits<Bits>));
		PackedBits<Bits> d;
		d.d00 = source[0]; d.d01 = source[1]; d.d02 = source[2]; d.d03 = source[3]; d.d04 = source[4]; d.d05 = source[5]; d.d06 = source[6]; d.d07 = source[7];
		d.d08 = source[8]; d.d09 = source[9]; d.d10 = source[10]; d.d11 = source[11]; d.d12 = source[12]; d.d13 = source[13]; d.d14 = source[14]; d.d15 = source[15];
		d.d16 = source[16]; d.d17 = source[17]; d.d18 = source[18]; d.d19 = source[19]; d.d20 = source[20]; d.d21 = source[21]; d.d22 = source[22]; d.d23 = source[23];
		d.d24 = source[24]; d.d25 = source[25]; d.d26 = source[26]; d.d27 = source[27]; d.d28 = source[28]; d.d29 = source[29]; d.d30 = source[30]; d.d31 = source[31];
		memcpy(dest,&d,sizeof(d));
	}

}__attribute__ ((__packed__));


//-------------------------------------------------------------------
inline void newUnpackHub(unsigned int *dstDecomressedPtr, const unsigned int *srcCompressedPtr, int flag) {
	switch((flag>>12) & 15) {
	case  0: PackedBits<2>::unpack(srcCompressedPtr,dstDecomressedPtr); PackedBits<2>::unpack(srcCompressedPtr+2,dstDecomressedPtr+32); break;
	case  1: PackedBits<3>::unpack(srcCompressedPtr,dstDecomressedPtr); PackedBits<3>::unpack(srcCompressedPtr+3,dstDecomressedPtr+32); break;
	case  2: PackedBits<4>::unpack(srcCompressedPtr,dstDecomressedPtr); PackedBits<4>::unpack(srcCompressedPtr+4,dstDecomressedPtr+32); break;
	case  3: PackedBits<5>::unpack(srcCompressedPtr,dstDecomressedPtr); PackedBits<5>::unpack(srcCompressedPtr+5,dstDecomressedPtr+32); break;
	case  4: PackedBits<6>::unpack(srcCompressedPtr,dstDecomressedPtr); PackedBits<6>::unpack(srcCompressedPtr+6,dstDecomressedPtr+32); break;
	case  5: PackedBits<7>::unpack(srcCompressedPtr,dstDecomressedPtr); PackedBits<7>::unpack(srcCompressedPtr+7,dstDecomressedPtr+32); break;
	case  6: PackedBits<8>::unpack(srcCompressedPtr,dstDecomressedPtr); PackedBits<8>::unpack(srcCompressedPtr+8,dstDecomressedPtr+32); break;
	case  7: PackedBits<9>::unpack(srcCompressedPtr,dstDecomressedPtr); PackedBits<9>::unpack(srcCompressedPtr+9,dstDecomressedPtr+32); break;
	case  8: PackedBits<10>::unpack(srcCompressedPtr,dstDecomressedPtr); PackedBits<10>::unpack(srcCompressedPtr+10,dstDecomressedPtr+32); break;
	case  9: PackedBits<12>::unpack(srcCompressedPtr,dstDecomressedPtr); PackedBits<12>::unpack(srcCompressedPtr+12,dstDecomressedPtr+32); break;
	case 10: PackedBits<16>::unpack(srcCompressedPtr,dstDecomressedPtr); PackedBits<16>::unpack(srcCompressedPtr+16,dstDecomressedPtr+32); break;
	case 11: PackedBits<20>::unpack(srcCompressedPtr,dstDecomressedPtr); PackedBits<20>::unpack(srcCompressedPtr+20,dstDecomressedPtr+32); break;
	case 12: PackedBits<32>::unpack(srcCompressedPtr,dstDecomressedPtr); PackedBits<32>::unpack(srcCompressedPtr+32,dstDecomressedPtr+32); break;
	default: CERR << ("Wrong flag "+toString(flag)) << ENDL; break;
	}
}
//-------------------------------------------------------------------
inline void newPackHub(const unsigned*src, const unsigned bits, const unsigned size, unsigned int *dest) {
	switch(bits) { //note: this is bits, not index of bits array
	case 2: PackedBits<2>::pack(src,dest); PackedBits<2>::pack(src+32,dest+2); break;
	case 3: PackedBits<3>::pack(src,dest); PackedBits<3>::pack(src+32,dest+3);break;
	case 4: PackedBits<4>::pack(src,dest); PackedBits<4>::pack(src+32,dest+4);break;
	case 5: PackedBits<5>::pack(src,dest); PackedBits<5>::pack(src+32,dest+5);break;
	case 6: PackedBits<6>::pack(src,dest); PackedBits<6>::pack(src+32,dest+6);break;
	case 7: PackedBits<7>::pack(src,dest); PackedBits<7>::pack(src+32,dest+7);break;
	case 8: PackedBits<8>::pack(src,dest); PackedBits<8>::pack(src+32,dest+8);break;
	case 9: PackedBits<9>::pack(src,dest); PackedBits<9>::pack(src+32,dest+9);break;
	case 10: PackedBits<10>::pack(src,dest); PackedBits<10>::pack(src+32,dest+10);break;
	case 12: PackedBits<12>::pack(src,dest); PackedBits<12>::pack(src+32,dest+12); break;
	case 16: PackedBits<16>::pack(src,dest); PackedBits<16>::pack(src+32,dest+16);break;
	case 20: PackedBits<20>::pack(src,dest); PackedBits<20>::pack(src+32,dest+20); break;
	case 32: PackedBits<32>::pack(src,dest); PackedBits<32>::pack(src+32,dest+32); break;
	default: CERR << ("Wrong bits for new pack: "+toString(bits)) << ENDL; break;
	}
}

/*
template <int bits>
void testPackUnpack() {
	unsigned src[128]; unsigned dst1[128]; unsigned dst2[128];
	for(unsigned i=0; i<32; ++i) src[i]=i%(1<<bits);
	PackedBits<bits>::pack(src,dst1);
	PackedBits<bits>::unpack(dst1,dst2);
	for(unsigned i=0; i<32; ++i)
		if(src[i]!=dst2[i]) FATAL("oops");
	for(unsigned i=0; i<20; ++i)
		COUT << (int) ((unchar*)dst1)[i] << " ";
	for(unsigned i=0; i<32; ++i) dst1[i]=0;
	pack(src,5,32,dst1);
	COUT << ENDL;
	for(unsigned i=0; i<5; ++i)
		COUT << ((unsigned*)dst1)[i] << " ";

}
*/
#endif
