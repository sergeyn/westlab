#include <sstream>
#include <fstream>
#include <iterator>
#include <cstring>
#include <iomanip>

#include "Executor.h"
#include "utils.h"

//#include "TestAndBenchmarks.h"
#include "OR.h"
#include "ListComp.h"

#include "pairwiseOracle.h"


void Executor::runAlgorithm(QueryProcessorInterface* queryProcessorAlg,  AugmentsHelperInterface* helper, std::vector<std::string>& terms, std::vector<NewIteratorAugmented*>& lists, QpResult<float>* resArr, score_t threshold) {
	//quantizeAndSave(terms,lists);
	memset((void*)NewIteratorAugmented::liveDeadArea,0,CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK);
	queryProcessorAlg->setup(lists,options,helper,threshold);
	//as hacky as it gets... for testing stuff only
	if(options.ALG == "EvaluateDocs") {
		auto ptrAlg = reinterpret_cast<EvaluateDocs*>(queryProcessorAlg);
		ptrAlg->setTargets(logManager.topKDids(terms));	
	}
	profiler.start(enm::COMPLETEQP);
	queryProcessorAlg->run(options.TOPK);
	profiler.end(enm::COMPLETEQP);
	queryProcessorAlg->putResults(resArr);
}
//=============================================================================================
void Executor::run(QueryProcessorInterface* queryProcessorAlg) {

	//nSetOracle<2> pairsOra("/data1/team/sergeyn/git/newTopK/QueryLog/goldenPairwise");
	//nSetOracle<1> singlesOra("/data1/team/sergeyn/git/newTopK/QueryLog/Top-10_Threshold");

	logManager.loadScoresFromFile(options.GOLDENSCORES.c_str());
	QueryTraceManager::queriesFileIterator queriesFIterator(
			logManager.getIterator());
	queriesFIterator.changeSkipPolicy(options.LIMIT, options.KTERMSBUCKET,
			options.MORETHANKTERMS);
	unsigned queryNum = 0;
	while (queryNum++ < options.LIMIT) {

		if (queryNum < options.SKIPTOQUERY) {
			++queriesFIterator;
			//++queriesFIteratorThreshold;
			continue;
		}

		++queriesFIterator;
		//++queriesFIteratorThreshold;
		if (queriesFIterator == logManager.end())
			break;

		std::vector<std::string> queryTerms = (*queriesFIterator);
		LOG << queryTerms << ENDL;
		COUT4 << queryTerms << ENDL;

		//inject new iterators
		auto lps = index[queryTerms];
		std::vector<NewIteratorAugmented*> lists;
		lists.reserve(lps.size());
		for (unsigned i = 0; i < lps.size(); ++i) {
			lists.push_back(NewIteratorAugmented::scratchPads + i);
			lists[i]->setScratchId(i);
			//lists[i]->maxScoreOfList = lps[i]->getMaxScoreOfList();
			lists[i]->initBM25Float(lps[i]->getUnpaddedLength());
			static OldPforDelta unpacker; //todo: is this correct?
			lists[i]->setDecompressor(&unpacker);
			lists[i]->injectCompressedState(lps[i]->getUnpaddedLength(),
					lps[i]->getMaterializedData(), lps[i]->getSizePerBlockPtr(),
					lps[i]->getMaxDidPerBlockPtr(), lps[i]->getFlagsPtr());
			lists[i]->setPolicy(womanly.suggestPolicy(lps[i]->getTerm()));
			lists[i]->setUseRealScores(lists[i]->getPolicy() == enm::ONTHEFLY);
			lists[i]->initListFromCache(
					womanly.getTheGoodStuff(lps[i]->getTerm()));
			lists[i]->initListFromCacheFloatPart(
					womanly.getTheGoodStuff(lps[i]->getTerm()));

			/*
			 std::ofstream f("/tmp/"+queryTerms[i]);
			 unsigned d=0;
			 lists[i]->setUseReals(false);
			 while(d<CONSTS::NUMERIC::MAXD) {
			 d = lists[i]->nextGEQ(d+1);
			 f << d << " " << lists[i]->getScore() << ENDL;

			 }*/
		}

		//float maxOfPairs = std::max( singlesOra.maxScoreForQuery(queryTerms), pairsOra.maxScoreForQuery(queryTerms));
		//float maxOfPairs = std::max( singlesOra.maxScoreForQuery(queryTerms), 0.0f);
		score_t threshold = (options.CLRVTPERCENT / 100.0)
				* queriesFIterator.score();

		QpResult<float> res[options.TOPK];
		runAlgorithm(queryProcessorAlg, augmentsHelper, queryTerms, lists, res,
				threshold);

		if (options.PRINTALLRES > 1)
			continue;

		float score = res[options.TOPK - 1].score; //resLogger(qn, word_l, res, topk);
		if (options.PRINTALLRES) {
			COUT<< queryTerms << ENDL;
			for (int i=0; i<CONSTS::NUMERIC::TOPK; i++)
			COUT << res[i].did << " " << res[i].score << ENDL;
		}
		// Sanity check, if top-kth score matches with the precomputed golden threshold
		else {

			float gold = queriesFIterator.score();
			if (gold>0.0f && !FloatEquality(score,gold)) {

				CERR << "golden score: " << queriesFIterator.score() << "!=" << score << ENDL;

				for(size_t i=0; i<queryTerms.size(); ++i)
				CERR << queryTerms[i] << ",";
				CERR << ENDL;

				for (int i=0; i<CONSTS::NUMERIC::TOPK; i++)
				CERR << i << "\t" << res[i].did << "\t" << res[i].score << ENDL;
			}
		}
	} // end of qp while loop
}
//=============================================================================================
void Executor::printReport() const {  profiler.printReport(); }
//=============================================================================================
bool QueryTraceManager::queriesFileIterator::isSkip() const {
	//return false;
	if(limit && count>=limit)
		return true;
	const size_t sz = queriesP[curr].size();
	if(sz == 0) {
		CERR << "empty query in log: " << curr << ENDL;
		return true;
	}
	if(sz<=moreThanKTerms) return true;
//	if(sz>10) return true;
	return exactlyKTermsBucket > 0 ? sz != exactlyKTermsBucket : false;
}
//=============================================================================================
QueryTraceManager::queriesFileIterator& QueryTraceManager::queriesFileIterator::operator++() {
	++curr;
	while((unsigned)curr < queriesP.size() && isSkip()) {
		++curr;
	}
	++count;
	return *this;
}
//=============================================================================================
static std::string joinStringsVector(const std::vector<std::string>& terms) {
	std::stringstream buffer;
	std::copy(terms.begin(), terms.end(), std::ostream_iterator<std::string>( buffer ) );
	return buffer.str();
}
//=============================================================================================
float QueryTraceManager::score(const std::vector<std::string>& terms) const {
	const std::string joined(joinStringsVector(terms));
	strToFloatMap::const_iterator it = mapQueryToScore.find(joined);
	if( it == mapQueryToScore.end()) return 0.0f; // FATAL("couldn't find: " + joined);

	return  (*it).second;
}
//=============================================================================================
const std::vector<QpResult<float> >& QueryTraceManager::topKDids(const std::vector<std::string>& terms) const {
	const std::string joined(joinStringsVector(terms));
	auto it = mapQueryToTopResultsVector.find(joined);
	if( it == mapQueryToTopResultsVector.end()) FATAL("couldn't find: " + joined);

	return  (*it).second;
}
//=============================================================================================
void QueryTraceManager::setScoreForQuery(const std::vector<std::string>& terms, float v) {
	mapQueryToScore[joinStringsVector(terms)] = v;
}
//=============================================================================================
void QueryTraceManager::loadTopKResultsFromFile(const std::string& fname, unsigned topk) {
	//assumes the following format:
	//a line of the query followed by topk lines with did score pair
	std::ifstream f(fname);
	while(f.good()) {
		std::string line,term;
		std::getline(f,line);
		if(!line.size())continue;
		
		std::istringstream iss(line);
		std::vector<std::string> terms;
		
		while (std::getline(iss, term, ' ') )  
			terms.push_back(term);
		
		unsigned did;
		float score;
		std::vector<QpResult<float> >& pairs = mapQueryToTopResultsVector[joinStringsVector(terms)];
		
		for(unsigned i=0; i<topk; ++i) {
			f >> did >> score;
			pairs.push_back(QpResult<float>(did,score));
		} //end for
	} //end while 
}
//=============================================================================================
void QueryTraceManager::loadScoresFromFile(const char* fname) {
	//assumes the following format:
	//each line has the terms of a single query (space separated) followed by a float
	FILE *fq = fopen(fname,"r");
	if(fq==NULL) {
		CERR << "input file is wrong: " << fname << ENDL;
		return;
	}
	std::vector<std::string> terms;
	while(1)  {
		terms.clear();
		char line[5120];

		if( readline(fq, line) == -1 ) //this is the end
			break;

		std::string token, text(line);
		std::istringstream iss(text);
		while ( getline(iss, token, ' ') )
			terms.push_back(token);

		//well, last one is not a term, but a score
		float score = atof(terms[terms.size()-1].c_str());
		terms.pop_back();
		setScoreForQuery(terms,score);
	}
	fclose(fq);
}
//=============================================================================================
void QueryTraceManager::shuffle() {
	std::random_shuffle(queriesD.begin(),queriesD.end());
}


QueryTraceManager::QueryTraceManager(const std::string& fname, indexHandler& indexH) : index(indexH) {
	FILE *fq = fopen(fname.c_str(),"r");
	if(fq==NULL) {
		CERR << "input file is wrong: " << fname << ENDL;
		exit(1);
	}
	queriesD.reserve(10000);
	std::vector<std::string> terms;
	while(1)  {
		terms.clear();
		char line[5120];

		if( readline(fq, line) == -1 ) //this is the end
			break;

		char *word;
		word = strtok(line," ");

		if( index.isLoaded(word) ) //only if term is in cache...
			terms.push_back(word);
		else {
			COUT4 << word << " is not in lex" << ENDL;
			LOG << word << " is not in lex" << ENDL;
		}

		while((word = strtok(NULL," ")) != NULL){
			if( index.isLoaded(word) ) //only if term is in cache...
				terms.push_back(word);
			else {
				COUT4 << word << " is not in lex" << ENDL;
				LOG << word << " is not in lex" << ENDL;
			}
		}

		queriesD.push_back(terms);
		setScoreForQuery(terms,0.0);
	}

	COUTV(5) << queriesD.size() << " queries loaded" << ENDL;
	fclose(fq);
}
//=============================================================================================
void PreprocessingOracleManager::loadPoliciesFromFile(const std::string& fname) {
	if(fname == "") return;
	std::fstream is(fname.c_str());
	std::string term, policyStr;
	enm::prprPolicy policy;
	while(is.good()) {
		is >> term >> policyStr;
		convert(policyStr,policy);
		if(storedPolicies.count(term) && storedPolicies[term]!=policy) 
			COUT4 << "Overriding " <<  storedPolicies[term] << " with " << policy << " in " << term << ENDL;
		
		storedPolicies[term]=policy;
	}
}
//=============================================================================================
void PreprocessingOracleManager::setStaticPolicy(const ListMetaData& list, const unsigned ontheflystopat, const unsigned bmqcstop) {
	const std::string& term = list.getTerm();
	const did_t lengthOfList = list.getUnpaddedLength();
	if(lengthOfList < ontheflystopat)
		storedPolicies[term]=enm::ONTHEFLY; //while working on new code
	else if(lengthOfList < bmqcstop && lengthOfList>(1<<15)) //note the important barrier at 32k!
		storedPolicies[term]=enm::BMQC_PBS;
	else if(lengthOfList < (1<<CONSTS::NUMERIC::FAKE_PBS_LIMIT))
		storedPolicies[term]=enm::BMQ_PBS;
	else
		storedPolicies[term]=enm::BMQ_FAKE;

}
//=============================================================================================
enm::prprPolicy PreprocessingOracleManager::suggestPolicy(const std::string& term) {
	assert(storedPolicies.count(term));
	return storedPolicies[term];
}
//=============================================================================================
PreprocessingData& PreprocessingOracleManager::getTheGoodStuff(const std::string& term) {
#ifdef DEBUG
	assert(storedPolicies.count(term));
	if(storedPolicies[term] != enm::ONTHEFLY) {
		assert(storedPreprocessedAugments.count(term));
	}
#endif
	return storedPreprocessedAugments[term];
}
//=============================================================================================
void PreprocessingOracleManager::preloadAndStoreAccordingToPolicy(std::vector<const ListMetaData*>& lists, AugmentsHelperInterface* helper) {
	for(const ListMetaData* list : lists){
		if(storedPreprocessedAugments.count(list->getTerm())) continue;//we have it already

		#ifdef USEINTPIPE
			NewIteratorIntAugmented& tmp = NewIteratorIntAugmented::scratchPads[0];
		#else
			NewIteratorAugmented& tmp = NewIteratorAugmented::scratchPads[0];
		#endif
		tmp.setPolicy(suggestPolicy(list->getTerm()));
		COUTD << "inject compressed state for: " << list->getTerm() << ENDL;
		static OldPforDelta unpacker;
		tmp.setDecompressor(&unpacker);
		tmp.injectCompressedState( list->getUnpaddedLength(),
				list->getMaterializedData(),
				list->getSizePerBlockPtr(), list->getMaxDidPerBlockPtr(), list->getFlagsPtr() );
		#ifndef USEINTPIPE
			tmp.initBM25Float(list->getUnpaddedLength());
		#endif

		COUT4 << "build " << list->getTerm() << " ["  << list->getUnpaddedLength() << "] with policy: " << tmp.getPolicy() << ENDL;
		storedPreprocessedAugments.insert({list->getTerm(), tmp.buildInitialAugnemtsData(helper)});
	}
}
//=============================================================================================
Executor::Executor(Options& opts, indexHandler& indexH, CompressionInterface* unpckr, QueryTraceManager& qlog, AugmentsHelperInterface* helper)
:options(opts), profiler(profilerC::getTheInstance()),index(indexH),
 /*queryLog(opts.QLOG),*/womanly(unpckr),unpacker(unpckr),logManager(qlog), augmentsHelper(helper){

	//pre-loading of on-demands to memory:
	QueryTraceManager::queriesFileIterator queriesFIterator(logManager.getIterator());
	queriesFIterator.changeSkipPolicy(options.LIMIT,options.KTERMSBUCKET,options.MORETHANKTERMS);
	NewIteratorAugmented::initScratchpads();

	unsigned qn=0;
	while(true) { //go over all queries and preload them
		if(qn<options.SKIPTOQUERY) {
			++queriesFIterator;
			continue;
		}

		++queriesFIterator;
		if(queriesFIterator == logManager.end())
			break;

		std::vector<std::string> terms = (*queriesFIterator);
		COUTD << "preloading " << terms << ENDL;
		std::vector<const ListMetaData*> lists = index[terms];

		//set static policy for list. will be overridden by stored policies if present
		for(const ListMetaData* list : lists)
			womanly.setStaticPolicy(*list,options.ONTHEFLYSTOPAT,options.BMQCSTOPAT);

		womanly.loadPoliciesFromFile(options.POLICY);
		womanly.preloadAndStoreAccordingToPolicy(lists,augmentsHelper); //preloads all
		GLOBALPROFILER.zeroCounter(enm::CALC_BLOCK_SCORE);
	}
}
