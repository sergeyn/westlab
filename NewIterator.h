#ifndef NEWITERATOR_H_
#define NEWITERATOR_H_

#include "globals.h"
#include "utils.h"
#include "Enums.h"
#include "Interfaces.h"

#define USEAUGMENTS

//basic list iterator.
//can be used for compressed lists, raw lists, and quantized lists traversal
class NewIteratorBase {
public:
	did_t did; //the current did of this list. the deep state of the list in a way.

protected:
	const unsigned* maxDidPerBlock;	//array of max dids for each postings block
	unsigned offsetInPostingSpace;	//has different semantics under compressed and raw lists

	//for onthefly guys this is available, for others it is nullptr
	did_t* realDids;	//someone kindly puts here dids

	unsigned  paddedLength;         //number of postings in lists padded to be aligned to block size

	//be careful when you move these two: the buffers must be aligned 16-bytes aligned for sse...
	unsigned  didsBuffer[CONSTS::NUMERIC::BS];	 	//temp buffer of size BS for decompressed dids of current posting block
	unsigned  freqsBuffer[CONSTS::NUMERIC::BS];		//temp buffer of size BS for decompressed freqs of current posting block (if non. quantized index)

	CompressionInterface* unpacker; //handles decompression of postings

	//compression related meta-data and pointers
	unsigned  offsetFromBase;	//where is the compressed data
	unsigned  compressedBlock;	//what block are we operating on?

	const unsigned* sizePerBlock;	// array of compressed sizes for each block of postings
	const unsigned* flags;			// array of compression flags for each block. You hope that unpacker knows to handle them
	const unsigned* ptrToFreqs;     // we obtain this ptr after decompressing a block of postings. Suai!!!

	const unsigned* compressedBase;	// ptr to the actual compressed data in memory
	unsigned char fflag;			//indicator of freqsBuffer content and state

	unsigned  postingsInWindow;		//it must be here, because we want nextGEQ to be window aware.

	//scores part ------
	unsigned char useRealScores;			//indicates that we are in fact using real scores and there is no need to calc. them from freq
	score_t BM25Element; //we need this to calculate BM25 scores from frequencies
	//if real scores are used, they are going to be located here:
	float* realScoresFloat;
	unchar* realScoreByte;

	static const unsigned* documentLengthArr; 	//the lengths of documents, used in bm25 score calculations
	static unsigned  instancesCounter;			//used for bookkeeping -- we reuse the iterators

protected:
	template<int freqOrQuant>
	inline unsigned getFreq();	//return the freq. of this term in current posting (did)
	inline void 	skipToDidBlockAndDecode(const did_t d); //populate the did buffer (not prefix summed)
	inline score_t 	calcOneScoreFloat(const unsigned freq, const unsigned doclen) const; //BM25 calculation

public:
	NewIteratorBase();
	inline void setDecompressor(CompressionInterface* unpacker); //handles decompression of dids/freqs
	static void setDocLenPtr(const unsigned* ptr); //set the pointer to length array (needed for BM25)
	//open list sets the compressed data related pointers
	void injectCompressedState(const unsigned unpaddedLen, const unsigned *data, const unsigned *sizes, const unsigned* maxes, const unsigned* flags);
	void setUseRealScores(bool use);
	inline unsigned	getPaddedLength() const;

	void resetPostingIterator();						//reset the list after traversal for new round

	//main functions:
	template <int useReals>
	inline did_t nextGEQ(const did_t d);				//changes state. returns did s.t. did>=d
	template <int useReals>
	inline score_t	getScore(score_t);  //returns score for current did. this is either real score, result of score calc, or dequantization
	template <int useReals>
	inline unchar	getScore(unchar);  //returns score for current did. this is either real score, result of score calc, or dequantization

};
//=============================================================================================
struct PreprocessingData {
	unchar* 		quantBlockMaxes;
	unchar*			rawQuants;

	unsigned* 		postingBitSet;
	float 			maxScoreOfListFloat; //*std::max_element(blockMaxesBase,blockMaxesBase+paddedLength/CONSTS::NUMERIC::BS);
	unsigned 		maxScoreOfListInt;
	float 	 		quantile; //set quantile to 1 or 0 when in integer pipe

	unchar*		 	nonZeroBlockMaxes;
	unsigned* 		compressedZeros;
	unsigned* 		flagsForBMs;
	unsigned* 		sizesForBMs;

	PreprocessingData();
};
//=============================================================================================
const unsigned MAX_TERMS_IN_QUERY(32);
class NewIteratorAugmentedBase{
public:
	NewIteratorAugmentedBase();

#ifdef	USEAUGMENTS
public:

protected:
	unsigned  scratchPadId;	 //the place of this term in a global ordering of query terms
	unsigned* postingBitSet; //buffer for posting bitset of current window
	unsigned* bmQorC;  	  	 //ptr to quantized or compressed block maxes
	unsigned* postingBitSetBase; //for prestoring values of posting bitset
	unchar*	   nonZeroesBase;
	unsigned* sizesCompBMs;
	unsigned* flagsBM;
	ktuple<unsigned,4> stateForBMQC;

	enm::prprPolicy policy;	  //preprocessing policy indicator

	static unsigned  	didBlockBits;  //how many dids are considered a did space
	static unsigned* 	fakePostingBitSet;
	static did_t*    	scratchPadDids[MAX_TERMS_IN_QUERY];
	static unchar*  	scratchPadQBMs[MAX_TERMS_IN_QUERY];
	static unsigned* 	scratchPadPBS[MAX_TERMS_IN_QUERY];
	static unsigned  	currentWindow;
	static unsigned  	firstDidOfWindow;


public:
	static unchar*		bitSetsPBS[MAX_TERMS_IN_QUERY]; //we need to move to helper...
	static unsigned* 	liveDeadArea; //we need to move to helper...

	void			initListFromCache(PreprocessingData& initData);
#ifdef DEBUG
	PreprocessingData res;
#endif
	inline void 			setScratchId(const unsigned id);
	inline unsigned 		getScratchId() const;
	inline void				setPolicy(enm::prprPolicy p);			//set the policy
	inline enm::prprPolicy	getPolicy() const;					    //get the policy
	inline did_t   			nextLivePosting(const did_t d) const ;  //uses internal pbs to get you next live posting miniblock
	inline did_t			getFirstDidOfNextBlock( const did_t d) const; // get the first did in the next block
	inline did_t	        getFirstDidinWindow() const; // get first did in the window
	inline const unchar*    getPBS() const { return reinterpret_cast<const unchar*>(postingBitSetBase); }
	template<int doMemset>
	static
	void			  		incCurrentWindow();

	static
	inline did_t   			nextLiveArea(const did_t d) ;			//find the next live area in this did's neighborhood (next set bit in bitset)

	static
	inline unsigned			getBits();						 		//get the bits of did space block
	static void				setBits(const unsigned bits);			//set the bits of did space block

protected:
	template <typename T>
	inline void typedGenMaxesWithPtr(const did_t* docIds, const T* scores, T* blockMaxes, const unsigned size, const did_t smallest) const;

	template <typename Q>
	static unsigned 		compressQuantized(const Q* quantized, const unsigned sz, PreprocessingData& prData);
	static void			decompressQuantized(unchar* dst, ktuple<unsigned,4>& offsets,

			const unchar* nonZeroBlockMaxes, unsigned* compressedZeros,
			const unsigned* flagsForBMs, unsigned* sizesForBMs, const unsigned countBlocks); //changed state: offsets in comp. data
#endif
};
//=============================================================================================
class NewIteratorAugmented : public NewIteratorAugmentedBase, public NewIteratorBase, public LowLevelListAccessorInterface {
protected:

	score_t*   blockMaxes;	  //buffer for block maxes of current window
	unchar*		blockMaxesQuant; //buffer for block maxes of current window
	score_t*   blockMaxesBase;	  //for prestoring values of block maxes
	score_t    quantileForBM; //unless onthefly BM scores are linearly quantized and need to be dequantized

	static score_t*  	scratchPadBMs[MAX_TERMS_IN_QUERY];
	static score_t*   	scoresBuffer;		//temp buffer of size BS for decompressed freqs of current posting block
	static score_t*		blockScores[MAX_TERMS_IN_QUERY];
	static score_t*  	scratchPadScores[MAX_TERMS_IN_QUERY];
	static  unchar*		scratchPadBMQs[MAX_TERMS_IN_QUERY];

public:	
	static  unchar*		blockScoresQuants[MAX_TERMS_IN_QUERY]; //need to move it to helper

	NewIteratorAugmented();
	static NewIteratorAugmented scratchPads[MAX_TERMS_IN_QUERY];
	score_t    maxScoreOfList;

	PreprocessingData 		buildInitialAugnemtsData(AugmentsHelperInterface*); 			//this is PREprocessing, not the inrpocessing
	void				preprocessAugmentsForWindow(AugmentsHelperInterface*);			//build the structures for current window
	inline score_t 			getMaxScoreOfBlock(const did_t d) const;//gives you a score of a block containing this did
	inline score_t 			getMaxScoreOfList() const; // get maxscore of list

	static void 			initScratchpads();							//initialize statics

	void 					initBM25Float(const unsigned unpaddedLen);
	void 					initListFromCacheFloatPart(PreprocessingData& initData);

	inline void buildBM(const did_t* docIds, const float* scores, unchar* blockMaxes, const unsigned size, const did_t smallest) const;
	inline void buildBM(const did_t* docIds, const unchar* scores, unchar* blockMaxes, const unsigned size, const did_t smallest) const;

	inline void entireBlockDidsAndScores(did_t* dids, float* scores, const did_t block, const int instrSet=1) const; //decode given block and calc scores
	inline void entireBlockDidsAndScores(did_t* dids, unchar* scores, const did_t block, const int instrSet=1) const; //decode given block and point to q. scores

	inline void batchUnpackingBlocks(did_t endBlock, did_t* dids, float* scores, did_t startBlock=0, const int instrSet=1) const;
	inline void batchUnpackingBlocks(did_t endBlock, did_t* dids, unchar* scores, did_t startBlock=0, const int instrSet=1) const {FATAL("todo");}

	inline void batchUnpackingBlocksUptoDid(did_t& postings, did_t nextWindowsFirst, did_t* realDids, float* scores, did_t& block, const int instrSet=1) const;
	inline void batchUnpackingBlocksUptoDid(did_t& postings, did_t nextWindowsFirst, did_t* realDids, unchar* scores, did_t& block, const int instrSet=1) const;

protected:
	inline const unsigned* entireBlockDids(did_t* dids, const unsigned block) const; //decode dids of a block -- does prefix sum
	void					handleAWindowOfOnTheFly(const did_t start, const did_t end, AugmentsHelperInterface*);
	void					genMaxesWithPtr(const did_t* docIds, const score_t* scores, const unsigned size, const did_t smallest=0); //size is the amount of postings there
	static void			quantizeMaxes(const float* scores, const unsigned sz,  PreprocessingData& prData, const float maxScoreOfList=-1.0f);
};
//=============================================================================================
template<typename It, typename T>
inline void popdown(unsigned ind);
template<typename It, typename T>
inline static void sortByDid(It start, It end);
template<typename It, typename T>
inline static void sortByMaxscore(It start, It end);
template<typename It>
inline static did_t getSmallestDid(It start, It end);


#include "NewIteratorIMP.h"
#include "AugmentsLittleHelpers.h"

#endif /* NEWITERATOR_H_ */
