#ifndef _LISTCOMP_H_
#define _LISTCOMP_H_

#include <vector>
#include <algorithm>
#include <iterator>

#include "SqlProxy.h"
#include "Pfor.h"
#include "utils.h"
#include "globals.h"
#include "sql/sqlite3.h"

#include "ListMetaData.h"

class ListCompression  {
	ListMetaData meta;
	CompressionInterface* compressor;
public:
	vecUInt compressedVector;				// the compressed data
	did_t lengthOfList;
	//unsigned metaInfoLength;						// the length of meta info
	
	ListCompression(const std::string& trm, const unsigned id, const unsigned unpadLen, CompressionInterface* cmp=nullptr) :  compressor(cmp), lengthOfList(unpadLen) {
		meta.setName(trm);
		meta.setTid(id);
		meta.setUnpaddedLength(unpadLen);
	}

	void compressDocsAndFreqs(const vecUInt& dids, const vecUInt& freqsOrQuants, bool isQuant);
	long int serializeToFS(const std::string& rootPath) const ;
	void serializeToDb(SqlProxy&);

	static long int serializeToFS(const std::string& rootPath, const vecUInt& compressedData );
	static void serializeToDb(SqlProxy&, const ListMetaData& meta);
	static int makeDirs(const std::string& rootPath);
};

class Quasi {
	vecUInt boundaries;
	std::vector<ushort> data;
public:
	Quasi(){}
	Quasi(const vecUInt& ranges, const std::vector<ushort>& cmprsd) :boundaries(ranges),data(cmprsd) {}
	void compressDocs(const vecUInt& dids);
	void decompressDocsScalar(vecUInt& dids);
};
#endif
