/*
 * ShiftList.cpp
 *
 *  Created on: Jan 4, 2014
 *      Author: sergeyn
 */

#include "ShiftList.h"
#include "utils.h"
#include "profiling.h"

unsigned ShiftList::MAXDID(25205179);

ShiftList::data_vec uniformFill(unsigned length) {
	ShiftList::data_vec vec;
	vec.reserve(length);
	const unsigned bucketsCount = length>>ShiftList::LOGBUCKETSIZE;
	const unsigned bucketRange = ShiftList::MAXDID/bucketsCount;
	assert(bucketRange < (1<<16) );
	for(unsigned i=0; i<bucketsCount; ++i)
		vec.push_back(1);
		for(unsigned n=1; n<ShiftList::BUCKETSIZE; ++n)
			vec.push_back((bucketRange/ShiftList::BUCKETSIZE*n)-1);
	return vec;
}

ShiftList::data_vec compress(const std::vector<unsigned>& src) {
	ShiftList::data_vec vec;

	const unsigned bucketsCount = (src.size()>>ShiftList::LOGBUCKETSIZE)+1; //todo: no +1 if no modulo
	const unsigned bucketRange = ((ShiftList::MAXDID+256)/bucketsCount)*0.66;
	//vec.resize((64+bucketsCount)<<ShiftList::LOGBUCKETSIZE);

	unsigned opBucket = 0;
	unsigned offsetInB = 0;
	unsigned overflow = 0;
	for(auto did : src) {
		const unsigned bucket = (did/bucketRange)<<ShiftList::LOGBUCKETSIZE;
		const unsigned start = (bucket>>ShiftList::LOGBUCKETSIZE)*bucketRange;

		if(bucket!=opBucket) {
			opBucket = bucket; offsetInB = 0;
		}
		if(offsetInB<ShiftList::BUCKETSIZE) {
			offsetInB++; //vec[opBucket+offsetInB++] = did-start;
			//if(opBucket+offsetInB>vec.size()) COUTL << did << " @ " << opBucket+offsetInB << ">" << vec.size() << ENDL;
		}
		else
			++overflow;
	}
	COUT << src.size() << " compressed: " << overflow << " postings overflew" << ENDL;
	return vec;
}

ShiftList::ShiftList(unsigned length) {
	postings = uniformFill(length);
}

ShiftList::ShiftList(const std::vector<unsigned>& src) {
	postings = compress(src);
};

/* Concrete example:
 * len: 64123
 * buckets: 8015
 * range: 3144
 *
 * did: 141872
 * bucket: 141872/3144 = 45
 */
void _unit_test_ShiftList(unsigned length, bool isBenchmark) {
	std::vector<unsigned> src(length);
	src[0]=1;
	const unsigned blk = (ShiftList::MAXDID/length);
	for(unsigned i=1; i<length; ++i)
		src[i] = blk*i-1; //model an ideal list

	ShiftList sl(src);
	std::random_shuffle(src.begin(),src.end());

	const unsigned lookups(4096);
	std::sort(src.begin(),src.begin()+lookups);

	if(isBenchmark) thrashSomeCache(1<<25);

	sTimer t;
	t.start();
	volatile bool flag = true;
	for(unsigned i=0; i<lookups; ++i)
		flag &= sl.has(src[i]);

	if(isBenchmark) COUT << t.end()/1000.0 << "ms" << ENDL;
	if(!flag) CERR <<  "bug in shiftlist! could not find " << ENDL;
}
