#ifndef TYPES_H_
#define TYPES_H_
#include <stdint.h>

	typedef float score_t;
	typedef unsigned tid_t;
	typedef unsigned did_t;
	typedef uint8_t unchar;
	typedef unsigned short unshort;
	typedef unsigned short scoreInt_t;

#ifdef CPP0X
	#include <unordered_map>
	#include <unordered_set>
	#define hashMapStd std::unordered_map
	#define hashSetStd std::unordered_set
#else
	#include <map>
	#include <set>
	#define hashMapStd std::map
	#define hashSetStd std::set
#endif

	#include <string>
	typedef hashMapStd<std::string, int> termsMap;
	typedef hashMapStd<std::string, unsigned> termsToUintMap;
	typedef hashMapStd<unsigned,std::string> uintToTermsMap;
	typedef hashMapStd<std::string, float> strToFloatMap;


	typedef std::string STRING;

	#include <vector>
	typedef std::vector<unsigned int> vecUInt;
	typedef	 std::vector<unsigned int>::iterator vecUIntItr;
	typedef	 std::vector<unsigned int>::const_iterator vecUIntCItr;

#endif /* TYPES_H_ */
