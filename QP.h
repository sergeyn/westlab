#ifndef QP_H_
#define QP_H_

#include "NewIterator.h"
#include "options.h"

#define ITERATOR_VEC_PTR NewIteratorAugmented**
#define ITERATOR_VEC_PTR_INT NewIteratorIntAugmented**

class QueryProcessorInterface : public RegistryEntry{
public:
	virtual ~QueryProcessorInterface() {}
	virtual void run(const unsigned topk) = 0;
	virtual void putResults(void* res) = 0;
	//the threshold is always given as float.
	virtual void setup(std::vector<NewIteratorAugmented*>& lps, Options opts, AugmentsHelperInterface* augments, const float thresh) = 0;
};

#endif /* QP_H_ */
