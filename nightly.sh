#!/bin/bash

#================= it should at least build =========================
make clean
make -j23
#=========================check small random sample =================
export LIMIT=10
echo "-clrvtpercent 0 -alg 'ExhaustiveWindowedOR<float,float,1>' -aughelper 'AugmentsQuantBlockMaxScalar<float>' -verbosity 1 -limit $LIMIT"
./qp -clrvtpercent 0 -alg 'ExhaustiveWindowedOR<float,float,1>' -aughelper 'AugmentsQuantBlockMaxScalar<float>' -verbosity 1 -shuffle 1 -limit $LIMIT
echo "-clrvtpercent 0 -alg 'ExhaustiveWindowedOR<float,float,1>' -aughelper 'AugmentsQuantBlockMaxSSE<float>' -verbosity 1 -limit $LIMIT"
./qp -clrvtpercent 0 -alg 'ExhaustiveWindowedOR<float,float,1>' -aughelper 'AugmentsQuantBlockMaxSSE<float>' -verbosity 1 -shuffle 1  -limit $LIMIT

echo "-clrvtpercent 99 -alg 'ExhaustiveWindowedOR<float,float,1>' -aughelper 'AugmentsQuantBlockMaxScalar<float>' -verbosity 1 -limit $LIMIT"
./qp -clrvtpercent 99 -alg 'ExhaustiveWindowedOR<float,float,1>' -aughelper 'AugmentsQuantBlockMaxScalar<float>' -verbosity 1 -shuffle 1 -limit $LIMIT
echo "-clrvtpercent 99 -alg 'ExhaustiveWindowedOR<float,float,1>' -aughelper 'AugmentsQuantBlockMaxSSE<float>' -verbosity 1 -limit $LIMIT"
./qp -clrvtpercent 99 -alg 'ExhaustiveWindowedOR<float,float,1>' -aughelper 'AugmentsQuantBlockMaxSSE<float>' -verbosity 1 -shuffle 1  -limit $LIMIT
#===========================check all =========================================
export LIMIT=1000
echo "-clrvtpercent 0 -alg 'ExhaustiveWindowedOR<float,float,1>' -aughelper 'AugmentsQuantBlockMaxScalar<float>' -verbosity 1 -limit $LIMIT"
./qp -clrvtpercent 0 -alg 'ExhaustiveWindowedOR<float,float,1>' -aughelper 'AugmentsQuantBlockMaxScalar<float>' -verbosity 1 -shuffle 1 -limit $LIMIT
echo "-clrvtpercent 0 -alg 'ExhaustiveWindowedOR<float,float,1>' -aughelper 'AugmentsQuantBlockMaxSSE<float>' -verbosity 1 -limit $LIMIT"
./qp -clrvtpercent 0 -alg 'ExhaustiveWindowedOR<float,float,1>' -aughelper 'AugmentsQuantBlockMaxSSE<float>' -verbosity 1 -shuffle 1  -limit $LIMIT

echo "-clrvtpercent 99 -alg 'ExhaustiveWindowedOR<float,float,1>' -aughelper 'AugmentsQuantBlockMaxScalar<float>' -verbosity 1 -limit $LIMIT"
./qp -clrvtpercent 99 -alg 'ExhaustiveWindowedOR<float,float,1>' -aughelper 'AugmentsQuantBlockMaxScalar<float>' -verbosity 1 -shuffle 1 -limit $LIMIT
echo "-clrvtpercent 99 -alg 'ExhaustiveWindowedOR<float,float,1>' -aughelper 'AugmentsQuantBlockMaxSSE<float>' -verbosity 1 -limit $LIMIT"
./qp -clrvtpercent 99 -alg 'ExhaustiveWindowedOR<float,float,1>' -aughelper 'AugmentsQuantBlockMaxSSE<float>' -verbosity 1 -shuffle 1  -limit $LIMIT
