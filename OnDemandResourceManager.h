#ifndef ONDEMANDRESOURCEMANAGER_H_
#define ONDEMANDRESOURCEMANAGER_H_

#include <vector>
#include <deque>
#include <algorithm>

#include "Interfaces.h"
#include "types.h"

//typedef std::vector<unsigned> DemandType;
//typedef std::string KeyType;

template <typename KeyType, typename DemandType>
class OnDemandManager {
	void evictData(unsigned index) { storage[index]=DemandType(); keysToInds[indsToKeys[index]]=capacity+1; } //evict
	bool isLRU(unsigned index) { return std::find(LRU.begin(),LRU.end(),index) == LRU.end(); } //not real LRU - just watchdog for latest guys
	bool isOccupied(unsigned index) { return !storage[index].empty(); }
	bool isValid(unsigned index) { return index == keysToInds[indsToKeys[index]]; }
	void thinOutTheirNumbers(); //remove invalids from mappings
public:
	OnDemandManager(BringCompressedDataInterface<DemandType>* supplier, const unsigned cap):
		capacity(cap),counter(0),dataSupplier(supplier) {
		storage.clear();
		storage.resize(cap);
		LRU.resize(32,cap+1); //unless you want to support >32 terms in a query...
		if(LRU.size()>cap) FATAL("increase your capacity");
	}

	void resize(unsigned newSz) { FATAL("no support yet"); }

	const DemandType& getMaterializedData(const KeyType& term){
		//COUTD << "Materialize: " << term << ENDL;
		//test if hit
		auto ind = keysToInds.find(term);
		if(ind != keysToInds.end() && isValid(ind->second)) {
			size_t indx = ind->second;
			LRU.pop_front();
			LRU.push_back(indx);
			//COUTD << "Materialize found: " << indx << " size: " << storage[indx].size() << ENDL;
			if(storage[indx].empty()) FATAL("trying to return empty data buffer");
			return storage[indx]; //this one is materialized already
		}

		//no hit, so we need to place it in storage
		while(isOccupied(counter) && isLRU(counter)) //pick non-colliding victim for eviction
			counter = (counter+1)%capacity;

		const unsigned candidate = counter;
		if(isOccupied(candidate))  //the bucket is occupied
			evictData(candidate);

		//store this in lookup structures
		keysToInds[term]=candidate;
		indsToKeys[candidate]=term;
		LRU.pop_front(); //update the list of recent guys
		LRU.push_back(candidate);

		//COUTD << "Materialize bring " << term << " to " << candidate << ENDL;
		dataSupplier->bring(term,storage[candidate]);

		counter = (counter+1)%capacity;
		return storage[candidate];

	}

private:
	size_t capacity;		//how much terms we can handle before evicting
	size_t counter;		 	//how many terms we have in a preloaded state
	std::vector<DemandType> storage; //storage for data
 	hashMapStd<unsigned,KeyType> indsToKeys;		//mapping offsets to terms in storage
 	hashMapStd<KeyType,unsigned> keysToInds;		//mapping terms to offset in storage
 	std::deque<unsigned> LRU;

 	BringCompressedDataInterface<DemandType>* dataSupplier;

};

#endif /* ONDEMANDRESOURCEMANAGER_H_ */
