#ifndef AND_H_
#define AND_H_

#include "QP.h"

template<int UseAugments>
class Intersection : public QueryProcessorInterface {
private:
	std::vector<NewIteratorAugmented*> lists;
	Options options;
	AugmentsHelperInterface* augmentsHelper;
	did_t counterHits;
	did_t pbsLive;

public:
	Intersection():augmentsHelper(nullptr),counterHits(0),pbsLive(0){};
	~Intersection(){};
	void setup(std::vector<NewIteratorAugmented*>& lps, Options opts, AugmentsHelperInterface* augments, const float thresh=0);

	virtual void run(const unsigned topk=0);
	virtual void putResults(void* res);

	virtual const std::string name() const;
	virtual void* getOne() const;
};

did_t naiveIntersect(NewIteratorAugmented** lists, const unsigned termsCount);

class RawDidsIterator {
	const did_t* list;
	did_t lengthOfList;
	did_t index;
public:
	did_t did;

	RawDidsIterator(did_t length, did_t* dids) : list(dids), lengthOfList(length), index(0){
		dids[lengthOfList] = CONSTS::NUMERIC::MAXD; //put an elephant there
		did=(list[0]);
	}
	void reset() { index=0;}

	inline did_t nextGEQ(const did_t d) {
		while(list[index]<d) ++index;
		return (did = list[index]);
	}

	inline bool aligned(did_t a, did_t b) const { return a==b && did == b; }
};

class FakeDidsIterator {
public:
	FakeDidsIterator(){}
	inline did_t nextGEQ(did_t did) { return did; }
	inline bool aligned(did_t a, did_t b) const { return a==b; }
};

#include "AndImp.h"

#endif /* AND_H_ */
