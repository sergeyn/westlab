exit((STDERR.puts 'ERR: two arguments are mandatory: maxd and terms count (how many documents in corpus and how many terms to generate)').to_i) unless ARGV.size > 1

maxd = ARGV[0].to_i
exit((STDERR.puts 'ERR: maxd set to 0?').to_i) unless maxd>0

terms = ARGV[1].to_i

(1..terms).each { |tid|
	term = (0...16).map{(65+rand(26)).chr}.join #generate a random string

	#note the distribution of short and long lists is uniform in this (but Zipfian in real data)	
	assumeLen = 1+(rand maxd)
	step = 2+(maxd/assumeLen).to_i
	
	postings = []
	docid=0
	docid = [[rand(maxd-docid),3].max+docid,docid+step].min
	while(docid<maxd) do
		postings << docid << rand(10)+1
		docid = [[rand(maxd-docid),3].max+docid,docid+step].min
	end

	print "#{tid} #{term} #{postings.size / 2} "
	puts postings.join " " 
}
