/*
 * ThresholdPredictor.h
 *
 *  Created on: Nov 10, 2012
 *      Author: constantinos
 */

#ifndef THRESHOLDPREDICTOR_H_
#define THRESHOLDPREDICTOR_H_
/*
#include "BMW.h"
#include "naiveOR.h"
#include "LAStorage.h"
#include "FastShiftBlocker.h"

class ThresholdPredictor {
private:
	unsigned int* pages;
    const unsigned char labits;

public:
	ThresholdPredictor(unsigned int* pgs, const unsigned char bits) : pages(pgs), labits(bits) {}
	std::vector<FastShiftBlocker*>* hashblocker;

	// Pairwise
	float MaxPairwise(lptrArray& lps, const unsigned int topk);

	// simple predictors, assumes space = (topk*distinct number of terms in your index) * float * unsigned int
	float TopKScoresANDDids(lptrArray& lps, const unsigned int topk);
	float MaxTopKScore(lptrArray& lps, const unsigned int topk);

	// Sampling
	float SampleANDEvaluateDids(lptrArray& lps, const unsigned int samplesize, const unsigned int topk); // really bad performance
	float BiasedEvaluateDids(lptrArray& lps, const unsigned int evalsize, const unsigned int topk);
	float SamplingBlocks(lptrArray& lps, const unsigned int samplesize, const unsigned int topk);
	float BiasedSamplingBlocks(lptrArray& lps, const unsigned int samplesize, const unsigned int topk);
	float SamplingBlocksANDEvaluateDids(lptrArray& lps, const unsigned int samplesize, const unsigned int topk); // TODO sample, get topk then eval inside the blocks

	// Hybrid - very good (3/150 unsafe)
	float Hybrid(lptrArray& lps, const unsigned int samplesize, const unsigned int topk);
};
*/

#endif /* THRESHOLDPREDICTOR_H_ */
