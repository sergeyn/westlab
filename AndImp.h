#ifndef ANDIMP_H_
#define ANDIMP_H_

#include "utils.h"

template <int UseAugments>
void Intersection<UseAugments>::setup(std::vector<NewIteratorAugmented*>& lps, Options opts, AugmentsHelperInterface* augments, const float thresh){
	lists=(lps);
	options=(opts);
	augmentsHelper=(augments);
}

//todo: make nice
extern did_t intersection[25123123];
extern uint64_t intersectionBitSet[25123123];

template<int specialListValid>
void intersectBitSets(const void** bits, unsigned termsCount, unsigned bytesCount) { //todo: simd version
	const uint64_t** sets = reinterpret_cast<const uint64_t**>(bits);
	for(unsigned wordIndex=0; wordIndex<bytesCount/(sizeof(uint64_t)); ++wordIndex) {
		if(specialListValid==0) intersectionBitSet[wordIndex]=sets[0][wordIndex];
		else intersectionBitSet[wordIndex]&=sets[0][wordIndex];
		for(unsigned term=1; intersectionBitSet[wordIndex] && term<termsCount; ++term) intersectionBitSet[wordIndex] &= sets[term][wordIndex];
	} 	//the original bitsets had elephants, hence we expect one here as well
}

inline did_t getNextLiveDid(const void* bs, did_t currentDid, unsigned shiftBy) { return (getNextSetBit(bs, currentDid>>shiftBy))<<shiftBy; }

template<typename SpecialList>
did_t fastIntersect(SpecialList* sList, NewIteratorAugmented** listsA, const unchar termsCnt, const unchar didblockbits) {

	did_t biggestCandidateDid = 0;
	did_t did;
	did_t counterHits = 0;
	while (biggestCandidateDid<CONSTS::NUMERIC::MAXD) {
		bool aligned = true;
		//mv first list and compare
		biggestCandidateDid = did = std::max(biggestCandidateDid, sList->nextGEQ( biggestCandidateDid ));

		unsigned i=0;
		for (; i<termsCnt && aligned && biggestCandidateDid<CONSTS::NUMERIC::MAXD; i++) {
			biggestCandidateDid = std::max(biggestCandidateDid, listsA[i]->nextGEQ<0>( biggestCandidateDid )); //nextGEQ and compare
			aligned &= sList->aligned(listsA[i]->did,listsA[0]->did);
		}
		if(i==termsCnt && aligned) {
			++counterHits;
			++biggestCandidateDid;
		}
		biggestCandidateDid = std::max(biggestCandidateDid,getNextLiveDid((uint64_t*)intersectionBitSet,biggestCandidateDid,didblockbits));
	} //end while
	return counterHits;
}

void embedDidsInPBS(const did_t* dids, const did_t sz, const unchar didBlockBits, void* dest, const unsigned szBytesDest);

template <int UseAugments>
void Intersection<UseAugments>::run(const unsigned) {
	NewIteratorAugmented* listsA[MAX_TERMS_IN_QUERY];
	const unsigned termsCnt = this->lists.size();
	for(unsigned i=0; i<termsCnt; ++i) listsA[i]=this->lists[i];
	sortByLength<ITERATOR_VEC_PTR,NewIteratorAugmented>( listsA+0, listsA+termsCnt);

	COUT << " <4> " << listsA[0]->getPaddedLength() << "\n"; return;
	auto stopAt = options.ONTHEFLYSTOPAT;
	unsigned shortCount = std::count_if(lists.begin(),lists.end(),
									[&stopAt](const NewIteratorAugmented* a){return a->getPaddedLength()<stopAt;});
	unsigned fakePBSsCount = std::count_if(lists.begin(),lists.end(),
			[](const NewIteratorAugmented* a){return a->getPaddedLength()>=(1<<CONSTS::NUMERIC::FAKE_PBS_LIMIT);});

	//naive alg. running or hard case: no shorts, all are fake
	if(!UseAugments ||  fakePBSsCount==termsCnt)  shortCount = termsCnt; //do all of them naively

	pbsLive = 0;
	did_t intersectionSize = counterHits = naiveIntersect(listsA, shortCount);
	//case 1: no long ones to intersect at all or this is the naive flow
	if(shortCount == termsCnt || intersectionSize == 0) //then done, we have the intersection ready
		return;

	counterHits = 0;

	const int CONSIDER_SHORTS_INTERSECTION(1);
	const int NO_SHORTS(0);

	const unsigned sizeBytesBS = (CONSTS::NUMERIC::MAXD>>(options.DIDBLOCKBITS-3))/8;
	const void* pbss[MAX_TERMS_IN_QUERY];
	for(unsigned lt=shortCount; lt<termsCnt-fakePBSsCount; ++lt) //get the long non-fake ones if any
		pbss[lt-shortCount] = listsA[lt]->getPBS();

	//todo: when finally working, we can make a special case where the intersection of otfs is so short we can just ignore all this mess...
	//case 2: some are short and are already intersected, some are long, some are fake
	if(shortCount > 0) {
		//embed shorts in did space
		embedDidsInPBS(intersection,intersectionSize,options.DIDBLOCKBITS-3,intersectionBitSet,sizeBytesBS);
		intersectBitSets<CONSIDER_SHORTS_INTERSECTION>(pbss,termsCnt-shortCount-fakePBSsCount,sizeBytesBS);
		RawDidsIterator special(intersectionSize,intersection);
		counterHits = fastIntersect(&special,listsA+shortCount,termsCnt-shortCount,options.DIDBLOCKBITS-3);
		//COUT<<" CONSIDER_SHORTS_INTERSECTION ";
	}
	else { //case 3: no short ones at all, and only _some_ are fake
		//this case better be done naively as well, or so it seems :(
		intersectBitSets<NO_SHORTS>(pbss,termsCnt-fakePBSsCount,sizeBytesBS);
		FakeDidsIterator special;
		counterHits = fastIntersect(&special,listsA,termsCnt,options.DIDBLOCKBITS-3);
		//COUT<<" NO_SHORTS ";
	}
	//pbsLive = countSetBits(listsA[2]->getPBS(),sizeBytesBS);
	/*
	238855003 5.99  gold
	6 bits
	242615215 6.06  overhead
	232783006 11.49 no-shorts
	159027387 18.56 all
	5 bits
	131024015 12.89 all

	pangolin:
	239233350 7.45 gold
	159402063 24.47 all
	*/

}

template <int UseAugments>
const std::string  Intersection<UseAugments>::name() const { return "Intersection<" + toString(UseAugments) + ">"; }

template <int UseAugments>
void* Intersection<UseAugments>::getOne() const { return new Intersection<UseAugments>(); }

template <int UseAugments>
void Intersection<UseAugments>::putResults(void* res) {	COUT2 << counterHits << " hits " << pbsLive << " pbsLive\n";}

#endif /* ANDIMP_H_ */
