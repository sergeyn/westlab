#ifndef INDEXBUILDER_H_
#define INDEXBUILDER_H_

//=============================================================================================
#include <mutex>
#include <atomic>

#include "../Interfaces.h"


class IndexBuilder {
public:
	//const std::string sourceBase = "/home/sergeyn/BMW/index_result/";
	//const std::string sourceMapping = sourceBase+"word_file";

	static void buildIndex(std::mutex& writerMutex, IndexBuilderFilterInterface& filter, const std::string destBase, bool calcScores, bool quantize, const std::string sourceBase = "/data/sding/TwoLevelTrec/index_result/", const std::string sourceMapping = "../QueryLog/10000q_terms.mapping.isorted" );
	static void mergeTwoNewStyleIndexDirs(const std::string& src1, const std::string& src2, const std::string& dest);
};

class lengthAwareFilter : public IndexBuilderFilterInterface{
	std::vector<unsigned> inf;
	const unsigned threshold;
	unsigned counter;
public:
	explicit lengthAwareFilter(const std::string infPath, const unsigned thresh=128);
	bool good(const std::string& term) {return false;}
	bool good(const unsigned long tid);
};


//compress and write the terms from a flat file to destination dir
//see the description of the flat file format in the .cpp
void indexFlatFile(const char* flatFile, const unsigned maxd,  const char* destinationPath, bool isQuant=false, CompressionInterface* cmprsr=nullptr);

#endif /* INDEXBUILDER_H_ */
