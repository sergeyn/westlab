/*
 * IndexBuilder.cpp
 *
 *  Created on: Apr 9, 2013
 *      Author: sergeyn
 */


#include "IndexBuilder.h"

#include <fstream>

#include "TrecReader.h"
#include "../ListComp.h"
#include "../NewIterator.h"
#include "../IndexHandler.h"
#include "../SSE.h"
//======================================================================
void IndexBuilder::buildIndex(std::mutex& writerMutex, IndexBuilderFilterInterface& filter, const std::string destBase, bool calcScores, bool quantize, const std::string sourceBase, const std::string sourceMapping ) {
	//TODO: inject an abstract reader -- then we can use the same code for vectors of dids/freqs/scores.
	TrecReader reader = TrecReader(
			sourceBase+"merged_bool/8_1.dat",
			sourceBase+"merged_bool/8_1.inf",
			sourceBase+"doclen_file",
			sourceMapping,
			CONSTS::NUMERIC::MAXD); //it should get MAXD in some other way, dude!

	//load document lengths and override static
	unsigned *ptr;
	NEWMA((void**)&ptr,sizeof(int)*(256+CONSTS::NUMERIC::MAXD));

	FILE *fdoclength = fopen64((sourceBase+"doclen_file").c_str(),"r");
	if( fread(ptr,sizeof(int), CONSTS::NUMERIC::MAXD, fdoclength) != CONSTS::NUMERIC::MAXD )
		FATAL("can not read doclength");

	for(unsigned i = 0; i< 256; i++) //bogus data for padding
		ptr[ i + CONSTS::NUMERIC::MAXD ] = 0;

	NewIteratorBase::setDocLenPtr(ptr); //set the static variable

	COUT4 << "Read words from :" << sourceMapping << ENDL;
	std::ifstream words(sourceMapping);
	std::string term;
	unsigned long tid;

	ListCompression::makeDirs(destBase);
	SqlProxy sql(destBase+"indx.sqlite");
	//NewIteratorAugmented& list = NewIteratorAugmented::scratchPads[0];

	while(words.good()) {
		//read the term and its termid
		words >> term >> tid;
		if(!filter.good(tid)) continue;

		std::vector<unsigned> docids;
		std::vector<unsigned> freqs;

		//read the list from raw file
		reader.getList(term.c_str(),tid,docids,freqs);

		const unsigned unpaddedLen = docids.size();
		//pad with MAXDs...
		unsigned pad = 0;
		while(docids.size()%CONSTS::NUMERIC::BS != 0) {
			docids.push_back(CONSTS::NUMERIC::MAXD+(pad++));
			freqs.push_back(1); //Suai was padding with 1 (when minus-one become zeroes)
		}

		const unsigned sz(docids.size());
		//TODO: inject an abstract freq->converter: for quantization or anything else
		std::vector<score_t> scores;
		if(calcScores) {
			std::vector<unsigned> lengths;
			float bm25 = 3.0f * (float)(  log( 1.0 + (float)(CONSTS::NUMERIC::MAXD - unpaddedLen + 0.5f)/(float)( unpaddedLen + 0.5f))/log(2));
			scores.resize(sz);
			lengths.resize(sz);

			for(unsigned i=0; i<sz; ++i)
				lengths[i]=ptr[docids[i]]; //build lengths array
			for(did_t i=0; i<sz; i+=CONSTS::NUMERIC::BS)
			calcScoresForBlockSSE(&freqs[i], &lengths[i], bm25, &scores[i] );
			//float maxScore = *(std::max_element(scores.begin(),scores.end())); //todo
		}

		if(quantize) {
			for(did_t i=0; i<sz; ++i)
				freqs[i]=quantF_(scores[i]);
		}

		//compressq
		ListCompression cmprsr(term, tid,unpaddedLen);
		cmprsr.compressDocsAndFreqs(docids,freqs,quantize);
		COUT4 << term << " " << tid << ENDL;
		writerMutex.lock(); //could be replaced with recompiling sql in multi-t mode
		try {
			cmprsr.serializeToFS(destBase);
			cmprsr.serializeToDb(sql);
		}catch(...) {
			COUTL << "exception in serialization. probably the data file is corrupted now :(" << ENDL;
		}
		writerMutex.unlock();
	}
}
//======================================================================
static void handleOneTerm(SqlProxy& sql, SqliteBasedLexicon& lex, const std::string& term, const std::string& dest) {
	const ListMetaData* meta = reinterpret_cast<const ListMetaData*>(lex.getConstListHandleUnsafe(term));
	ListCompression::serializeToDb(sql,*meta);
	const unsigned* ptr = meta->getMaterializedData();
	vecUInt localCopy(ptr,ptr+meta->getCompressedDataLengthBytes()/sizeof(unsigned));
	ListCompression::serializeToFS(dest,localCopy);
}
//======================================================================
void IndexBuilder::mergeTwoNewStyleIndexDirs(const std::string& src1, const std::string& src2, const std::string& dest) {
	//Boo-Boo alert: this thing will break if the lexicons of src1 and src2 are not disjoint!!!
	SqliteBasedLexicon lex1, lex2;
	lex1.setPath(src1); lex2.setPath(src2);
	std::vector<std::string> terms1 = lex1.loadMeta();
	lex1.loadLexicon(&terms1,false);
	std::vector<std::string> terms2 = lex2.loadMeta();
	lex2.loadLexicon(&terms2,false);
	CERR << src1 << " has " << terms1.size() << " terms\n" << src2 << " has " << terms2.size() << " terms\n" << ENDL;
	ListCompression::makeDirs(dest);
	SqlProxy sql(dest+"indx.sqlite");

	//merge sort by tid:
	unsigned i1 = 0; unsigned i2=0;
	tid_t t1,t2;
	for(; i1<terms1.size() && i2<terms2.size(); ) {
		if( (t1=lex1.getTid(terms1[i1])) < (t2=lex2.getTid(terms2[i2])) ) {
			handleOneTerm(sql,lex1,terms1[i1],dest);
			++i1;
		}
		else {
			handleOneTerm(sql,lex2,terms2[i2],dest);
			++i2;
		}
	}

	std::vector<std::string>* terms = (i1<terms1.size()) ? &terms1 : &terms2;
	SqliteBasedLexicon* leftOvers = (i1<terms1.size()) ? &lex1 : &lex2;
	for(unsigned i=(i1<terms1.size()?i1:i2); i<terms->size(); ++i)
		handleOneTerm(sql,*leftOvers,(*terms)[i],dest);
		//COUT << leftOvers->getTid((*terms)[i]) << "\n";

	CERR << "done with ptrs at:" << i1 << " " << i2 << ENDL;
}
//======================================================================
lengthAwareFilter::lengthAwareFilter(const std::string infPath, const unsigned thresh):threshold(thresh),counter(0) {
	TrecReader::loadInf(infPath,inf);
}
//======================================================================
bool lengthAwareFilter::good(const unsigned long tid) {
	if(++counter % 100000 == 0) COUT4 << counter << " terms" << ENDL;
	return true;
	//if(counter & 1) return true;
	//return TrecReader::getLength(tid,inf)>=threshold;
	//temp -- augmenting 1024 downto 3:
	const unsigned len = TrecReader::getLength(tid,inf);
	return len>=64 && len<1024;

}

void handleOneTerm(const std::string& destDir, SqlProxy& sql, const vecUInt& dids, const vecUInt& freqsOrQuants,
					unsigned tid, const std::string& term, unsigned unpaddedLen, bool isQuant=false, CompressionInterface* cmprs=nullptr) {
	assert(dids.size() == freqsOrQuants.size()); assert(dids.size()%CONSTS::NUMERIC::BS == 0); //assume padded

	ListCompression cmprsr(term, tid,unpaddedLen,cmprs);
	cmprsr.compressDocsAndFreqs(dids,freqsOrQuants,isQuant);
	cmprsr.serializeToFS(destDir);
	cmprsr.serializeToDb(sql);
}

void buildRandomDoclen(const char* doclenFile, const unsigned maxd) {
	vecUInt lens;
	lens.resize(maxd);
	for(unsigned i=0; i<maxd; ++i)
		lens[i] = rand()%5000+36;
	appendToFile(std::string(doclenFile),lens.begin(),lens.end());
}

//compress and write the terms from a flat file to destination dir
//flat file is an ascii file where every line has:
//term-id term length d1 f1 d2 f2 ...
//where term-id is the unique id of the term
//term is the actual keyword
//length is how many postings to expect
//di fi  -- are doc-id and frequency (or quantized score) of this doc-id
void indexFlatFile(const char* flatFile, const unsigned maxd, const char* destinationPath, bool isQuant, CompressionInterface* cmprsr) {
	//buildRandomDoclen("doclen",250000);
	const std::string& destDir(destinationPath);
	ListCompression::makeDirs(destDir);
	SqlProxy sql(destDir+"/indx.sqlite");

	//overwrite config file
	std::ofstream cfgOut(destDir+"/config");
	cfgOut << "SqliteBasedLexicon\n" << cmprsr->getName() << "\n" << maxd << ENDL;

	std::ifstream in(flatFile);
	if(!in.is_open()) CERR << "couldn't open " << flatFile <<ENDL;
	vecUInt dids, freqs;
	while(in.good()){
		std::string term;
		unsigned tid;
		unsigned len;
		in >> tid >> term >> len; //read tid,term,unpadded length
		if(!in.good()) break;
		dids.clear(); freqs.clear();
		dids.reserve(len+256);
		freqs.reserve(len+256);

		unsigned did, freq;
		for(unsigned i=0; i<len; ++i) { //read all dids,freqs from file
			in >> did >> freq;
			dids.push_back(did);
			freqs.push_back(freq);
		}
		//pad with MAXDs...
		unsigned pad = 0;
		while(dids.size()%CONSTS::NUMERIC::BS != 0) {
			dids.push_back(maxd+(pad++));
			freqs.push_back(1); //Suai was padding with 1 (when minus-one become zeroes)
		} //end while
		COUT4 << "TermId: " << tid << " Len: " << len << "(" << dids.size() << ")\n";
		for(unsigned i=0; i<64; ++i) COUT << dids[i] << " " ;
			COUT<<ENDL;
		handleOneTerm(destDir,sql,dids,freqs,tid,term,len,isQuant,cmprsr);
	}
	COUT4 << "DONE" << ENDL;
}

