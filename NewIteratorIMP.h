#ifndef NEWITERATORIMP_H_
#define NEWITERATORIMP_H_

#include "Compression.h"
#include "profiling.h"
#include "SSE.h"
#include "Scalar.h"

#include <cstring>
//=============================================================================================
template<typename It, typename T>
inline void popdown(It start, It end) {
	 T* temp = *(start); 
         ++start;
         while( start != end && temp->did >= ( (*start)->did) )        {
                       *(start-1) = *(start); //this->operator [](j-1) = this->operator [](j); //swap
                        ++start;
          }
	  *(start-1) = temp; //  this->operator [](j - 1) = temp;
}
//=============================================================================================
	template<typename It, typename T>
	inline void sortByDid(It start, It end){ //assumes that derefernecing an iterator with *it gives a T* that has a did member
			std::sort(start, end, [](const T* a, const T* b){ return a->did < b->did; });
}
	template<typename It, typename T>
	inline void sortByLength(It start, It end){ //assumes that derefernecing an iterator with *it gives a T* that has a did member
			std::sort(start, end, [](const T* a, const T* b){ return a->getPaddedLength() < b->getPaddedLength(); });
}
	template<typename It, typename T> //assumes that derefernecing an iterator with *it gives a T* that has a getMaxScore method
	inline void sortByMaxscore(It start, It end) {
	std::sort(start, end, [](const T* a, const T* b){ return a->getMaxScoreOfList() < b->getMaxScoreOfList(); });
}		
//=============================================================================================
	template<typename It>
	inline did_t getSmallestDid(It start, It end) {
			assert(end-start>0);
		did_t smallest = (*start)->did;
		for(start = start+1; start!=end; ++start)
			if((*start)->did < smallest) smallest = (*start)->did;
		return smallest;
	}
//=============================================================================================
template<int freqOrQuant>
inline unsigned NewIteratorBase::getFreq() 	{
#ifdef USEUNCOMPRESSEDQUANTS
	return rawQuants[compressedBlock*CONSTS::NUMERIC::BS + offsetInPostingSpace];
#endif
	if (fflag == 0)  {
		packDecodeFreqs(freqsBuffer,ptrToFreqs, flags[compressedBlock]>>16,unpacker);
		fflag = 1;
	}

	return freqOrQuant+freqsBuffer[offsetInPostingSpace];
}

//=============================================================================================
template <int useReals>
inline did_t NewIteratorBase::nextGEQ(const did_t d) {				//changes state. returns did s.t. did>=d
	if(d<did) return did; //if we are already greater or equal -- return

	if(useReals)
		if(useRealScores) {
			GLOBALPROFILER.stepCounter(enm::NEXTGEQ_CHEAP);
			//if we use realscores and have realdids set
			// check if we need to skip to a new block of size BS
			const unsigned oldPS = compressedBlock;

			while (d > maxDidPerBlock[compressedBlock+1]) ++compressedBlock;
			offsetInPostingSpace += (compressedBlock-oldPS) ? (compressedBlock-oldPS-1)*CONSTS::NUMERIC::BS : 0;

			while(did<d && offsetInPostingSpace<postingsInWindow) did = realDids[++offsetInPostingSpace];
			//compressedBlock = offsetInPostingSpace/CONSTS::NUMERIC::BS;

			//TODO: remove this when the 64-x lists are properly padded
			if(did>=CONSTS::NUMERIC::MAXD)
					return CONSTS::NUMERIC::MAXD;

			assert(did<CONSTS::NUMERIC::MAXD+128);
			assert(compressedBlock<=paddedLength/CONSTS::NUMERIC::BS);

			return did;
		}

	GLOBALPROFILER.stepCounter(enm::NEXTGEQ);

	//if no realdids:
	if(d>maxDidPerBlock[compressedBlock+1]) {//can't make it in this compressed block
		skipToDidBlockAndDecode(d);
		offsetInPostingSpace=0;
	}

	while(d>did){
		did += didsBuffer[++offsetInPostingSpace]; //prefix sum
		assert(offsetInPostingSpace<=CONSTS::NUMERIC::BS);
	}
	//TODO: remove this when the 64-x lists are properly padded
	if(offsetInPostingSpace>CONSTS::NUMERIC::BS || did>=CONSTS::NUMERIC::MAXD)
		return CONSTS::NUMERIC::MAXD;
	assert(offsetInPostingSpace<=CONSTS::NUMERIC::BS); //TODO: sanity test all lists?
	assert(did<CONSTS::NUMERIC::MAXD+128);
	return did;
}

//=============================================================================================
inline void NewIteratorBase::setDecompressor(CompressionInterface* unpacker) { this->unpacker = unpacker; }
//=============================================================================================
inline unsigned	NewIteratorBase::getPaddedLength() const { return paddedLength; }
//=============================================================================================
inline const unsigned * NewIteratorAugmented::entireBlockDids(did_t* dids, const unsigned block) const {
	//TODO: can't we unite it with skip to block and decode?
	//if(block>=paddedLength/CONSTS::NUMERIC::BS) return nullptr;
	unsigned offset = (sizePerBlock[block]);
	assert(maxDidPerBlock[block]<CONSTS::NUMERIC::MAXD+256); //the first guy is not an elephant
	const unsigned *ptr = packDecodeDids(dids, compressedBase + offset, flags[block]&CONSTS::NUMERIC::maskFlagWithOnes, maxDidPerBlock[block],unpacker); //decode new block
	//prefix sum dids
	for(unsigned i=1; i<CONSTS::NUMERIC::BS; ++i) {
		dids[i]+=dids[i-1]; //dbuf[0] is the real did, not a gap
		//assert(dids[i]<CONSTS::NUMERIC::MAXD+256);
	}
	if(block>=paddedLength/CONSTS::NUMERIC::BS) dids[0]=CONSTS::NUMERIC::MAXD; //TODO
	return ptr;

}
//=============================================================================================
inline void NewIteratorAugmented::batchUnpackingBlocksUptoDid(did_t& postings, did_t nextWindowsFirst, did_t* realDids, unchar* scores, did_t& block, const int instrSet) const {
	while(realDids[postings-1]<nextWindowsFirst && block<paddedLength/CONSTS::NUMERIC::BS) {
		entireBlockDidsAndScores(realDids+postings,scores+postings,block++,instrSet);
		postings+=CONSTS::NUMERIC::BS;
	}
}
//=============================================================================================
inline void NewIteratorAugmented::batchUnpackingBlocksUptoDid(did_t& postings, did_t nextWindowsFirst, did_t* realDids, float* scores, did_t& block, const int instrSet) const {
	while(realDids[postings-1]<nextWindowsFirst && block<paddedLength/CONSTS::NUMERIC::BS) {
		entireBlockDidsAndScores(realDids+postings,scores+postings,block++,instrSet);
		postings+=CONSTS::NUMERIC::BS;
	}
}
//=============================================================================================
inline void NewIteratorAugmented::batchUnpackingBlocks(did_t endBlock, did_t* dids, float* scores, did_t startBlock, const int instrSet) const {
	for(did_t block=startBlock; block<endBlock; ++block)
		entireBlockDidsAndScores(dids + (CONSTS::NUMERIC::BS*block), scores + (CONSTS::NUMERIC::BS*block), block,instrSet);
}
//=============================================================================================
inline void NewIteratorAugmented::entireBlockDidsAndScores(did_t* dids, unchar* scores, const did_t block, const int) const {
	static unsigned freqs[CONSTS::NUMERIC::BS];
	const unsigned *ptr = entireBlockDids(dids,block);

	packDecodeFreqs(freqs,ptr, flags[block]>>16,unpacker);
	//todo: add a version of unpacker that brings unchars...
	for(unsigned i=0; i<CONSTS::NUMERIC::BS; ++i)
		scores[i]=freqs[i];
}
//=============================================================================================
inline void NewIteratorAugmented::entireBlockDidsAndScores(did_t* dids, score_t* scores, const unsigned block, const int instrSet) const { //decode given block and calc scores
	static unsigned freqs[CONSTS::NUMERIC::BS];
	static unsigned lengths[CONSTS::NUMERIC::BS];	
	const unsigned *ptr = entireBlockDids(dids,block);
	packDecodeFreqs(freqs,ptr, flags[block]>>16,unpacker);
	for(unsigned i=0; i<CONSTS::NUMERIC::BS; ++i) {
		assert(dids[i]<CONSTS::NUMERIC::MAXD+256);
		lengths[i]=documentLengthArr[dids[i]]; //build lengths array
	}
	//observe discussion in wiki...
	GLOBALPROFILER.stepCounter(enm::CALC_BLOCK_SCORE);
	if(likely(instrSet==1)) calcScoresForBlockSSE(freqs, lengths, BM25Element, scores );
	else if(unlikely(instrSet==0))
		calcScoresForBlockScalar(freqs, lengths, BM25Element, scores );
}

//=============================================================================================
inline score_t NewIteratorBase::calcOneScoreFloat(const unsigned freq, const unsigned doclen) const { return CALC_SCORE(freq,doclen, BM25Element); 	}
//=============================================================================================
template <int useReals>
inline score_t NewIteratorBase::getScore(score_t) {  //returns score for current did. this is either real score, result of score calc, or dequantization
	if(useReals)
		if(useRealScores) return realScoresFloat[offsetInPostingSpace];

	return calcOneScoreFloat(getFreq<1>(),documentLengthArr[did]);
}
//=============================================================================================
template <int useReals>
inline unchar NewIteratorBase::getScore(unchar) {
	return getFreq<0>();
}
//=============================================================================================
// skip blocks until you come to the right one
inline void NewIteratorBase::skipToDidBlockAndDecode(const did_t d) { //populate the buffers with decompressed data of posting block containing d's nextGEQ
	compressedBlock++; //we enter this only when needed
	while (d > maxDidPerBlock[compressedBlock+1]) 		compressedBlock++;
	//TODO: think, how to remove this check:
	if(compressedBlock>=paddedLength/CONSTS::NUMERIC::BS) {
		did = didsBuffer[0] = CONSTS::NUMERIC::MAXD;
		return;
	}
	//no evidence speeding up when using this:
	//compressedBlock = ((gallop_up_lower_bound(maxDidPerBlock+compressedBlock+1,maxDidPerBlock+paddedLength/CONSTS::NUMERIC::BS, d))-maxDidPerBlock)-1;

	offsetFromBase = (sizePerBlock[compressedBlock]);

	ptrToFreqs = packDecodeDids(didsBuffer, compressedBase + offsetFromBase, flags[compressedBlock]&CONSTS::NUMERIC::maskFlagWithOnes, maxDidPerBlock[compressedBlock],unpacker); //decode new block
	offsetInPostingSpace = 0; //point to first element in that block
	did = didsBuffer[0]; //get the first did IN THE DECODED block
	fflag = 0;			 //indicating that we didn't decompress the freqs yet
}

#ifdef	USEAUGMENTS
//=============================================================================================
#define DBLOCK ((d-firstDidOfWindow)>>didBlockBits)

//because we could have defined it via Makefile and then redefine...
#ifndef USEPOSTINGS
#define USEPOSTINGS
#endif

#ifdef USEPOSTINGS
#define MTHREE 3 // In case we want to use only LA, hardcode MTHREE to 0
#else
#define MTHREE 0
#endif
#define MBITS	(didBlockBits-MTHREE)
#define DMBLOCK ((d-firstDidOfWindow)>>(didBlockBits-MTHREE))
//=============================================================================================
inline void		NewIteratorAugmentedBase::setPolicy(enm::prprPolicy p) { policy = p; }
//=============================================================================================
inline void 		NewIteratorAugmentedBase::setScratchId(const unsigned id) { scratchPadId = id; }
inline unsigned 	NewIteratorAugmentedBase::getScratchId() const { return scratchPadId; }
//=============================================================================================
inline enm::prprPolicy
					NewIteratorAugmentedBase::getPolicy() const {return policy;}			//get the policy
//=============================================================================================
template<int doMemset>
inline void	NewIteratorAugmentedBase::incCurrentWindow() {
	++currentWindow;
	if(doMemset) memset((void*)liveDeadArea,0,CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK);
}

inline did_t   		NewIteratorAugmentedBase::nextLiveArea(const did_t d) {
	assert(d>=firstDidOfWindow);
	//COUTL << firstDidOfWindow+(getNextSetBit(liveDeadArea,DMBLOCK)<<MBITS) << " " << (getNextSetBit(liveDeadArea,DMBLOCK)<<MBITS) << ENDL; //debug
	return std::min(CONSTS::NUMERIC::MAXD,std::max(d,firstDidOfWindow+(getNextSetBit(liveDeadArea,DMBLOCK)<<MBITS)));
}
//=============================================================================================
inline did_t   		NewIteratorAugmentedBase::nextLivePosting(const did_t d) const {
	return std::min(CONSTS::NUMERIC::MAXD,std::max(d,firstDidOfWindow+(getNextSetBit(postingBitSet,DMBLOCK)<<MBITS)));
}
//=============================================================================================
inline score_t 		NewIteratorAugmented::getMaxScoreOfList() const {
	return maxScoreOfList;
}
//=============================================================================================
inline did_t NewIteratorAugmentedBase::getFirstDidOfNextBlock( const did_t docID) const {
	return ( ( (docID>>didBlockBits) + 1) << didBlockBits);
}
//=============================================================================================
inline did_t NewIteratorAugmentedBase::getFirstDidinWindow() const {
	return firstDidOfWindow;
}
//=============================================================================================

class PartialScoreAccumulator {
	float		scoreFloat; // 4 bytes
	unsigned	scoreUnsigned; //4 bytes
	ushort		scoreShort; // 2 bytes
	ushort		pad;    	// 2 bytes
	unchar		scoreByte;  // 1 byte

public:
	PartialScoreAccumulator() : scoreFloat(0.0f), scoreUnsigned(0),scoreShort(0),scoreByte(0){  }
	inline void init(float score) { scoreFloat=0.0f; }
	inline void init(ushort score) { scoreShort=0; }
	inline void init(unchar score) { scoreByte=0; }
	inline void init(unsigned score) { scoreUnsigned=0; }

	inline void get(float& score) const { score=scoreFloat; }
	inline void get(ushort& score) const { score=scoreShort; }
	inline void get(unchar& score) const { score=scoreByte; }
	inline void get(unsigned& score) const { score=scoreUnsigned; }

	inline void add(float score) { scoreFloat+=score; }
	inline void sub(float score) { scoreFloat-=score; }
	inline bool compare(float threshold) { return (Fcompare(scoreFloat, threshold) == 1); }

	inline void add(unchar score) { scoreByte+=score; }
	inline void sub(unchar score) { scoreByte-=score; }
	inline bool compare(unchar threshold) { return scoreByte>=threshold; }

	inline void add(ushort score) { scoreShort+=score; }
	inline void sub(ushort score) { scoreShort-=score; }
	inline bool compare(ushort threshold) { return scoreShort>=threshold; }

	inline void add(unsigned score) { scoreUnsigned+=score; }
	inline void sub(unsigned score) { scoreUnsigned-=score; }
	inline bool compare(unsigned threshold) { return scoreUnsigned>=threshold; }

};
#endif
#endif /* NEWITERATORIMP_H_ */
