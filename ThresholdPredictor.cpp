/*
 * ThresholdPredictor.cpp
 *
 *  Created on: Nov 10, 2012
 *      Author: constantinos
 */

#include "ThresholdPredictor.h"
/*
// Usage: Given a query, for each pair get it's kth highest score and return the max of them
float ThresholdPredictor::MaxPairwise(lptrArray& lps, const unsigned int topk) {
	float threshold = 0.0f;
	if (lps.size()>1) {
		// compute combinations
		for (unsigned int i=0; i<lps.size()-1; i++) {
			for (unsigned int j=i+1; j<lps.size(); j++) {
				// run query on this pair
				lptrArray lpsTmp;
				lpsTmp.push_back(lps[i]);
				lpsTmp.push_back(lps[j]);
				wandwithpartial BMW(pages);
				QpResult res[topk];
				BMW(lpsTmp, topk, res);
				if ( Fcompare(res[topk-1].score, threshold) > 0 )
					threshold = res[topk-1].score;

				// reset list iterators
				lps[i]->reset_list();
				lps[j]->reset_list();
			}
		}
	} else // corner case when 1-term query calls this predictor
		threshold = MaxTopKScore(lps, topk);
	return threshold;
}

// NOTE there is a problem: 547:emergency management kauai 25.167024612426757812500000000000 TODO it returns larger value
// Usage: Store the k highest scores and the dids for each term and return the highest safe threshold estimation
float ThresholdPredictor::TopKScoresANDDids(lptrArray& lps, const unsigned int topk) {
	std::map<unsigned int, float> Results;
	for (int i=0; i<lps.size(); i++) {
		lptrArray lpsTmp;
		lpsTmp.push_back(lps[i]);
		wandwithpartial BMW(pages);
		QpResult res[topk];
		BMW(lpsTmp, topk, res);

//		COUT << "list:" << i << ENDL;
//		for (unsigned int j=0; j<topk; j++)
//			COUTL << res[j].did << "\t" << res[j].score << ENDL;

		for (unsigned int j=0; j<topk; j++) {
			std::map<unsigned int, float>::iterator iter = Results.find(res[j].did);
			if (iter != Results.end()) { // if the did already exists in the map, add its impact score
//				COUT << "already before: " << iter->second << "\t" << iter->first << ENDL;
				Results[res[j].did] = Results[res[j].did] + iter->second;
//				COUT << "already after\t" << Results[res[j].did] << "\t" << res[j].did << ENDL;
			}else { // did doesn't exist
				if (res[j].did < CONSTS::NUMERIC::MAXD && res[j].score > 0.0f) {
					Results.insert( std::pair<unsigned int, float>(res[j].did, res[j].score) );
//					COUT << "new\t" << res[j].score << "\t" << res[j].did << ENDL;
				}

			}
		}
		lps[i]->reset_list();
	}

	std::vector<float> topkscores;

	for (std::map<unsigned int, float>::iterator iter = Results.begin(); iter != Results.end(); ++iter) {
		topkscores.push_back(iter->second);
		//COUT << iter->second << "\t" << iter->first << ENDL;
	}

	std::sort(topkscores.begin(), topkscores.end());
	std::reverse(topkscores.begin(), topkscores.end());

//	COUT << "sorted:" << ENDL;
//	for (unsigned int i=0; i<topkscores.size(); i++)
//		COUTL << topkscores[i] << ENDL;

//	COUTL << topkscores[topk-1];
	if (topkscores.size() > topk-1) return topkscores[topk-1];
	else return 0.0f; // or MaxTopKscore(lps, topk);
}

// Usage: run qp to find the highest top-kth score so as to use it as starting threshold
float ThresholdPredictor::MaxTopKScore(lptrArray& lps, const unsigned int topk) {
	float threshold = 0.0f;
	for (int i=0; i<lps.size(); i++) {
		lptrArray lpsTmp;
		lpsTmp.push_back(lps[i]);
		wandwithpartial BMW(pages);
		QpResult res[topk];
		BMW(lpsTmp, topk, res);
		if ( Fcompare(res[topk-1].score, threshold) > 0 )
			threshold = res[topk-1].score;
		lps[i]->reset_list();
	}
	return threshold;
}

// Safe-Block-Max Sampling
float ThresholdPredictor::SamplingBlocks(lptrArray& lps, const unsigned int samplesize, const unsigned int topk) {
	std::vector<float> estimThreshold;
	float blocksum = 0.0f;
	srand(time(NULL));

	for (unsigned int numTries=0; numTries<samplesize; numTries++) {
		blocksum = 0.0f;
		const unsigned int candDid = (rand() % CONSTS::NUMERIC::MAXD) + 1;
		for (unsigned int numTerms=0; numTerms<lps.size(); numTerms++)
			blocksum += lps[numTerms]->augStructs.blockScoreForDid(candDid);
		if (Fcompare(blocksum, 0.0f) > 0)
			estimThreshold.push_back(blocksum);
	}

	std::sort(estimThreshold.begin(), estimThreshold.end());
	std::reverse(estimThreshold.begin(), estimThreshold.end());

	if (estimThreshold.size() > topk) // include some epsilon for safety ?
		return estimThreshold[topk-1];
	else return 0.0f; //MaxTopKScore(lps, topk);
}

// pseudo Safe-biased to short lists
float ThresholdPredictor::BiasedSamplingBlocks(lptrArray& lps, const unsigned int samplesize, const unsigned int topk) {
	std::vector<float> estimThreshold;
	float blocksum = 0.0f;
	srand(time(NULL));

	// first list with largest score
	lps.sortByReverseScore();

	unsigned int numTries=0;
	while ( numTries<samplesize ) {
		const unsigned int candDid = (rand() % CONSTS::NUMERIC::MAXD) + 1;
		blocksum = lps[0]->augStructs.blockScoreForDid(candDid);
		if (Fcompare( blocksum, 0.0f) > 0) {
			for (unsigned int numTerms=1; numTerms<lps.size(); numTerms++)
				blocksum += lps[numTerms]->augStructs.blockScoreForDid(candDid);
			estimThreshold.push_back(blocksum);
			numTries++;
		}
	}

	std::sort(estimThreshold.begin(), estimThreshold.end());
	std::reverse(estimThreshold.begin(), estimThreshold.end());

	if (estimThreshold.size() > topk + CONSTS::NUMERIC::SAMPLEERROR)
		return estimThreshold[topk-1 + CONSTS::NUMERIC::SAMPLEERROR];
	return 0.0f; //MaxTopKScore(lps, topk);
}

float ThresholdPredictor::SampleANDEvaluateDids(lptrArray& lps, const unsigned int samplesize, const unsigned int topk) {
	float score = 0.0f;
	std::vector<unsigned int> candDids;
	std::vector<float> estimThreshold;
	srand(time(NULL));

	// no check for duplicates
	for (unsigned int i=0; i<samplesize; i++ ) {
		const unsigned int candDid = rand() % CONSTS::NUMERIC::MAXD + 1;
		candDids.push_back(candDid);
	}

	std::sort(candDids.begin(), candDids.end());

	for (unsigned int i=0; i<candDids.size(); i++) {
		const unsigned int candDid = candDids[i];
		score = 0.0f;
		for (unsigned int numTerms = 0; numTerms<lps.size(); numTerms++) {
			lps[numTerms]->did = lps[numTerms]->nextGEQ( candDid );
			if (lps[numTerms]->did == candDid)
				score += lps[numTerms]->getRealScore( pages[candDid] );
		}

		if ( Fcompare(score, 0.0f ) > 0)
			estimThreshold.push_back(score);
	}

	for (unsigned int numTerms = 0; numTerms<lps.size(); numTerms++)
		lps[numTerms]->reset_list();

	std::sort(estimThreshold.begin(), estimThreshold.end());
	std::reverse(estimThreshold.begin(), estimThreshold.end());

	if (estimThreshold.size() > topk)
		return estimThreshold[topk-1];
	return 0.0f; //MaxTopKScore(lps, topk);
}

// Safe
float ThresholdPredictor::BiasedEvaluateDids(lptrArray& lps, const unsigned int evalsize, const unsigned int topk) {
	float score = 0.0f;
	std::vector<unsigned int> candDids;
	std::vector<float> estimThreshold;
	srand(time(NULL));

	// first list with largest score
	lps.sortByReverseScore();

	for (unsigned int i=0; i<evalsize && lps[0]->did < CONSTS::NUMERIC::MAXD; i++) {
		score = 0.0f;
		const unsigned int candDid = lps[0]->did;
		score += lps[0]->getRealScore( pages[candDid] );
		for (unsigned int numTerms = 1; numTerms<lps.size(); numTerms++) {
			lps[numTerms]->did = lps[numTerms]->nextGEQ( candDid );
			if (lps[numTerms]->did == candDid)
				score += lps[numTerms]->getRealScore( pages[candDid] );
		}

		if ( Fcompare(score, 0.0f ) > 0)
			estimThreshold.push_back(score);
	}

	for (unsigned int numTerms = 0; numTerms<lps.size(); numTerms++)
		lps[numTerms]->reset_list();

	std::sort(estimThreshold.begin(), estimThreshold.end());
	std::reverse(estimThreshold.begin(), estimThreshold.end());

	if (estimThreshold.size() > topk)
		return estimThreshold[topk-1];
	return 0.0f; //MaxTopKScore(lps, topk);

}

float ThresholdPredictor::Hybrid(lptrArray& lps, const unsigned int samplesize, const unsigned int topk) {
	float threshold = 0.0f;
	float maxTopk = MaxTopKScore(lps, topk);
	float sampling = SamplingBlocks(lps, samplesize, topk);
	return max3(maxTopk, sampling, threshold);
}
*/
