#include <fstream>

#include "options.h"
#include "Executor.h"
#include "count.h"
#include "Pfor.h"

#include "OR.h"
#include "AND.h"
#include "Augments2.h"


void runShiftTests(/*LexiconInterface *lexN, */indexHandler& index,CompressionInterface* cmprsr);

std::ofstream logFile("logfile.txt", std::ios_base::out |std::ios_base::app);

Options parseCommandLineArgs(int argc, char * argv[]) ;

Registry* makeRegistry() {
	//register classes
	Registry* registry = new Registry();

	//Index/Lexicon styles
	registry->registerClass<SqliteBasedLexicon>();
	registry->registerClass<OldStyleSqliteBasedLexicon>();

	//Compression
	registry->registerClass<OldPforDelta>();
	registry->registerClass<OldPforDeltaWithNewPacker>();

	//OR-Algs
	registry->registerClass<EvaluateDocs>(); //naive
	registry->registerClass<ExhaustiveWindowedOR<unchar,unchar,0> >(); //naive
	//registry->registerClass<ExhaustiveWindowedOR<unchar,score_t,0> >(); //naive
	registry->registerClass<ExhaustiveWindowedOR<score_t,score_t,0> >(); //naive
	registry->registerClass<ExhaustiveWindowedOR<unchar,unchar,1> >(); //integer pipe LB-PB
	registry->registerClass<ExhaustiveWindowedOR<score_t,score_t,1> >(); //use LB-PB

	//AND-Algs
	registry->registerClass<Intersection<0> >(); //naive intersection
	registry->registerClass<Intersection<1> >(); //intersection with augments

	//Augmented structures processors
	registry->registerClass<AugmentsQuantBlockMaxSSE<float> >();
	registry->registerClass<AugmentsQuantBlockMaxSSE<unchar> >();
	registry->registerClass<AugmentsQuantBlockMaxScalar<float> >();
	registry->registerClass<AugmentsForIntersectionSSE>();

	return registry;
}

#include "ShiftList.h"
int mainRunner(int argc, char * argv[] ){
	//globalVerbosity = 5; indexFlatFile("random100.flat",250000,"/tmp/testIndexFromFlat/"); return 0;
//	doAllCounting(); return 0;

	Options options1 =  parseCommandLineArgs(argc, argv);
	globalVerbosity = options1.VERBOSITY;

	if(options1.BUILDINDEX) {
		COUT << "BUILDINDEX is no longer supported in this binary please use standalone builder (make builder)" << ENDL;
		return 0;
	}

	options1.print(LOG);
	if(options1.VERBOSITY>2)
		options1.print(COUT);

	Registry* registry = makeRegistry();

	std::ifstream readIndexConfig(options1.INDEXDIR+"/config");
	if(! readIndexConfig.is_open()) FATAL(options1.INDEXDIR+"/config");
	std::string indexClass;
	std::string compressionClass;
	readIndexConfig >> indexClass >> compressionClass >> CONSTS::NUMERIC::MAXD; //(25205179);
	COUT4 << "MAXD: " <<  CONSTS::NUMERIC::MAXD << ENDL;
	LexiconInterface *lexN = registry->getInstanceOf<LexiconInterface>(indexClass);
	lexN->setPath(options1.INDEXDIR);

	//initiate the index handler
	indexHandler index( options1.INDEXDIR+"/doclen_file",CONSTS::NUMERIC::MAXD,lexN);
	NewIteratorBase::setDocLenPtr(index.getDocLensPtr());

	LOG << getTimeString() << ENDL;
	NewIteratorAugmented::setBits(options1.DIDBLOCKBITS);
	if(options1.IMBUE)
		std::cout.imbue(std::locale(""));

	CompressionInterface* cmprsr = registry->getInstanceOf<CompressionInterface>(compressionClass);
	//runShiftTests(index,cmprsr); return 0;
	QueryTraceManager logManager(options1.QLOG,index);
	if(options1.SHUFFLE) {
		unsigned int seed = time(NULL);
		std::srand(seed);
		COUT2 << "Random seed: " << seed << ENDL;
		logManager.shuffle(); //todo: change to c++11 shuffle!
	}
	logManager.loadTopKResultsFromFile("/data1/team/sergeyn/git/morethan10log",10);

	AugmentsHelperInterface* helper = registry->getInstanceOf<AugmentsHelperInterface>(options1.AUGHELPER);
	Executor qp1(options1,index,cmprsr,logManager,helper);

	if(options1.VERBOSITY>2) {
		//std::cout << "waiting for profiler... Press enter to run" << std::endl;
		//std::getchar();
	}
	QueryProcessorInterface* queryProcessorAlg = registry->getInstanceOf<QueryProcessorInterface>(options1.ALG);
	qp1.run(queryProcessorAlg);
	qp1.printReport();

	return 0;
}

#include <random>
void runShiftTests(/*LexiconInterface *lexN, */indexHandler& index,CompressionInterface* cmprsr) {
	OldStyleSqliteBasedLexicon lex;
	lex.setPath("/data2/BMW/trec06/");
	std::vector<std::string> terms = lex.loadMeta("/data2/BMW/trec06/indx.sqlite");
	for(auto& s : terms) {
		std::vector<std::string> term = {s};
		auto lps = index[term];
		if(lps[0]->getUnpaddedLength()<2048 || lps[0]->getUnpaddedLength() > 2*1024*1024) continue;
		COUTL << term[0] << " length: " << lps[0]->getUnpaddedLength() << " comp. size bytes: "<< lps[0]->getCompressedDataLengthBytes()<< ENDL;
		NewIteratorAugmented& list = NewIteratorAugmented::scratchPads[0];
		unsigned i =0;
		list.setScratchId(0);
		list.setDecompressor(cmprsr);
		list.injectCompressedState(lps[i]->getUnpaddedLength(),
						lps[i]->getMaterializedData(), lps[i]->getSizePerBlockPtr(),
						lps[i]->getMaxDidPerBlockPtr(), lps[i]->getFlagsPtr());
		std::vector<did_t> dids(list.getPaddedLength());
		std::vector<float> scores(list.getPaddedLength());
		list.setUseRealScores(false);
		list.resetPostingIterator();
		list.batchUnpackingBlocks(list.getPaddedLength()/CONSTS::NUMERIC::BS, dids.data(), scores.data(), 0);
		list.resetPostingIterator();
		ShiftList sl(dids);

		/*
		thrashSomeCache(1<<25);
		std::vector<did_t> lookupsV;
		std::default_random_engine rnd;
		std::uniform_int_distribution<int> distribution(1,ShiftList::MAXDID);
		for(unsigned i=0; i<8096; ++i)
			lookupsV.push_back(distribution(rnd));
		std::sort(lookupsV.begin(),lookupsV.end());
		sTimer timer;
		timer.start();
		volatile bool flag = true;
		for(auto l : lookupsV)
			flag &= sl.has(l);
		COUT << timer.end()/1000.0 << "ms " << flag << ENDL;

		thrashSomeCache(1<<25);
		timer.start();
		flag = true;
		for(auto l : lookupsV)
			flag &= (list.nextGEQ<0>(l) == l);
		COUT << timer.end()/1000.0 << "ms " << flag << ENDL;
		*/
	}
}
