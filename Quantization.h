 /*
 * fastSplitter.h
 *
 *  Created on: Feb 3, 2012
 *      Author: sergeyn
 */

#ifndef FASTSPLITTER_H_
#define FASTSPLITTER_H_
#include <vector>
#include <algorithm>
#include <iostream>
#include <cassert>
#include <limits>

/*
// we can do:
1)Heuristic fast algorithm
2)Slow-dp algorithm while sampling data (then it is still fast)
3)Slow-dp algorithm


then we can go for those cost functors:
1)sum-weighter O(1) with preprocessing
2)diff-weighter O(1)
3)avg-weighter - either with abs. error or rmsd (very slow -- hard to preprocess)


then for every block we can use the following selectors:
1)average
2)min
3)max

that makes 27 experiments some of them VERY slow...
*/

// !! end is always the one past last interesting element, just like in std
typedef std::vector<float>::iterator floatVecIt;
template<typename T, typename Iterator>
T sum_range(Iterator begin, Iterator end, T sum=0.0) {
	for(auto it=begin; it!=end; ++it)
		sum += *it;
	return sum;
}
//---------------------------------------------------------------------------------------
class AbsAccumulator {
public:
	virtual ~AbsAccumulator() {}
	virtual void initAccumulator() = 0;
	virtual void push_back(float val) = 0;
	virtual float getAcc() const = 0 ;
};
//---------------------------------------------------------------------------------------
template <typename Iterator>
class AbsWeighter : public AbsAccumulator {
public:
	typedef Iterator IteratorT;
	AbsWeighter() {}
	virtual ~AbsWeighter() {}

	inline virtual void preprocess(IteratorT begin, IteratorT end) {}
	virtual void initAccumulator() {throw 1;  }
	virtual void push_back(float val) {throw 1;  }
	virtual float getAcc() const {throw 1;  }

	virtual float operator()(Iterator begin, Iterator end) const =0;

};
//---------------------------------------------------------------------------------------
class AvgAccumulator : public AbsAccumulator {
protected:
	float accSum;
	unsigned int accSize;
public:
	AvgAccumulator() { initAccumulator(); }
	void initAccumulator() { accSum = 0.0; accSize = 0; }
	void push_back(float val) { accSum += val; ++accSize; }
	float getAcc() const { return accSum/accSize; }
};
//---------------------------------------------------------------------------------------
template <typename Iterator>
class AvgSelector : public  AvgAccumulator {
public:
	AvgAccumulator avg;
	AvgSelector() {}
	float operator()(Iterator begin, Iterator end) {
		float sum = sum_range(begin,end,0.0f);
		return sum/(end-begin);
	}
};
//---------------------------------------------------------------------------------------
template <typename Iterator>
class DiffSelector {
public:
	inline float operator()(Iterator begin, Iterator end) { return (*(end-1)) - (*begin); }
};
//---------------------------------------------------------------------------------------
template <typename Iterator>
class CheapMeanSelector {
public:
	inline float operator()(Iterator begin, Iterator end) { return 0.5 * ((*(end-1)) + (*begin)); }
};
//---------------------------------------------------------------------------------------
template <typename Iterator>
class MiddleSelector {
public:
	inline float operator()(Iterator begin, Iterator end) { return *((end+begin)/2); }
};
//---------------------------------------------------------------------------------------
template <class Selector>
class TriangleWeighter : public AbsWeighter<floatVecIt> {
public:
	virtual float operator()(floatVecIt begin, floatVecIt end) const {
		Selector select;
		const float representative = select(begin,end); //better be O(1)
		return 0.5*representative*(end-begin); //build a trinagle
	}
};
//---------------------------------------------------------------------------------------
class DummyWeighter : public AbsWeighter<floatVecIt> {
public:
	virtual float operator()(floatVecIt begin, floatVecIt end) const {return 1234567890.0;}
};
//---------------------------------------------------------------------------------------
template <class Selector>
class TieWeighter : public AbsWeighter<typename Selector::IteratorT> {
public:
	typedef typename Selector::IteratorT  IteratorT ;
	virtual float operator()(IteratorT begin, IteratorT end) const {
		if(begin==end) return 123456789.0f;
		Selector select;
		const float representative = select(begin,end); //better be O(1)
		const float hi = *(end-1);
		unsigned int mid = (begin+((end-begin)/2))-begin;
		return ((representative*mid)+ (mid*(hi-representative)) )/2.0; //build 2 trinagles
	}
};
//---------------------------------------------------------------------------------------
class SumWeighter : public AbsWeighter<floatVecIt> {
	std::vector<float> prefixSum;
	IteratorT b;
public:
	virtual ~SumWeighter() {}
	void preprocess(IteratorT begin, IteratorT end) {
		b=begin;
		prefixSum.clear();
		prefixSum.resize(end-begin);
		prefixSum.push_back(0.0f);
		std::partial_sum(begin,end,prefixSum.begin()+1);
	}

	virtual inline float operator()(floatVecIt begin, floatVecIt end) const {
		return (prefixSum[end-b] - prefixSum[begin-b]) - ( (*begin)*(end-begin));
	}
};
//---------------------------------------------------------------------------------------
class TieSumWeighter : public SumWeighter {
public:
	inline float operator()(floatVecIt begin, floatVecIt end) const {
		floatVecIt mid  = begin+((end-begin)/2);
		return SumWeighter::operator ()(begin,mid) + SumWeighter::operator ()(mid,end);
	}
};
//---------------------------------------------------------------------------------------
template <typename Iterator>
class MinSelector {
public:
	float operator()(Iterator begin, Iterator end) { return *begin; }
};
//---------------------------------------------------------------------------------------
template <typename Iterator>
class MaxSelector {
public:
	typedef Iterator IteratorT;
	float operator()(Iterator begin, Iterator end) { return *(end-1); }
};
//---------------------------------------------------------------------------------------
template <class Selector>
class RmsdWeighter : public AbsWeighter<floatVecIt> {
public:
	virtual float operator()(floatVecIt begin, floatVecIt end) const {
		Selector select;
		const float representative = select(begin,end);
		float sum=0.0;
		for(floatVecIt it=begin; it!=end; ++it)
			sum += (representative - (*it))*(representative - (*it));
		return sum;
	}
};
//---------------------------------------------------------------------------------------
template <class Selector>
class ErrWeighter : public AbsWeighter<floatVecIt> {
public:
	virtual float operator()(floatVecIt begin, floatVecIt end) const {
		Selector select;
		const float representative = select(begin,end);
		float sum=0.0;
		for(floatVecIt it=begin; it!=end; ++it)
			sum += std::abs(representative - (*it));
		return sum;
	}

	virtual void initAccumulator() {throw 1;  } //this class doesn't support accumulation
};
//---------------------------------------------------------------------------------------
template <typename Weighter>
class ScoresBlock {
private:
	Weighter* weighter;
	ScoresBlock() {}
public:
	typename Weighter::IteratorT begin;
	typename Weighter::IteratorT end; //one past the last
	float representative;
	explicit ScoresBlock(float r) : representative(r){}
	ScoresBlock(Weighter* w, typename  Weighter::IteratorT b, typename  Weighter::IteratorT e) : weighter(w), begin(b), end(e){
		assert(*begin);
		representative=(0.0);
	}

	void print() { std::cout << "[" << *begin << "," << *(end-1) << "]" << std::endl;}
	void print(int) {
		std::cout << "[" ;
		for(typename Weighter::IteratorT it=begin; it!=end; ++it)
			std:: cout << *it << ",";
		std::cout <<	"]";
		std::cout << (*weighter)(begin,end) << " " << representative << std::endl;
	}
	inline float weight() const {return (*weighter)(begin,end); }
	inline bool operator< (const ScoresBlock& rhs) const { return weight() < rhs.weight(); }
	inline bool operator< (const float value) const {return (*(end-1) < value) ; }
	inline size_t size() const { return end-begin; }

	class minVComparator { public:
		inline bool operator()(const ScoresBlock<Weighter>& lhs, const ScoresBlock<Weighter>& rhs) const {
			return *(lhs.begin) < *(rhs.begin);
		}
	};
	class maxVComparator { public:
		inline bool operator()(const ScoresBlock<Weighter>& lhs, const ScoresBlock<Weighter>& rhs) const {
			return *(lhs.end-1) < *(rhs.end-1);
		}
	};

};
//---------------------------------------------------------------------------------------
template <typename Weighter>
class AbstractSplitter {
public:
	typedef ScoresBlock<Weighter> blockt;
	typedef typename Weighter::IteratorT ItT;
	Weighter w;
	AbstractSplitter() {}
	virtual ~AbstractSplitter() {}

	const std::vector<blockt>& getBlocks() const { return blocksv;}
	void orderBlocks() {
		typename ScoresBlock<Weighter>::maxVComparator cmp;
		std::sort(blocksv.begin(), blocksv.end(), cmp);
	}

	template <typename Selector>
	void setRepresentative(Selector s) {
		for(auto& b : blocksv)
			b.representative = s(b.begin,b.end);
	}

	virtual void writeLookupTable(float* ptr) const {
		unsigned i = 0;
		for(auto& b : blocksv) //assume already sorted blocks
			ptr[(i++)]=b.representative;
	}

	void print() const {
		for(auto b: blocksv)
			b.print();
	}
	size_t size() const { return blocksv.size(); }

	float sumOfBlocks(size_t start, size_t end) const {
		float sum = 0.0;
		for(size_t i=start; i<end; ++i)
			sum += blocksv[i].weight();
		return sum;
	}

	ItT splitOptimal(blockt& b) const  {
		//* when time of weighting is not O(1) this becomes expensive...
		if(b.size() < 2)
			return b.begin;
		else if(b.size() == 2)
			return b.begin+1;

		float minW = std::numeric_limits<float>::max();
		ItT minIt;
		float tmp;
		for(ItT it = b.begin+1; it != b.end-1; ++it) //*
			if(minW > (tmp = w(b.begin,it)+w(it+1,b.end)) ) {
				minW = tmp;
				minIt = it;
			}
		return minIt;
	}

	inline virtual unsigned int make(float value) const {
		auto it = std::lower_bound(blocksv.begin(),blocksv.end(),value);
		return it-blocksv.begin();
	}

	inline virtual  float operator[](unsigned int id) const { return blocksv[id].representative;}

	void clear() {blocksv.clear(); }
	virtual void run(ItT begin, ItT end, const size_t splitters) = 0;

protected:
	void specialCase(ItT begin, ItT end) {
		std::vector<blockt>& blocks = this->blocksv;
		blocks.clear(); //the splitter is being reused by the runner
		blocks.reserve(256);
		Weighter* wieghter = &(this->w);
		for(ItT it =  begin; it!=end; ++it)
			blocks.push_back(blockt(wieghter,it,it+1));
		blocks.resize(256,blockt(wieghter,end,end));
	}

	std::vector<blockt> blocksv;
};

//---------------------------------------------------------------------------------------
template<typename Weighter>
class FastSplitter : public AbstractSplitter<Weighter> {
public:
	typedef typename Weighter::IteratorT ItT;
	typedef ScoresBlock<Weighter> blockt;
	FastSplitter() {}
	void run(ItT begin, ItT end, const size_t splitters)  {
		if(splitters>=end-begin) {
			AbstractSplitter<Weighter>::specialCase(begin,end);
			return;
		}
		Weighter* wieghter = &(this->w);
		wieghter->preprocess(begin,end); //often does nothing

		std::vector<blockt> evicted;
		std::vector<blockt>& blocks = this->blocksv;
		blocks.clear(); //the splitter is being reused by the runner
		blocks.reserve(splitters+1);
		blocks.push_back(blockt(wieghter,begin,end));
		while(blocks.size()+evicted.size() < splitters+1) {
			blockt b = blocks.front();
			pop_heap(blocks.begin(), blocks.end());
			blocks.pop_back();

			auto splitter = AbstractSplitter<Weighter>::splitOptimal(b);
			if(splitter == b.begin){ //no more splitting possible
				evicted.push_back(b);
				continue;
			}
			assert(*splitter);
			blocks.push_back(blockt(wieghter,b.begin,splitter));
			push_heap(blocks.begin(), blocks.end());
			blocks.push_back(blockt(wieghter,splitter,b.end));
			push_heap(blocks.begin(), blocks.end());

		}
		for(auto& b : evicted)
			blocks.push_back(b);

		assert(blocks.size() == splitters+1);
	}
private:

};
//---------------------------------------------------------------------------------------
class NaiveLinearSplitter : public AbstractSplitter<DummyWeighter> {
public:
	typedef ScoresBlock<DummyWeighter> blockt;

	void run(ItT begin, ItT end, const size_t splitters)  {
		float low = *begin;
		float hi = *(end-1);
		float add = (hi-low)/(splitters+1);
		values.reserve(splitters+1);
		for(size_t i=0; i<splitters+1; ++i)
			values.push_back(i*add);
	}

	inline virtual unsigned int make(float value) const {
			auto it = std::lower_bound(values.begin(),values.end(),value);
			return it-values.begin();
		}

	inline virtual  float operator[](unsigned int id) const { return values[id];}
private:
	std::vector<float> values;
};
//---------------------------------------------------------------------------------------
class NaiveExpSplitter : public AbstractSplitter<DummyWeighter> {
public:
	typedef ScoresBlock<DummyWeighter> blockt;

	void run(ItT begin, ItT end, const size_t splitters)  {
		low = *begin;
		float hi = std::numeric_limits<float>::epsilon( ) + *(end-1);
		base = powf(hi/low, 0.00390625);
		const float log10 = log10f(10.0);
		logbase = log10f(base)/log10;

		values.reserve(256);
		for(unsigned i=0; i<256; ++i)
			values.push_back(low*powf(base,i+0.5));
	}

	virtual void writeLookupTable(float* ptr) const {
		unsigned i = 0;
		for(auto& b : values)
			ptr[(i++)]=b;
	}

	inline float logB(float score) const { return log10f(score)/logbase; }
	inline virtual unsigned int make(float value) const {
			return std::min(255,(int)floorf(logB(value/low)));
		}

	inline virtual  float operator[](const unsigned int id) const {return values[id];}
private:
	float low;
	float base;
	float logbase;
	float quantile;
	std::vector<float> values;

};
//---------------------------------------------------------------------------------------
class LinearSplitter : public AbstractSplitter<DummyWeighter> {
public:
	typedef ScoresBlock<DummyWeighter> blockt;

	void run(ItT begin, ItT end, const size_t splitters)  {
		if(splitters>=end-begin) {
			specialCase(begin,end);
			return;
		}
		std::vector<blockt>& blocks = this->blocksv;
		DummyWeighter* wieghter = &(this->w);
		size_t bucketSize = (end-begin)/(splitters+1);
		for(size_t i=0; i<splitters; ++i)
			blocks.push_back(blockt(wieghter,begin+(bucketSize*i),begin+(bucketSize*(1+i))));
		blocks.push_back(blockt(wieghter,begin+(bucketSize*splitters),end));
		assert(blocks.size() == splitters+1);
	}

};
//---------------------------------------------------------------------------------------
/*
  scores list: 8,6,5,7

  DP:

       3| 32 29 27 26
	   2| 24 20 19  #
	   1| 16 14  #  #
       0|  8  #  #  #
	    +-------------
	   #|  0  1  2  3

	        splitters = columns
	        right edge of scores = rows

	The first column is simply the weight of single block of 1,2,3,4 postings
	The diagonal [i][i] is the prefix sum of the scores list -- having block for each score

	Every other cell [splits][items] is calculated by getting min of:
	  0)	[s-1][i-1] + Weight(scores[i])
	  1) 	[s-1][i-2] + Weight(scores[i],scores[i-1])
	  ...
	  t)    [s-1][i-t] + Weight(scores[i],scores[i-1], ... , scores[i-t])

	  while (i-t>s-1)

	  (!) Note, that Weight can be calculated in O(1) using the slider class that keeps track of max and count
 */
template<typename Weighter>
class DPSplitter: public AbstractSplitter<Weighter>  {
	size_t length;
	float* scores;
	float** table;

	class slider {
		float acc;
	public:
		slider() : acc(0.0) {}
		inline float push(float score) { return (acc = acc+score);}
	};

public:
	typedef typename Weighter::IteratorT ItT;
	typedef ScoresBlock<Weighter> blockt;

	//splitters is the number of splitters we will have (blocksCount-1)
	void run(ItT begin, ItT end, const size_t splitters)  {
		assert(splitters<end-begin);
		Weighter* wieghter = &(this->w);
		wieghter->preprocess(begin,end); //often does nothing

		const size_t blocksCount = splitters+1;
		scores = &(*begin);

		//create the table
		float** dptable = new float*[blocksCount+1];
		table = dptable;
		for(size_t i=0; i<blocksCount; ++i) {
			dptable[i] = new float[length+1];
		}

		slider slide;
		for(size_t items = 0; items<length; ++items) //init first column
			dptable[0][items] = slide.push(scores[items]);

		for(size_t i=1; i<blocksCount; ++i) //init diagonals
			dptable[i][i]=dptable[i-1][i-1]+scores[i];

		//main loop
		for(size_t splits=1; splits<blocksCount; ++splits) {
			for(size_t items = splits+1; items<length; ++items) { //can't have less items than splitters
				slider slide;
				float min = dptable[splits-1][items-1] + slide.push(scores[items]); //one option -- we open new block - init with it:

				//find optimal
				size_t subp = items-2;
				while(1) {
					float m = dptable[splits-1][subp] + slide.push(scores[subp+1]);
					if(min > m)
						min = m;

					if(subp == 0 || subp-1<splits-1 )
						break;
					--subp;
				} //end for min

				dptable[splits][items] = min;
			} //end for items
		} //end for splitters
	} //end
};
//---------------------------------------------------------------------------------------
template <typename Selector, typename Splitter>
class Quant {
	Splitter spltr;
public:
	typedef typename Splitter::ItT ItT;
	void operator()(ItT begin, ItT end, unsigned int buckets=256) {
		spltr.run(begin,end,buckets-1);

		//assert(spltr.getBlocks().size()==buckets);
		if(end-begin>buckets-1)
			spltr.orderBlocks();
		Selector s;
		spltr.setRepresentative(s);
		//spltr.print();
	}
	void writeLookupTable(float* ptr) const { spltr.writeLookupTable(ptr); }
	inline float operator[](unsigned int id) const { return spltr[id]; }
	inline unsigned int make(float value) const { return spltr.make(value); }
	void clear() { spltr.clear(); }
};
//---------------------------------------------------------------------------------------
#endif /* FASTSPLITTER_H_ */
