#ifndef EXECUTOR_H_
#define EXECUTOR_H_

#include "globals.h"
#include "profiling.h"
#include "options.h"
#include "IndexHandler.h"
#include "NewIterator.h"
#include "OR.h"


class QueryTraceManager {
	std::vector<std::vector<std::string> > queriesD;
	strToFloatMap mapQueryToScore;
	hashMapStd<std::string,std::vector<QpResult<float> > > mapQueryToTopResultsVector;
	indexHandler& index;

public:

	class queriesFileIterator : public std::iterator<std::forward_iterator_tag, std::vector<std::string> > 	{
		const QueryTraceManager& queriesP;
		int curr;
		unsigned count;
		unsigned limit;
		unsigned exactlyKTermsBucket;
		unsigned moreThanKTerms;

		bool isSkip() const;
	public:
	  queriesFileIterator(const QueryTraceManager& lm, int cur=-1) :queriesP(lm), curr(cur), count(0), limit(0), exactlyKTermsBucket(0), moreThanKTerms(0) {}
	  queriesFileIterator(const queriesFileIterator& it) : queriesP(it.queriesP), curr(it.curr){}
	  bool operator==(const queriesFileIterator& rhs) {return curr==rhs.curr;}
	  bool operator!=(const queriesFileIterator& rhs) {return curr!=rhs.curr;}
	  const std::vector<std::string>& operator*() { assert(queriesP[curr].size()); return queriesP[curr];}
	  float score() const { return queriesP.score(queriesP[curr]); }
	  void changeSkipPolicy(unsigned lim, unsigned ktbuck, unsigned mtkbuck) { limit=lim; exactlyKTermsBucket=ktbuck; moreThanKTerms=mtkbuck;}
	  queriesFileIterator& operator++();
	};

	QueryTraceManager(const std::string& fname, indexHandler& index); //will add more loaders in future?
	void shuffle();
	queriesFileIterator getIterator() { return queriesFileIterator(*this); }
	void setScoreForQuery(const std::vector<std::string>& terms, float v);
	size_t size() const { return queriesD.size(); }
	const queriesFileIterator end() { return queriesFileIterator(*this,queriesD.size()); }
	const std::vector<std::string>& operator[] (size_t q) const { return queriesD[q]; }
	float score(const std::vector<std::string>& terms) const;
	const std::vector<QpResult<float> >& topKDids(const std::vector<std::string>& terms) const;
	void loadScoresFromFile(const char* fname);
	void loadTopKResultsFromFile(const std::string& fname, unsigned topk=CONSTS::NUMERIC::TOPK);
};
//=============================================================================================
class PreprocessingOracleManager { //aka womanly shape oracle
	hashMapStd<std::string,enm::prprPolicy> storedPolicies;
	hashMapStd<std::string,PreprocessingData> storedPreprocessedAugments;
	CompressionInterface* unpacker;
public:
	PreprocessingOracleManager(CompressionInterface* unpckr) : unpacker(unpckr) {}
	void setStaticPolicy(const ListMetaData& list, const unsigned ontheflystopat, const unsigned bmqcstop);
	void loadPoliciesFromFile(const std::string& fname);
	void preloadAndStoreAccordingToPolicy(std::vector<const ListMetaData*>& lists, AugmentsHelperInterface*);
	enm::prprPolicy suggestPolicy(const std::string& term);
	PreprocessingData& getTheGoodStuff(const std::string& term);
};
//=============================================================================================
class Executor {
	Options& options;
	profilerC& profiler;
	indexHandler& index;
	//std::string queryLog;
	PreprocessingOracleManager womanly;
	CompressionInterface* unpacker;
	QueryTraceManager& logManager;
	AugmentsHelperInterface* augmentsHelper;

	void runAlgorithm(QueryProcessorInterface* queryProcessorAlg, AugmentsHelperInterface* helper, std::vector<std::string>& terms, std::vector<NewIteratorAugmented*>& lists, QpResult<float>* resArr, score_t threshold=0.0);
public:
	Executor(Options& opts, indexHandler& indexH, CompressionInterface* unpckr, QueryTraceManager& qlog, AugmentsHelperInterface* helper);
	void run(QueryProcessorInterface* queryProcessorAlg);
	void printReport() const;
};



#endif /* EXECUTOR_H_ */
