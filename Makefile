DIRS    := indexBuilder indexBuilder/..
SOURCES := $(foreach dir, $(DIRS), $(wildcard $(dir)/*.cpp))
#SOURCES := $(wildcard *.cpp) #override
OBJS    := $(patsubst %.cpp, %.o, $(SOURCES))
OBJS    := $(foreach o,$(OBJS),./obj/$(o))
DEPFILES:= $(patsubst %.o, %.P, $(OBJS))

HOST=$(shell hostname)

#Add to older eclipse versions:  __GXX_EXPERIMENTAL_CXX0X__ and __GXX_EXPERIMENTAL_CXX0X__
#Since Kepler: Project Propertirs -> c++ general -> preprocessor include -> providers -> gcc built-int settings change to: ${COMMAND} -E -P -v -dD "${INPUTS}" -std=c++11
STD=-std=c++11 -D CPP0X
#STD=-std=c++0x -D CPP0X

DBG=$(STD) -g3 -Wall -D DEBUG
TIMING=$(STD) -O3 -D TIMING -D NDEBUG 
COUNTING=$(TIMING) -D COUNTING 
#PROF=$(STD) -O3 -pg -D GPROFILING -D NDEBUG 
RLS=$(STD) -O3 -D NDEBUG

#-flto
CFLAGSOPT = $(COUNTING) -Wfatal-errors -Wall -c -fno-rtti -Wno-sign-compare -MMD -march=native 
CFLAGSDBG = $(STD) -g -D DEBUG -Wall -c  -MMD  -march=native

#-flto
LFLAGSOPT = -O3 
LFLAGSDBG = -g -D DEBUG

mkdebug ?= 0
ifeq ($(mkdebug),0)
	CFLAGS = $(CFLAGSOPT)
	LFLAGS = $(LFLAGSOPT)
else
	CFLAGS = $(CFLAGSDBG)
	LFLAGS = $(LFLAGSDBG)
endif

mk32 ?= 0
ifeq ($(mk32),1)
	CCFLAGS = $(CFLAGS) -m32
	LLFLAGS = $(LFLAGS) -m32
else
	CCFLAGS = $(CFLAGS) 
	LLFLAGS = $(LFLAGS) 
endif

  COMPILER = g++
ifeq ($(HOST),dodo)	
  COMPILER = g++-4.7 
endif

.SILENT:

#link the executable
QPBIN := qp
$(QPBIN): $(OBJS) sql/sqlite3.o 
	$(COMPILER) $(CCFLAGS) -o obj/main.o main.cpp
	$(COMPILER) $(LLFLAGS) $(OBJS) sql/sqlite3.o -o $(QPBIN) -lpthread
	echo "Run '$(QPBIN)'"
	echo "---- FIN ----"

all: $(QPBIN)
	echo ""
	
options.h: options.cfg create_options_h.pl
	echo 'Rebuild options' 
	perl create_options_h.pl > options.h

sql/sqlite3.o: 
	echo 'Rebuilding sqlite. This will take some time...'
	gunzip -q sql/*.gz | wc > /dev/null
	gcc -c -O3 -DSQLITE_THREADSAFE=2 -DSQLITE_OMIT_LOAD_EXTENSION=1 sql/sqlite3.c -o sql/sqlite3.o
	gzip sql/*.c

Enums.cpp: Enums.h enumBind.rb
	ruby enumBind.rb > Enums.cpp	
	
#generate dependency information and compile
obj/%.o : %.cpp options.h
	@mkdir -p $(@D)
	echo $@
	$(COMPILER) $(CCFLAGS) -o $@ -MF obj/$*.P $<
	@sed -e 's/#.*//' -e 's/^[^:]*: *//' -e 's/ *\\$$//' \
	-e '/^$$/ d' -e 's/$$/ :/' < obj/$*.P >> obj/$*.P;
 
#remove all generated files
clean:
	rm -rf qp obj/* core options.h

#make standalone builder
BLDBIN := builder
$(BLDBIN): $(OBJS) sql/sqlite3.o 
	$(COMPILER) $(CCFLAGS) -o obj/main.o -DBUILDER main.cpp
	$(COMPILER) $(LLFLAGS) $(OBJS) sql/sqlite3.o -o $(BLDBIN) -lpthread
	echo "---- Builder ready: run '$(BLDBIN)' ----"
#include the dependency information
-include $(DEPFILES)
