#ifndef SCALAR_H_
#define SCALAR_H_

#include "types.h"
#include "globals.h"
#include "utils.h"

//we probably, as T. wants need to round above the scores and round down the thresholds...

inline void quantizeWithGivenQuantileScalar(const float* src, const unsigned sz, unchar* dst) {
	assert(sz%8==0);
	for(unsigned i=0; i<sz; i+=8) { //a bit of manual unrolling just for fun
		dst[i]=quantF_(src[i]); dst[i+1]=quantF_(src[i+1]); dst[i+2]=quantF_(src[i+2]); dst[i+3]=quantF_(src[i+3]);
		dst[i+4]=quantF_(src[i+4]); dst[i+5]=quantF_(src[i+5]); dst[i+6]=quantF_(src[i+6]); dst[i+7]=quantF_(src[i+7]);
	}
}

#define CALC_SCORE(freq,lendid,pre) pre * (float)(freq) / ((float)(freq + 2 * ( 0.25 + 0.75 * (float)(lendid)/(float)(130) ) ) )
inline void calcScoresForBlockScalar(const unsigned* freqs, const unsigned* lengths, float BM25Element, float* scores ) {
	for(unsigned i=0; i<CONSTS::NUMERIC::BS; ++i)
		scores[i] = CALC_SCORE(freqs[i]+1,lengths[i], BM25Element);
}

void buildLiveAreaQuantScalar_(unchar* ptr, unchar** blockScores, unchar** bitSetsPBS, const unchar threshold, const unsigned termsCount, const unsigned windowsize);
void buildPBS_M3_Scalar_(unsigned* dest, const unsigned* dids, const unsigned winStartDid, unsigned bits, const unsigned sz);

/*quantizeMaxes(const float* scores, const unsigned sz,  PreprocessingData& prData, const float maxScoreOfList){
	float large = (maxScoreOfList>0) ? maxScoreOfList : (*std::max_element(scores,scores+sz));
	score_t quantile = ((score_t) large)/((score_t)CONSTS::NUMERIC::QUANTIZATION);
	const unsigned size = sz;
	const unsigned padSize = size%CONSTS::NUMERIC::BS ? ((size>>6)+1)<<6 : size;

	prData.quantile =quantile;
	unsigned char* ptr = NEWM unsigned char[padSize];
	prData.quantBlockMaxes = ptr;
	unsigned i=0;
	for(unsigned j=0; j<size; ++j) {
		float temp = (scores[j]/quantile);
		unsigned int quantized_value = (unsigned int) temp;
		float remainder = temp - quantized_value;
		ptr[i++] = ( (Fcompare(remainder, 0.0f) != 0)&&(quantized_value!=CONSTS::NUMERIC::QUANTIZATION) ) ?
				(unsigned char) quantized_value + 1 : (unsigned char) quantized_value;
	}
	//for(unsigned i=0; i<size; ++i) dest[i]=quantVals[i]*quantile; -- scalar dequant
}*/

#endif /* SCALAR_H_ */
