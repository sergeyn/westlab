#ifndef ORIMP_H_
#define ORIMP_H_

#include <sstream>

inline float aux__convertResultToScore(const float res, const float) { return res; }
inline unchar aux__convertResultToScore(const unchar res, const unchar) { return res; }

inline unchar aux__convertResultToScore(const float res, const unchar) { return quantF_(res); }
inline float aux__convertResultToScore(const unchar res, const float) { return dequantF_(res); }

inline unchar aux__convertResultToBlockMax(const unchar res) { return res; }
inline unchar aux__convertResultToBlockMax(const float res) { return quantF_(res); }

inline void aux__convertResultArrays(QpResult<float>* src, QpResult<float>* dst, unsigned sz) {
	std::copy(src,src+sz,dst);
}

inline void aux__convertResultArrays(QpResult<unchar>* src, QpResult<float>* dst, unsigned sz) {
	for(unsigned i=0; i<sz; ++i) {
		dst[i].did = src[i].did;
		dst[i].score=aux__convertResultToScore(src[i].score,dst[i].score);
	}
}


template <typename scoreType, typename resultType, int UseAugments>
void ExhaustiveWindowedOR<scoreType,resultType, UseAugments>::setup(std::vector<NewIteratorAugmented*>& lps, Options opts, AugmentsHelperInterface* augments, const float thresh){
	lists=(lps);
	threshold=aux__convertResultToScore(thresh,threshold); //make correct conversion from float threshold to your scoreType
	//COUTV(5)<< "Setting threshold to: ~" << (unsigned)threshold << ENDL;
	options=(opts);
	augmentsHelper=(augments);
}

template <typename scoreType, typename resultType, int UseAugments>
const std::string  ExhaustiveWindowedOR<scoreType,resultType, UseAugments>::name() const {
	std::stringstream str;
	str << "ExhaustiveWindowedOR<" << aux__nameForType(scoreType()) << "," <<  aux__nameForType(resultType()) <<  ","  << UseAugments << ">";
	return  str.str();
}

template <typename scoreType, typename resultType, int UseAugments>
void* ExhaustiveWindowedOR<scoreType,resultType, UseAugments>::getOne() const { return new ExhaustiveWindowedOR<scoreType,resultType, UseAugments>(); }

template <typename scoreType, typename resultType, int UseAugments>
void ExhaustiveWindowedOR<scoreType,resultType, UseAugments>::putResults(void* res) {
	QpResult<float>* arr = reinterpret_cast<QpResult<float>*>(res);
	std::vector<QpResult<resultType> > src(resultsHeap.getV().size()); //get a copy of a heap
	resultsHeap.putInArray(src.data());
	aux__convertResultArrays(src.data(),arr,src.size());
}

template <typename scoreType, typename resultType, int UseAugments>
void ExhaustiveWindowedOR<scoreType,resultType, UseAugments>::run(const unsigned topK) {
	const unsigned windows = (CONSTS::NUMERIC::MAXD>>options.DIDBLOCKBITS)/CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK;
	resultsHeap.setSizeLimit(topK); //init results heap

	NewIteratorAugmented* listsA[MAX_TERMS_IN_QUERY];
	const unsigned termsCnt = this->lists.size();
	for(unsigned i=0; i<termsCnt; ++i) listsA[i]=this->lists[i];

	sortByDid<ITERATOR_VEC_PTR,NewIteratorAugmented>( listsA+0, listsA+termsCnt);

	did_t smallestCandidateDid = 0;
	did_t nextSmallestDid = CONSTS::NUMERIC::MAXD;
	did_t nextLiveDid = 0;

	did_t hitsCount = 0;
	PartialScoreAccumulator scoreAcc;
	scoreType curThreshold = threshold;

	// foreach window
	for (unsigned winCnt=0; winCnt<=windows; ++winCnt) {
		//Boo-boo note: the fastex is broken if no USEREAL
		if(UseAugments) {
			for(unsigned i=0; i<termsCnt; ++i)
				listsA[i]->preprocessAugmentsForWindow(augmentsHelper);
			augmentsHelper->buildLivenessBitvector(reinterpret_cast<unchar*>(NewIteratorAugmented::liveDeadArea),
					reinterpret_cast<void**>(NewIteratorAugmented::blockScoresQuants),NewIteratorAugmented::bitSetsPBS,
					aux__convertResultToBlockMax(curThreshold),termsCnt,CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK);

			//COUT << winCnt << "# "<< countSetBits(NewIteratorAugmented::liveDeadArea,CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK) << ENDL;
		}
		// compute next window bound
		const did_t nextWinStart = std::min(CONSTS::NUMERIC::MAXD,((1+winCnt)*CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK) << options.DIDBLOCKBITS);

		// move lists, sort them and set the smallest did
		if(UseAugments)
			nextLiveDid = (listsA[0]->getFirstDidinWindow() == nextWinStart) ? listsA[0]->getFirstDidinWindow() : NewIteratorAugmented::nextLiveArea( listsA[0]->getFirstDidinWindow() );
		else
			nextLiveDid = listsA[0]->getFirstDidinWindow();

		nextSmallestDid = CONSTS::NUMERIC::MAXD;
		for (unsigned i=0; i<termsCnt; i++) {
			listsA[i]->did = listsA[i]->nextGEQ<UseAugments>( nextLiveDid );
			nextSmallestDid = std::min(nextSmallestDid, listsA[i]->did);
		}
		smallestCandidateDid = nextSmallestDid;
		// while candidate did in the range of the current window, try to evaluate it using ET methods
		while (smallestCandidateDid<nextWinStart) {
			scoreAcc.init(threshold); //zero partial sum

			if(UseAugments)
				nextLiveDid = (smallestCandidateDid+1 == nextWinStart) ? smallestCandidateDid + 1 : NewIteratorAugmented::nextLiveArea( smallestCandidateDid + 1 );
			else
				nextLiveDid = (smallestCandidateDid+1);

			nextSmallestDid = listsA[0]->did;
/*
			// evaluate aligned documents
			for (unsigned i=0; i<termsCnt; i++) {
				if ( listsA[i]->did == smallestCandidateDid ) {
					scoreAcc.add(listsA[i]->getScore<UseAugments>(curThreshold));
					//profilerC::getInstance().stepCounter(enm::EVALUATION);
					listsA[i]->did = listsA[i]->nextGEQ<UseAugments>( nextLiveDid );
				}
				nextSmallestDid = std::min(nextSmallestDid, listsA[i]->did);
			}

						// check if heapify is needed
			if (scoreAcc.compare(curThreshold)) {
				scoreType finalScore;
				scoreAcc.get(finalScore);
				resultsHeap.push(QpResult<resultType>(smallestCandidateDid,aux__convertResultToScore(finalScore,resultType())));
				curThreshold = std::max(curThreshold, aux__convertResultToScore(resultsHeap.fasthead().score,curThreshold));
			}
*/

			//Intersection code start ========================================================================
				bool aligned = true;
				//mv first list and compare
				nextLiveDid = std::max(nextLiveDid, listsA[0]->nextGEQ<0>( nextLiveDid ));

				unsigned i=0;
				for (; i<termsCnt && aligned && nextLiveDid<CONSTS::NUMERIC::MAXD; i++) {
					nextLiveDid = std::max(nextLiveDid, listsA[i]->nextGEQ<0>( nextLiveDid )); //nextGEQ and compare
					aligned &= (listsA[i]->did==listsA[0]->did);
				}
				if(i==termsCnt && aligned) {
					for (unsigned t=0; t<termsCnt; ++t)
						scoreAcc.add(listsA[t]->getScore<UseAugments>(curThreshold));
					if (scoreAcc.compare(curThreshold)) {
									++hitsCount;
									scoreType finalScore;
									scoreAcc.get(finalScore);
									resultsHeap.push(QpResult<resultType>(smallestCandidateDid,aux__convertResultToScore(finalScore,resultType())));
									curThreshold = std::max(curThreshold, aux__convertResultToScore(resultsHeap.fasthead().score,curThreshold));
					}
					smallestCandidateDid = nextSmallestDid = nextLiveDid;
				}


			//Intersection code end ========================================================================


			// update nextSmallestCandidateDid
			smallestCandidateDid = nextSmallestDid;
		} //end while window

		//if(UseAugments)
		NewIteratorAugmented::incCurrentWindow<1>();
	} //end windows
	COUTL << hitsCount << ENDL;
}

#endif /* ORIMP_H_ */
