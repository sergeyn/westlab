#include "Enums.h"
//created by automatic script, do not alter
std::string convert(const enm::prprPolicy p) {std::string print;
 switch(p) {
case enm::ONTHEFLY:  print = "ONTHEFLY";  break;
case enm::BM_FAKE:  print = "BM_FAKE";  break;
case enm::BM_PBS:  print = "BM_PBS";  break;
case enm::BM_PBSC:  print = "BM_PBSC";  break;
case enm::BMQ_FAKE:  print = "BMQ_FAKE";  break;
case enm::BMQC_FAKE:  print = "BMQC_FAKE";  break;
case enm::BMQ_PBS:  print = "BMQ_PBS";  break;
case enm::BMQC_PBS:  print = "BMQC_PBS";  break;
case enm::BMQ_PBSC:  print = "BMQ_PBSC";  break;
case enm::BMQC_PBSC:  print = "BMQC_PBSC";  break;
case enm::NONE:  print = "NONE";  break;
default: print = "ERR";    break; 
	};
 	return print; }
void convert(const std::string& str, enm::prprPolicy& p) {
if(str=="ONTHEFLY") p = enm::ONTHEFLY;
if(str=="BM_FAKE") p = enm::BM_FAKE;
if(str=="BM_PBS") p = enm::BM_PBS;
if(str=="BM_PBSC") p = enm::BM_PBSC;
if(str=="BMQ_FAKE") p = enm::BMQ_FAKE;
if(str=="BMQC_FAKE") p = enm::BMQC_FAKE;
if(str=="BMQ_PBS") p = enm::BMQ_PBS;
if(str=="BMQC_PBS") p = enm::BMQC_PBS;
if(str=="BMQ_PBSC") p = enm::BMQ_PBSC;
if(str=="BMQC_PBSC") p = enm::BMQC_PBSC;
if(str=="NONE") p = enm::NONE;
}
std::string convert(const enm::counters p) {std::string print;
 switch(p) {
case enm::COMPLETEQP:  print = "COMPLETEQP";  break;
case enm::SLOWNEXTGEQ:  print = "SLOWNEXTGEQ";  break;
case enm::FASTNEXTGEQ:  print = "FASTNEXTGEQ";  break;
case enm::NEXTGEQ:  print = "NEXTGEQ";  break;
case enm::NEXTGEQ_CHEAP:  print = "NEXTGEQ_CHEAP";  break;
case enm::EVALUATION:  print = "EVALUATION";  break;
case enm::HEAPIFY:  print = "HEAPIFY";  break;
case enm::CALC_BLOCK_SCORE:  print = "CALC_BLOCK_SCORE";  break;
case enm::PROC_AUGMENTS_LST:  print = "PROC_AUGMENTS_LST";  break;
case enm::PROC_AUGMENTS_ALL:  print = "PROC_AUGMENTS_ALL";  break;
case enm::LA_BUILD:  print = "LA_BUILD";  break;
case enm::TEST1:  print = "TEST1";  break;
case enm::TEST2:  print = "TEST2";  break;
case enm::NOP:  print = "NOP";  break;
default: print = "ERR";    break; 
	};
 	return print; }
void convert(const std::string& str, enm::counters& p) {
if(str=="COMPLETEQP") p = enm::COMPLETEQP;
if(str=="SLOWNEXTGEQ") p = enm::SLOWNEXTGEQ;
if(str=="FASTNEXTGEQ") p = enm::FASTNEXTGEQ;
if(str=="NEXTGEQ") p = enm::NEXTGEQ;
if(str=="NEXTGEQ_CHEAP") p = enm::NEXTGEQ_CHEAP;
if(str=="EVALUATION") p = enm::EVALUATION;
if(str=="HEAPIFY") p = enm::HEAPIFY;
if(str=="CALC_BLOCK_SCORE") p = enm::CALC_BLOCK_SCORE;
if(str=="PROC_AUGMENTS_LST") p = enm::PROC_AUGMENTS_LST;
if(str=="PROC_AUGMENTS_ALL") p = enm::PROC_AUGMENTS_ALL;
if(str=="LA_BUILD") p = enm::LA_BUILD;
if(str=="TEST1") p = enm::TEST1;
if(str=="TEST2") p = enm::TEST2;
if(str=="NOP") p = enm::NOP;
}
