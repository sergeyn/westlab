/*
 * OR.h
 *
 *  Created on: Dec 14, 2012
 *      Author: constantinos
 */

#ifndef OR_H_
#define OR_H_

#include "QP.h"

struct WindowManager {
	const unsigned maxWindowSize;
	unsigned current;
	unsigned didStartCurrent;
	unsigned didStartNext;
	std::vector<unsigned> sizes;
public:
	WindowManager(unsigned maxSz=CONSTS::NUMERIC::CACHE_FRIENDLY_CHUNK, unsigned policy=0);
	unsigned getStartDid() const { return didStartCurrent; }
	unsigned getStartBlock() const;
	unsigned getCurrentSizeDids() const { return sizes[current]; }
	unsigned getStartDidNext() const { return didStartNext; }
	unsigned operator++() { didStartCurrent=didStartNext; didStartNext+= sizes[++current]; return didStartCurrent; }
};

class OR {
private:
	std::vector<NewIteratorAugmented*> lists;

	const score_t threshold;
	const scoreInt_t thresholdInt;
	Options options;

public:
	OR(std::vector<NewIteratorAugmented*>& lps, Options opts, const score_t thresh=0.0f);

	void measurePreprocessTime();

	//=============================== Stats ========================================
	void LADistributions(const unsigned topk); // percent of live blocks
	void LAandPostingsDistributions(const unsigned topk); // percent of live miniblocks

	//===================================================================================//
	//============================= Floats =============================================//
	//===================================================================================//

	//=============================== exhaustive ========================================
	PriorityArray<QpResult<float> > exhaustiveWithWindows(const unsigned topk);
	PriorityArray<QpResult<float> > exhaustiveSimple(const unsigned topk);
	//=============================== WAND ========================================
	PriorityArray<QpResult<float> > WAND(const unsigned topk);
	bool pivotSelectionWAND(ITERATOR_VEC_PTR listsA, const score_t threshold, int& pivot, const unsigned termsCnt, const did_t bound, const int thresholdKnowledge);
	void handlePivotWAND(ITERATOR_VEC_PTR listsA, const int pivot, const unsigned termsCnt, PriorityArray<QpResult<float> >& resultsHeap, score_t& threshold, const did_t nextWinStart, const int thresholdKnowledge);
	//============================= BMW ====================================
	PriorityArray<QpResult<float> > BMW(const unsigned topK);
	bool pickCandidate(ITERATOR_VEC_PTR listsA, const unsigned termsCnt, const score_t threshold, int& pivot, score_t& sumOfBLockMaxesUptoPivot, const did_t nextWinStart, const int thresholdKnowledge);
	bool handleFake(ITERATOR_VEC_PTR listsA, const unsigned termsCnt, const int pivot, const did_t nextWinStart);
	void handleReal(ITERATOR_VEC_PTR listsA, const unsigned termsCnt, const int pivot, score_t sumOfBLockMaxesUptoPivot, PriorityArray<QpResult<float> >& resultsHeap, score_t& curThreshold, const unsigned topK, const did_t nextWinStart, const int thresholdKnowledge);
	//============================= Maxscore ====================================
	PriorityArray<QpResult<float> > Maxscore(const unsigned topk);
	void findNewNonEssentialLists(const unsigned termsCnt, unsigned& numEssentialLists, unsigned& numNonEssentialLists, const std::vector <score_t> prefixMaxScoresSum, const score_t threshold, bool& essentialListAdded, score_t& curNonEssentialMaxScoreSum);
	bool findSmallestDidInEssentialList(ITERATOR_VEC_PTR listsA, const unsigned termsCnt, const unsigned numNonEssentialLists, unsigned& smallestCandidateDid, const did_t bound);
	//============================= BMM ====================================
	PriorityArray<QpResult<float> > BMM(const unsigned topK);
	void evaluate(ITERATOR_VEC_PTR listsA, const unsigned termsCnt, PriorityArray<QpResult<float> >& resultsHeap, const score_t threshold, const unsigned topK,
			const did_t nextWinStart, const int numNonEssentialLists, did_t& smallestCandidateDid, std::vector <score_t>& listsCurBlockMaxScore,
			const score_t curNonEssentialMaxScoreSum, bool& checkNewEssentialList, std::vector <score_t>& listsMaxscore);
	void skipToNextLiveArea(ITERATOR_VEC_PTR listsA, const unsigned termsCnt, const int numNonEssentialLists, const did_t nextWinStart, did_t& smallestCandidateDid);

	//===================================================================================//
	//============================= Integers ===========================================//
	//===================================================================================//
/*
	//=============================== exhaustive ========================================
	PriorityArray<QpResult<> > exhaustiveInt(const unsigned topk);
	//=============================== WAND ========================================
	PriorityArray<QpResult<> > WANDInt(const unsigned topk);
	bool pivotSelectionWANDInt(ITERATOR_VEC_PTR_INT listsA, const scoreInt_t threshold, int& pivot, const unsigned termsCnt, const did_t bound, const int thresholdKnowledge);
	void handlePivotWANDInt(ITERATOR_VEC_PTR_INT listsA, const int pivot, const unsigned termsCnt, PriorityArray<QpResult<scoreInt_t> >& resultsHeap, scoreInt_t& threshold, const did_t nextWinStart, const int thresholdKnowledge);
	//============================= BMW ====================================
	PriorityArray<QpResult<> > BMWInt(const unsigned topK);
	bool pickCandidateInt(ITERATOR_VEC_PTR_INT listsA, const unsigned termsCnt, const scoreInt_t threshold, int& pivot, scoreInt_t& sumOfBLockMaxesUptoPivot, const did_t nextWinStart, const int thresholdKnowledge);
	bool handleFakeInt(ITERATOR_VEC_PTR_INT listsA, const unsigned termsCnt, const int pivot, const did_t nextWinStart);
	void handleRealInt(ITERATOR_VEC_PTR_INT listsA, const unsigned termsCnt, const int pivot, scoreInt_t sumOfBLockMaxesUptoPivot, PriorityArray<QpResult<scoreInt_t> >& resultsHeap, scoreInt_t& curThreshold, const unsigned topK, const did_t nextWinStart, const int thresholdKnowledge);
	//============================= Maxscore ====================================
	PriorityArray<QpResult<> > MaxscoreInt(const unsigned topk);
	void findNewNonEssentialListsInt(const unsigned termsCnt, unsigned& numEssentialLists, unsigned& numNonEssentialLists, const std::vector <scoreInt_t> prefixMaxScoresSum, const scoreInt_t threshold, bool& essentialListAdded, scoreInt_t& curNonEssentialMaxScoreSum);
	bool findSmallestDidInEssentialListInt(ITERATOR_VEC_PTR_INT listsA, const unsigned termsCnt, const unsigned numNonEssentialLists, unsigned& smallestCandidateDid, const did_t bound);
	//============================= BMM ====================================
	PriorityArray<QpResult<> > BMMInt(const unsigned topK);
	void evaluateInt(ITERATOR_VEC_PTR_INT listsA, const unsigned termsCnt, PriorityArray<QpResult<scoreInt_t> >& resultsHeap, const scoreInt_t threshold, const unsigned topK,
				const did_t nextWinStart, const int numNonEssentialLists, did_t& smallestCandidateDid, std::vector <scoreInt_t>& listsCurBlockMaxScore,
				const scoreInt_t curNonEssentialMaxScoreSum, bool& checkNewEssentialList, std::vector <scoreInt_t>& listsMaxscore);
	void skipToNextLiveAreaInt(ITERATOR_VEC_PTR_INT listsA, const unsigned termsCnt, const int numNonEssentialLists, const did_t nextWinStart, did_t& smallestCandidateDid);
*/
};

template<typename scoreType, typename resultType, int UseAugments>
class ExhaustiveWindowedOR : public QueryProcessorInterface {
private:
	std::vector<NewIteratorAugmented*> lists;
	scoreType threshold;
	Options options;

	PriorityArray<QpResult<resultType> > resultsHeap;
	AugmentsHelperInterface* augmentsHelper;

public:
	ExhaustiveWindowedOR():augmentsHelper(nullptr){};
	~ExhaustiveWindowedOR(){};
	void setup(std::vector<NewIteratorAugmented*>& lps, Options opts, AugmentsHelperInterface* augments, const float thresh);

	virtual void run(const unsigned topk);
	virtual void putResults(void* res);

	virtual const std::string name() const;
	virtual void* getOne() const;
};

class EvaluateDocs : public QueryProcessorInterface {
private:
	std::vector<NewIteratorAugmented*> lists;
	Options options;

	std::vector<QpResult<float> > targetMap;

public:
	EvaluateDocs(){};  
	~EvaluateDocs(){};
	void setTargets(const std::vector<QpResult<float> >& targets);
	
	virtual void setup(std::vector<NewIteratorAugmented*>& lps, Options opts, AugmentsHelperInterface* augments, const float thresh);

	virtual void run(const unsigned topk);
	virtual void putResults(void* res);

	virtual const std::string name() const;
	virtual void* getOne() const;
};

#include "OrIMP.h"


#endif /* OR_H_ */
