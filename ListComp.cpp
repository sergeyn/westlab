#include "ListComp.h"
#include "Compression.h"
#include <memory>

int ListCompression::makeDirs(const std::string& rootPath) {
	//this is a unix specific boo-boo -- replace with boost or write your own version...
	const std::string stringCmd = "mkdir -p "+  rootPath;
	return system(stringCmd.c_str());
}

long int ListCompression::serializeToFS(const std::string& rootPath ) const{
	return ListCompression::serializeToFS(rootPath,compressedVector);
}

long int ListCompression::serializeToFS(const std::string& rootPath, const vecUInt& compressedData ) {
	return appendToFile(rootPath+"/allCompressedLists",compressedData.begin(),compressedData.end()); // dump the compressed list
}

void ListCompression::serializeToDb(SqlProxy& sql, const ListMetaData& meta) {
	sqlite3_stmt *pStmt =  sql.prepare("insert or replace into terms values (?,?,?,?,?,?,?)"); // Compile the INSERT statement into a virtual machine.
	assert(meta.getSizePerBlock().size() == meta.getSizePerBlock().size() && meta.getSizePerBlock().size() == meta.getFlagsPerBlock().size());
	const unsigned blocksCount = meta.getFlagsPerBlock().size();
	const unsigned metaSizes = blocksCount*sizeof(unsigned int);

	//('tid' INTEGER PRIMARY KEY  NOT NULL  UNIQUE, 'term' TEXT, 'compFlags' BLOB, 'maxDidOfBlocks' BLOB, 'compSizes' BLOB, maxScoreOfList FLOAT, unpaddeLen INT)
	sqlite3_bind_int   (pStmt, 1, meta.getTid());
	sqlite3_bind_text  (pStmt, 2, meta.getTerm().c_str(), -1, SQLITE_STATIC);
	sqlite3_bind_blob  (pStmt, 3, meta.getFlagsPtr(), metaSizes, SQLITE_STATIC);
	sqlite3_bind_blob  (pStmt, 4, meta.getMaxDidPerBlockPtr(), metaSizes, SQLITE_STATIC);
	sqlite3_bind_blob  (pStmt, 5, meta.getSizePerBlockPtr(),metaSizes, SQLITE_STATIC);
	sqlite3_bind_double(pStmt, 6, 0.0); //we should deprecate this guy...
	sqlite3_bind_int   (pStmt, 7, meta.getUnpaddedLength());
	sql.executeStoredStatement();
}

void ListCompression::serializeToDb(SqlProxy& sql) {
	ListCompression::serializeToDb(sql,meta);
}

void ListCompression::compressDocsAndFreqs(const vecUInt& dids, const vecUInt& freqsOrQuants, bool isQuant) {
	//assuming BS padded list
	lengthOfList = dids.size();
	const unsigned BS(CONSTS::NUMERIC::BS);
	assert(dids.size()%BS == 0);
	assert(dids.size() == freqsOrQuants.size());
	
	size_t ind;
	std::vector<did_t> maxDidPerBlock;
	maxDidPerBlock.reserve(lengthOfList/BS);
	//build delta list for doc ids
	vecUInt deltaList = dids;
	for (ind = lengthOfList-1; ind > 0; ind--) {
		deltaList[ind] -= deltaList[ind-1];
	}


	vecUInt freq = freqsOrQuants;
	if(!isQuant)
		for (ind = 0; ind < lengthOfList; ++ind)
			freq[ind]--;

	assert(lengthOfList == deltaList.size() && lengthOfList == freq.size());

	int flag1, flag2, numb;
	unsigned int *start, *ptrOfDelta, *ptrOfFreq;

	vecUInt buffer(lengthOfList*2);

	vecUInt flag;
	flag.reserve(lengthOfList/BS);
	vecUInt sizePerBlock;
	sizePerBlock.reserve(lengthOfList/BS);

	start = buffer.data();

	//blocks iterator:
	for (size_t i = 0; i < lengthOfList; i += BS)	{
		vecUInt deltaTmp(deltaList.begin()+i,deltaList.begin()+i+BS); 
		vecUInt freqTmp(freq.begin()+i,freq.begin()+i+BS); 
		unsigned int* deltaPtr = deltaTmp.data();
		unsigned int* freqPtr = freqTmp.data();

		maxDidPerBlock.push_back(dids[i+BS-1]);

		ptrOfDelta = start;


		std::auto_ptr<OldPforDelta> ptrPforCmp(new OldPforDelta); //fallback to default compressor if none given
		CompressionInterface* cmpressor = (compressor) ? compressor : ptrPforCmp.get();

		std::pair<int,unsigned> packMeta = cmpressor->compressInts(deltaPtr,ptrOfDelta,CONSTS::NUMERIC::BS);
		ptrOfDelta += packMeta.second;
		flag1 = packMeta.first;

		ptrOfFreq = ptrOfDelta;
		packMeta = cmpressor->compressInts(freqPtr,ptrOfFreq,CONSTS::NUMERIC::BS);
		ptrOfFreq += packMeta.second;
		flag2 = packMeta.first;

		flag.push_back(((unsigned int)(flag1))|(((unsigned int)(flag2))<<16));

		if (ptrOfFreq - start > 256)
			CERR << "One block is " << ptrOfFreq - start << " bytes" << ENDL;

		sizePerBlock.push_back( (unsigned char)((ptrOfFreq - start) - 1));
		numb++;
		start = ptrOfFreq;
	} //end for block

	meta.swapFlags(flag);
	meta.swapSizePerBlock(sizePerBlock);
	meta.swapMaxDidsPerBlock(maxDidPerBlock);

	const size_t cells = (start-buffer.data());
	meta.setCompressedDataLengthBytes(cells * sizeof(int) );
	compressedVector.resize(cells);
	std::copy(buffer.data(),buffer.data()+cells,compressedVector.data());
	
	//COUT4 << " " << term << " lengthOfList: " << lengthOfList << " deltaList.size: " << deltaList.size() << " cpool_leng: " << compressedDataLength << ENDL;
}


//*********************************************************************************
//=================== New stuff
//*********************************************************************************

void Quasi::compressDocs(const vecUInt& dids) {
	const unsigned sz = dids.size();
	boundaries.resize(1<<9);
	data.reserve(sz);
	for(unsigned i=0; i<sz; ++i) {
		++boundaries[dids[i]>>16];
		data.push_back(65535);
	}
}

void Quasi::decompressDocsScalar(vecUInt& dids) {
	unsigned i=0;
	for(unsigned r=0; r<boundaries.size(); ++r)
		for(unsigned j=0; j<boundaries[r]; ++j)
			dids.push_back( (r<<16)+data[i++] );
}



