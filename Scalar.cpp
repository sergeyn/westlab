#include "Scalar.h"

void buildLiveAreaQuantScalar_(unchar* ptr, unchar** blockScores, unchar** bitSetsPBS, const unchar threshold, const unsigned termsCount, const unsigned windowsize){
	for(unsigned i=0; i<windowsize; ++i) {
		unsigned sum = (unsigned)(blockScores[0][i]);
		for(unsigned lst=1; lst<termsCount; ++lst) sum+= (unsigned)(blockScores[lst][i]);
		if(sum>=threshold) { //second layer:
			//now we need to figure out the situation for 8 miniblocks
			for(unsigned bit=0; bit<8; ++bit) {
				sum=0;
				for(unsigned lst=0; lst<termsCount; ++lst)
					sum+= (unsigned)(blockScores[lst][i] * ((bitSetsPBS[lst][i]>>bit)&1));
				if(sum>=threshold)
					ptr[i] |= (1<<bit);
			} //end for miniblocks
		} //end second layer
	} //end for window
	ptr[windowsize-1]|=128; //introduce elephant in Cairo
}

void buildPBS_M3_Scalar_(unsigned* dest, const unsigned* dids, const unsigned winStartDid, unsigned bits, const unsigned sz) {
	assert(dids && dids[0]>=winStartDid);
	bits -= 3; //this is why it is the M3 version -- every max-block has 8 miniblocks
	unchar* packed = (unchar*)dest;
	unsigned  alignedSize = (sz>>2)<<2;
	#define BUILD_PBS_HELPER__S(x) (dids[x]-winStartDid)>>bits
	for(unsigned i=0; i<alignedSize; i+=4) {
		//for(unsigned t=0; t<4; ++t) if(winStartDid>dids[i+t]) COUTL << t << " " << i+t << " " << dids[i+t] << " " << winStartDid << ENDL;
		unsigned four[4] = {BUILD_PBS_HELPER__S(i), BUILD_PBS_HELPER__S(i+1), BUILD_PBS_HELPER__S(i+2), BUILD_PBS_HELPER__S(i+3)};
		setBit(packed,four[0]); setBit(packed,four[1]); setBit(packed,four[2]); setBit(packed,four[3]); //set appropriate bits (for 4 shifted dids)
	}
	//leftovers:
	for(unsigned i=alignedSize; i<sz; ++i)
		setBit(packed, (dids[i]-winStartDid)>>bits);
}
