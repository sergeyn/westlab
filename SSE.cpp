#include "SSE.h"
#include "assert.h"

#include "globals.h"
#include "utils.h"

void buildLiveAreaQuantSSE_(unchar* ptr, unchar** blockScores, unchar** bitSetsPBS, const unchar threshold, const unsigned termsCount, const unsigned windowsize) {
	__m128i thresholds = _mm_set1_epi8(threshold); //create 16 copies of threshold

	for(unsigned i=0; i<windowsize; i+=16) {
		__m128i sumv = _mm_load_si128( (__m128i*) (blockScores[0]+i) ); //load 16 scores for list 0
		for(unsigned lst = 1; lst<termsCount; ++lst)
			sumv = _mm_adds_epu8(sumv, _mm_load_si128( (__m128i*) (blockScores[lst]+i))); //add 16 scores for list lst to sum

		__m128i masksL1 = _mm_cmpeq_epi8(_mm_max_epu8(sumv,thresholds),sumv); //get the mask for all  >= threshold
		int maskBitsL1 = _mm_movemask_epi8(masksL1); //extract the mask

		//_mm_store_si128((__m128i*)buffer,masksL1); memcpy(ptr+i,buffer,16); //for layer one only

		//now 2nd layer will take care of 16 blocks and make them 128 miniblocks
		for(unsigned bit=0; bit<16 && maskBitsL1; bit+=2) {
			unchar bits2 = maskBitsL1&3;
			maskBitsL1 >>=2;
			if(!bits2) continue; //this is an actual speedup! TODO: see if makes sense to look at all 4 cases...

			sumv = _mm_set1_epi8(0);
			for(unsigned lst = 0; lst<termsCount; ++lst) {
				const unsigned short bits16 = *((unsigned short*)(bitSetsPBS[lst]+i+bit)); //read 16 bits representing these 2 blocks' mini-blocks
				__m128i maskedTwoVals = duplicateTwoValsAndMaskWith16bits(blockScores[lst][i+bit],blockScores[lst][i+bit+1],bits16);
				sumv = _mm_adds_epu8(sumv, maskedTwoVals);
			} //end lists
			__m128i masksL2 = _mm_cmpeq_epi8(_mm_max_epu8(sumv,thresholds),sumv); //get the mask for all  >= threshold
			int maskBitsL2 = _mm_movemask_epi8(masksL2); //extract the mask

			ptr[i+bit]=(unchar)((maskBitsL2)&255);
			ptr[i+bit+1]=(unchar)((maskBitsL2>>8)&255);
		} //end bits
	} //end window

	ptr[windowsize-1]|=128; //introduce elephant in Cairo
}

// called from AugmentsQuantBlockMaxSSE<scoreType>::buildPBS_M3 there we memset
void buildPBS_M3_SSE_(unsigned* dest, const unsigned* dids, const unsigned winStartDid, unsigned bits, const unsigned sz) {
	assert(dids && dids[0]>=winStartDid);
	bits -= 3; //this is why it is the M3 version -- every max-block has 8 miniblocks
	unsigned  alignedSize = (sz>>2)<<2;
	unsigned char* packed = (unsigned char*)dest;
	int four[4] = {0,0,0,0};
	unsigned  starts[4] = { winStartDid,winStartDid,winStartDid,winStartDid};
	__m128i startsS = _mm_load_si128( (__m128i*)(starts));
	__m128i shiftbits = {bits,bits}; //TODO: validate!!!
	for(unsigned i=0; i<alignedSize; i+=4) {
		__m128i a = _mm_sub_epi32(_mm_load_si128( (__m128i*)(dids+i)), startsS); //load 4 dids and subtract the window start from them
		_mm_store_si128((__m128i*)(four), _mm_srl_epi32(a,shiftbits) ); //shift all 4 dids
		setBit(packed,four[0]); setBit(packed,four[1]); setBit(packed,four[2]); setBit(packed,four[3]); //set appropriate bits (for 4 shifted dids)
	}
	//leftovers:
	for(unsigned i=alignedSize; i<sz; ++i)
		setBit(packed, (dids[i]-winStartDid)>>bits);
}




//=============================================================================================
void dequantLinear(const unsigned char* arrSrc, float* arrDst, const unsigned sizeUnpadded, float quantile) {
	__m128 quant4 = {quantile,quantile,quantile,quantile};
	__m128i zero = _mm_set1_epi8(0);
	const unsigned size =(sizeUnpadded>>4)<<4; //make 16 aligned
	unsigned q;
	for(q=0; q<size; q+=16) {
		//first make 16 uint_16 out of 16 uint_8
		__m128i x = _mm_load_si128( (__m128i*)(arrSrc+q));
		__m128i xlo = _mm_unpacklo_epi8(x, zero);
		__m128i xhi = _mm_unpackhi_epi8(x, zero);

		//then make 16 uint_32 out of 16 uint_16
		__m128i xllo = _mm_unpacklo_epi16(xlo, zero);
		__m128i xlhi = _mm_unpackhi_epi16(xlo, zero);
		__m128i xhlo = _mm_unpacklo_epi16(xhi, zero);
		__m128i xhhi = _mm_unpackhi_epi16(xhi, zero);

		//convert to float
		__m128 yllo = _mm_cvtepi32_ps(xllo);
		__m128 ylhi = _mm_cvtepi32_ps(xlhi);
		__m128 yhlo = _mm_cvtepi32_ps(xhlo);
		__m128 yhhi = _mm_cvtepi32_ps(xhhi);

		//quantize and store
		float* ptr = arrDst+q;
		_mm_store_ps(ptr,   (_mm_mul_ps(yllo,quant4)));
		_mm_store_ps(ptr+4, (_mm_mul_ps(ylhi,quant4)));
		_mm_store_ps(ptr+8, (_mm_mul_ps(yhlo,quant4)));
		_mm_store_ps(ptr+12,(_mm_mul_ps(yhhi,quant4)));
	}
	//and if there are "spill overs" do them here
	for(; q<sizeUnpadded; ++q) arrDst[q]=arrSrc[q]*quantile;
}
//=============================================================================================
void dequantLinear(const unsigned* arrSrc, float* arrDst, const unsigned sizeUnpadded, float quantile) {
	__m128 quant4 = {quantile,quantile,quantile,quantile};
	const unsigned size = (sizeUnpadded>>2)<<2; //make 4 aligned
	unsigned q;
	for(q=0; q<size; q+=4) {
		__m128i x = _mm_load_si128( (__m128i*)(arrSrc+q)); //load 4 unsigned
		__m128 y = _mm_cvtepi32_ps(x); //convert to float
		float* ptr = arrDst+q;
		_mm_store_ps(ptr,   (_mm_mul_ps(y,quant4)));//quantize and store
	}
	//and if there are "spill overs" do them here
	for(; q<sizeUnpadded; ++q) arrDst[q]=arrSrc[q]*quantile;
}





void unpackBytesToShorts(const unsigned char* src, unsigned short* dest, const unsigned size){
	const __m128i zero = _mm_set1_epi8(0);
	__m128i x;
	assert(size%16==0);
	for(unsigned i=0; i<size; i+=16) {
		x = _mm_load_si128( (__m128i*)(src+i)); //make 16 uint_16 out of 16 uint_8
		_mm_store_si128((__m128i*)(dest+i),_mm_unpacklo_epi8(x, zero)); //first 8 shorts
		_mm_store_si128((__m128i*)(dest+i+8),_mm_unpackhi_epi8(x, zero)); //second 8 shorts
	}
}

void unpackBytesToUnsigneds(const unsigned char* src, unsigned* dest, const unsigned size){
	const __m128i zero = _mm_set1_epi8(0);
	__m128i x,xlo,xhi;
	assert(size%16==0);
	for(unsigned i=0; i<size; i+=16) {
		x = _mm_load_si128( (__m128i*)(src+i)); //make 16 uint_16 out of 16 uint_8
		xlo = _mm_unpacklo_epi8(x, zero);//first 8 shorts
		xhi = _mm_unpackhi_epi8(x, zero);//second 8 shorts

		//then make 16 uint_32 out of 16 uint_16
		_mm_store_si128((__m128i*)(dest+i),_mm_unpacklo_epi16(xlo, zero)); //first 4 unsigneds
		_mm_store_si128((__m128i*)(dest+i+4),_mm_unpackhi_epi16(xlo, zero));//second 4 unsigneds
		_mm_store_si128((__m128i*)(dest+i+8),_mm_unpacklo_epi16(xhi, zero));//third 4 unsigneds
		_mm_store_si128((__m128i*)(dest+i+12),_mm_unpackhi_epi16(xhi, zero));//forth 4 unsigneds
	}
}

void unpack64BytesToShorts(const unsigned char* src, unsigned short* dest){
	const __m128i zero = _mm_set1_epi8(0);
	__m128i x;
	x = _mm_load_si128( (__m128i*)(src)); //make 16 uint_16 out of 16 uint_8
	_mm_store_si128((__m128i*)dest,_mm_unpacklo_epi8(x, zero)); //first 8 shorts
	_mm_store_si128((__m128i*)(dest+8),_mm_unpackhi_epi8(x, zero)); //second 8 shorts

	x = _mm_load_si128( (__m128i*)(src+16));
	_mm_store_si128((__m128i*)(dest+16),_mm_unpacklo_epi8(x, zero));
	_mm_store_si128((__m128i*)(dest+24),_mm_unpackhi_epi8(x, zero));

	x = _mm_load_si128( (__m128i*)(src+32));
	_mm_store_si128((__m128i*)(dest+32),_mm_unpacklo_epi8(x, zero));
	_mm_store_si128((__m128i*)(dest+40),_mm_unpackhi_epi8(x, zero));

	x = _mm_load_si128( (__m128i*)(src+48));
	_mm_store_si128((__m128i*)(dest+48),_mm_unpacklo_epi8(x, zero));
	_mm_store_si128((__m128i*)(dest+56),_mm_unpackhi_epi8(x, zero));
}

void unpack64BytesToUnsigneds(const unsigned char* src, unsigned* dest){
	const __m128i zero = _mm_set1_epi8(0);
	__m128i x,xlo,xhi;

	x = _mm_load_si128( (__m128i*)(src)); //make 16 uint_16 out of 16 uint_8
	xlo = _mm_unpacklo_epi8(x, zero);//first 8 shorts
	xhi = _mm_unpackhi_epi8(x, zero);//second 8 shorts

	//then make 16 uint_32 out of 16 uint_16
	_mm_store_si128((__m128i*)(dest),_mm_unpacklo_epi16(xlo, zero)); //first 4 unsigneds
	_mm_store_si128((__m128i*)(dest+4),_mm_unpackhi_epi16(xlo, zero));//second 4 unsigneds
	_mm_store_si128((__m128i*)(dest+8),_mm_unpacklo_epi16(xhi, zero));//third 4 unsigneds
	_mm_store_si128((__m128i*)(dest+12),_mm_unpackhi_epi16(xhi, zero));//forth 4 unsigneds

	x = _mm_load_si128( (__m128i*)(src+16)); //make 16 uint_16 out of 16 uint_8
	xlo = _mm_unpacklo_epi8(x, zero);//first 8 shorts
	xhi = _mm_unpackhi_epi8(x, zero);//second 8 shorts

	//then make 16 uint_32 out of 16 uint_16
	_mm_store_si128((__m128i*)(dest+16),_mm_unpacklo_epi16(xlo, zero)); //first 4 unsigneds
	_mm_store_si128((__m128i*)(dest+20),_mm_unpackhi_epi16(xlo, zero));//second 4 unsigneds
	_mm_store_si128((__m128i*)(dest+24),_mm_unpacklo_epi16(xhi, zero));//third 4 unsigneds
	_mm_store_si128((__m128i*)(dest+28),_mm_unpackhi_epi16(xhi, zero));//forth 4 unsigneds

	x = _mm_load_si128( (__m128i*)(src+32)); //make 16 uint_16 out of 16 uint_8
	xlo = _mm_unpacklo_epi8(x, zero);//first 8 shorts
	xhi = _mm_unpackhi_epi8(x, zero);//second 8 shorts

	//then make 16 uint_32 out of 16 uint_16
	_mm_store_si128((__m128i*)(dest+32),_mm_unpacklo_epi16(xlo, zero)); //first 4 unsigneds
	_mm_store_si128((__m128i*)(dest+36),_mm_unpackhi_epi16(xlo, zero));//second 4 unsigneds
	_mm_store_si128((__m128i*)(dest+40),_mm_unpacklo_epi16(xhi, zero));//third 4 unsigneds
	_mm_store_si128((__m128i*)(dest+44),_mm_unpackhi_epi16(xhi, zero));//forth 4 unsigneds

	x = _mm_load_si128( (__m128i*)(src+48)); //make 16 uint_16 out of 16 uint_8
	xlo = _mm_unpacklo_epi8(x, zero);//first 8 shorts
	xhi = _mm_unpackhi_epi8(x, zero);//second 8 shorts

	//then make 16 uint_32 out of 16 uint_16
	_mm_store_si128((__m128i*)(dest+48),_mm_unpacklo_epi16(xlo, zero)); //first 4 unsigneds
	_mm_store_si128((__m128i*)(dest+52),_mm_unpackhi_epi16(xlo, zero));//second 4 unsigneds
	_mm_store_si128((__m128i*)(dest+56),_mm_unpacklo_epi16(xhi, zero));//third 4 unsigneds
	_mm_store_si128((__m128i*)(dest+60),_mm_unpackhi_epi16(xhi, zero));//forth 4 unsigneds

}

/*
ruby -e " (0...256).each {|i| print '{'+(i&15).to_s + ',' + (i>>4).to_s + '}' }"
{0,0}{1,0}{2,0}{3,0}{4,0}{5,0}{6,0}{7,0}{8,0}{9,0}{10,0}{11,0}{12,0}{13,0}{14,0}{15,0}{0,1}{1,1}{2,1}{3,1}{4,1}{5,1}{6,1}{7,1}{8,1}{9,1}{10,1}{11,1}{12,1}{13,1}{14,1}{15,1}{0,2}{1,2}{2,2}{3,2}{4,2}{5,2}{6,2}{7,2}{8,2}{9,2}{10,2}{11,2}{12,2}{13,2}{14,2}{15,2}{0,3}{1,3}{2,3}{3,3}{4,3}{5,3}{6,3}{7,3}{8,3}{9,3}{10,3}{11,3}{12,3}{13,3}{14,3}{15,3}{0,4}{1,4}{2,4}{3,4}{4,4}{5,4}{6,4}{7,4}{8,4}{9,4}{10,4}{11,4}{12,4}{13,4}{14,4}{15,4}{0,5}{1,5}{2,5}{3,5}{4,5}{5,5}{6,5}{7,5}{8,5}{9,5}{10,5}{11,5}{12,5}{13,5}{14,5}{15,5}{0,6}{1,6}{2,6}{3,6}{4,6}{5,6}{6,6}{7,6}{8,6}{9,6}{10,6}{11,6}{12,6}{13,6}{14,6}{15,6}{0,7}{1,7}{2,7}{3,7}{4,7}{5,7}{6,7}{7,7}{8,7}{9,7}{10,7}{11,7}{12,7}{13,7}{14,7}{15,7}{0,8}{1,8}{2,8}{3,8}{4,8}{5,8}{6,8}{7,8}{8,8}{9,8}{10,8}{11,8}{12,8}{13,8}{14,8}{15,8}{0,9}{1,9}{2,9}{3,9}{4,9}{5,9}{6,9}{7,9}{8,9}{9,9}{10,9}{11,9}{12,9}{13,9}{14,9}{15,9}{0,10}{1,10}{2,10}{3,10}{4,10}{5,10}{6,10}{7,10}{8,10}{9,10}{10,10}{11,10}{12,10}{13,10}{14,10}{15,10}{0,11}{1,11}{2,11}{3,11}{4,11}{5,11}{6,11}{7,11}{8,11}{9,11}{10,11}{11,11}{12,11}{13,11}{14,11}{15,11}{0,12}{1,12}{2,12}{3,12}{4,12}{5,12}{6,12}{7,12}{8,12}{9,12}{10,12}{11,12}{12,12}{13,12}{14,12}{15,12}{0,13}{1,13}{2,13}{3,13}{4,13}{5,13}{6,13}{7,13}{8,13}{9,13}{10,13}{11,13}{12,13}{13,13}{14,13}{15,13}{0,14}{1,14}{2,14}{3,14}{4,14}{5,14}{6,14}{7,14}{8,14}{9,14}{10,14}{11,14}{12,14}{13,14}{14,14}{15,14}{0,15}{1,15}{2,15}{3,15}{4,15}{5,15}{6,15}{7,15}{8,15}{9,15}{10,15}{11,15}{12,15}{13,15}{14,15}{15,15}

ruby -e " (0...256).each {|i| print '{'; (0..3).each {|b| print ((i>>(2*b))&3).to_s + ',' }; print '}' }"
{0,0,0,0}{1,0,0,0}{2,0,0,0}{3,0,0,0}{0,1,0,0}{1,1,0,0}{2,1,0,0}{3,1,0,0}{0,2,0,0}{1,2,0,0}{2,2,0,0}{3,2,0,0}{0,3,0,0}{1,3,0,0}{2,3,0,0}{3,3,0,0}{0,0,1,0}{1,0,1,0}{2,0,1,0}{3,0,1,0}{0,1,1,0}{1,1,1,0}{2,1,1,0}{3,1,1,0}{0,2,1,0}{1,2,1,0}{2,2,1,0}{3,2,1,0}{0,3,1,0}{1,3,1,0}{2,3,1,0}{3,3,1,0}{0,0,2,0}{1,0,2,0}{2,0,2,0}{3,0,2,0}{0,1,2,0}{1,1,2,0}{2,1,2,0}{3,1,2,0}{0,2,2,0}{1,2,2,0}{2,2,2,0}{3,2,2,0}{0,3,2,0}{1,3,2,0}{2,3,2,0}{3,3,2,0}{0,0,3,0}{1,0,3,0}{2,0,3,0}{3,0,3,0}{0,1,3,0}{1,1,3,0}{2,1,3,0}{3,1,3,0}{0,2,3,0}{1,2,3,0}{2,2,3,0}{3,2,3,0}{0,3,3,0}{1,3,3,0}{2,3,3,0}{3,3,3,0}{0,0,0,1}{1,0,0,1}{2,0,0,1}{3,0,0,1}{0,1,0,1}{1,1,0,1}{2,1,0,1}{3,1,0,1}{0,2,0,1}{1,2,0,1}{2,2,0,1}{3,2,0,1}{0,3,0,1}{1,3,0,1}{2,3,0,1}{3,3,0,1}{0,0,1,1}{1,0,1,1}{2,0,1,1}{3,0,1,1}{0,1,1,1}{1,1,1,1}{2,1,1,1}{3,1,1,1}{0,2,1,1}{1,2,1,1}{2,2,1,1}{3,2,1,1}{0,3,1,1}{1,3,1,1}{2,3,1,1}{3,3,1,1}{0,0,2,1}{1,0,2,1}{2,0,2,1}{3,0,2,1}{0,1,2,1}{1,1,2,1}{2,1,2,1}{3,1,2,1}{0,2,2,1}{1,2,2,1}{2,2,2,1}{3,2,2,1}{0,3,2,1}{1,3,2,1}{2,3,2,1}{3,3,2,1}{0,0,3,1}{1,0,3,1}{2,0,3,1}{3,0,3,1}{0,1,3,1}{1,1,3,1}{2,1,3,1}{3,1,3,1}{0,2,3,1}{1,2,3,1}{2,2,3,1}{3,2,3,1}{0,3,3,1}{1,3,3,1}{2,3,3,1}{3,3,3,1}{0,0,0,2}{1,0,0,2}{2,0,0,2}{3,0,0,2}{0,1,0,2}{1,1,0,2}{2,1,0,2}{3,1,0,2}{0,2,0,2}{1,2,0,2}{2,2,0,2}{3,2,0,2}{0,3,0,2}{1,3,0,2}{2,3,0,2}{3,3,0,2}{0,0,1,2}{1,0,1,2}{2,0,1,2}{3,0,1,2}{0,1,1,2}{1,1,1,2}{2,1,1,2}{3,1,1,2}{0,2,1,2}{1,2,1,2}{2,2,1,2}{3,2,1,2}{0,3,1,2}{1,3,1,2}{2,3,1,2}{3,3,1,2}{0,0,2,2}{1,0,2,2}{2,0,2,2}{3,0,2,2}{0,1,2,2}{1,1,2,2}{2,1,2,2}{3,1,2,2}{0,2,2,2}{1,2,2,2}{2,2,2,2}{3,2,2,2}{0,3,2,2}{1,3,2,2}{2,3,2,2}{3,3,2,2}{0,0,3,2}{1,0,3,2}{2,0,3,2}{3,0,3,2}{0,1,3,2}{1,1,3,2}{2,1,3,2}{3,1,3,2}{0,2,3,2}{1,2,3,2}{2,2,3,2}{3,2,3,2}{0,3,3,2}{1,3,3,2}{2,3,3,2}{3,3,3,2}{0,0,0,3}{1,0,0,3}{2,0,0,3}{3,0,0,3}{0,1,0,3}{1,1,0,3}{2,1,0,3}{3,1,0,3}{0,2,0,3}{1,2,0,3}{2,2,0,3}{3,2,0,3}{0,3,0,3}{1,3,0,3}{2,3,0,3}{3,3,0,3}{0,0,1,3}{1,0,1,3}{2,0,1,3}{3,0,1,3}{0,1,1,3}{1,1,1,3}{2,1,1,3}{3,1,1,3}{0,2,1,3}{1,2,1,3}{2,2,1,3}{3,2,1,3}{0,3,1,3}{1,3,1,3}{2,3,1,3}{3,3,1,3}{0,0,2,3}{1,0,2,3}{2,0,2,3}{3,0,2,3}{0,1,2,3}{1,1,2,3}{2,1,2,3}{3,1,2,3}{0,2,2,3}{1,2,2,3}{2,2,2,3}{3,2,2,3}{0,3,2,3}{1,3,2,3}{2,3,2,3}{3,3,2,3}{0,0,3,3}{1,0,3,3}{2,0,3,3}{3,0,3,3}{0,1,3,3}{1,1,3,3}{2,1,3,3}{3,1,3,3}{0,2,3,3}{1,2,3,3}{2,2,3,3}{3,2,3,3}{0,3,3,3}{1,3,3,3}{2,3,3,3}{3,3,3,3}

ruby -e " (0...256).each {|i| print '{'; (0..7).each {|b| print ((i>>(b))&1).to_s + ',' }; print '}' }"
{0,0,0,0,0,0,0,0}{1,0,0,0,0,0,0,0}{0,1,0,0,0,0,0,0}{1,1,0,0,0,0,0,0}{0,0,1,0,0,0,0,0}{1,0,1,0,0,0,0,0}{0,1,1,0,0,0,0,0}{1,1,1,0,0,0,0,0}{0,0,0,1,0,0,0,0}{1,0,0,1,0,0,0,0}{0,1,0,1,0,0,0,0}{1,1,0,1,0,0,0,0}{0,0,1,1,0,0,0,0}{1,0,1,1,0,0,0,0}{0,1,1,1,0,0,0,0}{1,1,1,1,0,0,0,0}{0,0,0,0,1,0,0,0}{1,0,0,0,1,0,0,0}{0,1,0,0,1,0,0,0}{1,1,0,0,1,0,0,0}{0,0,1,0,1,0,0,0}{1,0,1,0,1,0,0,0}{0,1,1,0,1,0,0,0}{1,1,1,0,1,0,0,0}{0,0,0,1,1,0,0,0}{1,0,0,1,1,0,0,0}{0,1,0,1,1,0,0,0}{1,1,0,1,1,0,0,0}{0,0,1,1,1,0,0,0}{1,0,1,1,1,0,0,0}{0,1,1,1,1,0,0,0}{1,1,1,1,1,0,0,0}{0,0,0,0,0,1,0,0}{1,0,0,0,0,1,0,0}{0,1,0,0,0,1,0,0}{1,1,0,0,0,1,0,0}{0,0,1,0,0,1,0,0}{1,0,1,0,0,1,0,0}{0,1,1,0,0,1,0,0}{1,1,1,0,0,1,0,0}{0,0,0,1,0,1,0,0}{1,0,0,1,0,1,0,0}{0,1,0,1,0,1,0,0}{1,1,0,1,0,1,0,0}{0,0,1,1,0,1,0,0}{1,0,1,1,0,1,0,0}{0,1,1,1,0,1,0,0}{1,1,1,1,0,1,0,0}{0,0,0,0,1,1,0,0}{1,0,0,0,1,1,0,0}{0,1,0,0,1,1,0,0}{1,1,0,0,1,1,0,0}{0,0,1,0,1,1,0,0}{1,0,1,0,1,1,0,0}{0,1,1,0,1,1,0,0}{1,1,1,0,1,1,0,0}{0,0,0,1,1,1,0,0}{1,0,0,1,1,1,0,0}{0,1,0,1,1,1,0,0}{1,1,0,1,1,1,0,0}{0,0,1,1,1,1,0,0}{1,0,1,1,1,1,0,0}{0,1,1,1,1,1,0,0}{1,1,1,1,1,1,0,0}{0,0,0,0,0,0,1,0}{1,0,0,0,0,0,1,0}{0,1,0,0,0,0,1,0}{1,1,0,0,0,0,1,0}{0,0,1,0,0,0,1,0}{1,0,1,0,0,0,1,0}{0,1,1,0,0,0,1,0}{1,1,1,0,0,0,1,0}{0,0,0,1,0,0,1,0}{1,0,0,1,0,0,1,0}{0,1,0,1,0,0,1,0}{1,1,0,1,0,0,1,0}{0,0,1,1,0,0,1,0}{1,0,1,1,0,0,1,0}{0,1,1,1,0,0,1,0}{1,1,1,1,0,0,1,0}{0,0,0,0,1,0,1,0}{1,0,0,0,1,0,1,0}{0,1,0,0,1,0,1,0}{1,1,0,0,1,0,1,0}{0,0,1,0,1,0,1,0}{1,0,1,0,1,0,1,0}{0,1,1,0,1,0,1,0}{1,1,1,0,1,0,1,0}{0,0,0,1,1,0,1,0}{1,0,0,1,1,0,1,0}{0,1,0,1,1,0,1,0}{1,1,0,1,1,0,1,0}{0,0,1,1,1,0,1,0}{1,0,1,1,1,0,1,0}{0,1,1,1,1,0,1,0}{1,1,1,1,1,0,1,0}{0,0,0,0,0,1,1,0}{1,0,0,0,0,1,1,0}{0,1,0,0,0,1,1,0}{1,1,0,0,0,1,1,0}{0,0,1,0,0,1,1,0}{1,0,1,0,0,1,1,0}{0,1,1,0,0,1,1,0}{1,1,1,0,0,1,1,0}{0,0,0,1,0,1,1,0}{1,0,0,1,0,1,1,0}{0,1,0,1,0,1,1,0}{1,1,0,1,0,1,1,0}{0,0,1,1,0,1,1,0}{1,0,1,1,0,1,1,0}{0,1,1,1,0,1,1,0}{1,1,1,1,0,1,1,0}{0,0,0,0,1,1,1,0}{1,0,0,0,1,1,1,0}{0,1,0,0,1,1,1,0}{1,1,0,0,1,1,1,0}{0,0,1,0,1,1,1,0}{1,0,1,0,1,1,1,0}{0,1,1,0,1,1,1,0}{1,1,1,0,1,1,1,0}{0,0,0,1,1,1,1,0}{1,0,0,1,1,1,1,0}{0,1,0,1,1,1,1,0}{1,1,0,1,1,1,1,0}{0,0,1,1,1,1,1,0}{1,0,1,1,1,1,1,0}{0,1,1,1,1,1,1,0}{1,1,1,1,1,1,1,0}{0,0,0,0,0,0,0,1}{1,0,0,0,0,0,0,1}{0,1,0,0,0,0,0,1}{1,1,0,0,0,0,0,1}{0,0,1,0,0,0,0,1}{1,0,1,0,0,0,0,1}{0,1,1,0,0,0,0,1}{1,1,1,0,0,0,0,1}{0,0,0,1,0,0,0,1}{1,0,0,1,0,0,0,1}{0,1,0,1,0,0,0,1}{1,1,0,1,0,0,0,1}{0,0,1,1,0,0,0,1}{1,0,1,1,0,0,0,1}{0,1,1,1,0,0,0,1}{1,1,1,1,0,0,0,1}{0,0,0,0,1,0,0,1}{1,0,0,0,1,0,0,1}{0,1,0,0,1,0,0,1}{1,1,0,0,1,0,0,1}{0,0,1,0,1,0,0,1}{1,0,1,0,1,0,0,1}{0,1,1,0,1,0,0,1}{1,1,1,0,1,0,0,1}{0,0,0,1,1,0,0,1}{1,0,0,1,1,0,0,1}{0,1,0,1,1,0,0,1}{1,1,0,1,1,0,0,1}{0,0,1,1,1,0,0,1}{1,0,1,1,1,0,0,1}{0,1,1,1,1,0,0,1}{1,1,1,1,1,0,0,1}{0,0,0,0,0,1,0,1}{1,0,0,0,0,1,0,1}{0,1,0,0,0,1,0,1}{1,1,0,0,0,1,0,1}{0,0,1,0,0,1,0,1}{1,0,1,0,0,1,0,1}{0,1,1,0,0,1,0,1}{1,1,1,0,0,1,0,1}{0,0,0,1,0,1,0,1}{1,0,0,1,0,1,0,1}{0,1,0,1,0,1,0,1}{1,1,0,1,0,1,0,1}{0,0,1,1,0,1,0,1}{1,0,1,1,0,1,0,1}{0,1,1,1,0,1,0,1}{1,1,1,1,0,1,0,1}{0,0,0,0,1,1,0,1}{1,0,0,0,1,1,0,1}{0,1,0,0,1,1,0,1}{1,1,0,0,1,1,0,1}{0,0,1,0,1,1,0,1}{1,0,1,0,1,1,0,1}{0,1,1,0,1,1,0,1}{1,1,1,0,1,1,0,1}{0,0,0,1,1,1,0,1}{1,0,0,1,1,1,0,1}{0,1,0,1,1,1,0,1}{1,1,0,1,1,1,0,1}{0,0,1,1,1,1,0,1}{1,0,1,1,1,1,0,1}{0,1,1,1,1,1,0,1}{1,1,1,1,1,1,0,1}{0,0,0,0,0,0,1,1}{1,0,0,0,0,0,1,1}{0,1,0,0,0,0,1,1}{1,1,0,0,0,0,1,1}{0,0,1,0,0,0,1,1}{1,0,1,0,0,0,1,1}{0,1,1,0,0,0,1,1}{1,1,1,0,0,0,1,1}{0,0,0,1,0,0,1,1}{1,0,0,1,0,0,1,1}{0,1,0,1,0,0,1,1}{1,1,0,1,0,0,1,1}{0,0,1,1,0,0,1,1}{1,0,1,1,0,0,1,1}{0,1,1,1,0,0,1,1}{1,1,1,1,0,0,1,1}{0,0,0,0,1,0,1,1}{1,0,0,0,1,0,1,1}{0,1,0,0,1,0,1,1}{1,1,0,0,1,0,1,1}{0,0,1,0,1,0,1,1}{1,0,1,0,1,0,1,1}{0,1,1,0,1,0,1,1}{1,1,1,0,1,0,1,1}{0,0,0,1,1,0,1,1}{1,0,0,1,1,0,1,1}{0,1,0,1,1,0,1,1}{1,1,0,1,1,0,1,1}{0,0,1,1,1,0,1,1}{1,0,1,1,1,0,1,1}{0,1,1,1,1,0,1,1}{1,1,1,1,1,0,1,1}{0,0,0,0,0,1,1,1}{1,0,0,0,0,1,1,1}{0,1,0,0,0,1,1,1}{1,1,0,0,0,1,1,1}{0,0,1,0,0,1,1,1}{1,0,1,0,0,1,1,1}{0,1,1,0,0,1,1,1}{1,1,1,0,0,1,1,1}{0,0,0,1,0,1,1,1}{1,0,0,1,0,1,1,1}{0,1,0,1,0,1,1,1}{1,1,0,1,0,1,1,1}{0,0,1,1,0,1,1,1}{1,0,1,1,0,1,1,1}{0,1,1,1,0,1,1,1}{1,1,1,1,0,1,1,1}{0,0,0,0,1,1,1,1}{1,0,0,0,1,1,1,1}{0,1,0,0,1,1,1,1}{1,1,0,0,1,1,1,1}{0,0,1,0,1,1,1,1}{1,0,1,0,1,1,1,1}{0,1,1,0,1,1,1,1}{1,1,1,0,1,1,1,1}{0,0,0,1,1,1,1,1}{1,0,0,1,1,1,1,1}{0,1,0,1,1,1,1,1}{1,1,0,1,1,1,1,1}{0,0,1,1,1,1,1,1}{1,0,1,1,1,1,1,1}{0,1,1,1,1,1,1,1}{1,1,1,1,1,1,1,1}


*/
void unitTestBytesAndBits() {
#ifdef DEBUG
	unsigned char *arr = new unsigned char[16];
	for(unsigned bits16=0; bits16<(1<<16); ++bits16) {
		_mm_store_si128((__m128i*)arr,aux__initByteMasksFromNibbles(bits16));
		unsigned chopBits = bits16;
		for(unsigned i=0; i<16; ++i) {
			unsigned bit = chopBits&1;
			chopBits >>=1;
			assert(! ((arr[i] == 0xff && bit) || (arr[i] == 0 && !bit))); //I think it is broken, because there was in fact a bug!!!
		}
	}
#endif
}

void unitTestDuplicateTwoValsAndMask() {
	unsigned char buffer[16];
	__m128i* arr = (__m128i*) buffer;
	unsigned char valLow = 0xab;
	unsigned char valHigh = 0xdd;
	for(unsigned short masks=0; masks+1<(1<<16); ++masks) {
		if(masks%1000==0) COUT << masks << ENDL;
		_mm_store_si128(arr, duplicateTwoValsAndMaskWith16bits(valLow,valHigh,masks));
		for(unsigned b=0; b<8; ++b)
			if((masks&(1<<b))==1 && buffer[b]!=valLow) FATAL("")
			else if((masks&(1<<b))==0 && buffer[b]!=0) FATAL("");
		for(unsigned b=8; b<16; ++b)
			if((masks&(1<<b))==1 && buffer[b]!=valHigh) FATAL("")
			else if((masks&(1<<b))==0 && buffer[b]!=0) FATAL("");

	}
}
